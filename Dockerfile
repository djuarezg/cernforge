#
FROM gitlab-registry.cern.ch/vcs/forge-base:latest
#
MAINTAINER Alvaro Gonzalez <agonzale@cern.ch>

#
# Copy entrypoint
#
COPY entrypoint.sh /

#
# Copy the code
#
COPY ./code /var/django/

RUN chown -R 1001:0 /var/django && \
    chmod -R ug+rwx /var/django && \
    chown apache /var/django/cernforge/logs/

#
# Run the application (port 8000)
#
EXPOSE 8000

WORKDIR /var/django/

ENTRYPOINT '/entrypoint.sh'
