https://forgedocs.web.cern.ch/

Test the docker container localy:

```
docker build .
oc login
oc env dc/cernforge --list >env.txt
# This just gets the variables to run it
docker run -it --env-file env.txt <IMAGE_ID>
```

# Export template

```
oc export bc,dc,svc,route,pvc,configmap,serviceaccount,secrets -o yaml --as-template=cernforge >Private/cernforge.yaml

oc create -f Private/cernforge.yaml
```

# Export Scheduled jobs as template

```
oc export --as-template='jobs' scheduledjobs
```

E.g. read file ssh-privatekey in secret sshdeploykey

```
oc get secret/sshdeploykey -o go-template='{{index .data "ssh-privatekey"}}' | base64 -d


Update a secret or create a new one from file (multi line) (certificates)?

# use base64 -w 0 to disable line wrapping
oc patch secret gitlab-secrets -p "{\"data\":{\"secret-key\":\""$(cat file_with_secret_key | base64 -w 0)"\"}}"
```

Edit puppetparams.py
```
oc get secrets/puppetparams -o go-template='{{index .data "puppetparams.py"}}' | base64 -d >puppetparams.py
# Edit the file
oc patch secrets/puppetparams -p "{\"data\":{\"puppetparams.py\":\""$(cat puppetparams.py | base64 -w 0)"\"}}"
```

# Imports/creates

```
oc create -f templates/cernforge.yml
oc create -f templates/routes.yml
```

# Create PostgreSQL DB

```
pwgen -n 20
# Choose one
CREATE USER test_cernforge_py3 WITH PASSWORD '$(pwgen -n 20 | awk '$PASSWORD';
CREATE DATABASE test_cernforge_py3 WITH ENCODING 'UNICODE' LC_COLLATE 'C' LC_CTYPE 'C' TEMPLATE template0;
GRANT ALL PRIVILEGES ON DATABASE test_cernforge_py3 TO test_cernforge_py3;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO test_cernforge_py3;
```

# Update secret puppetparams

```
oc export secrets puppetparams -o json | jq '.data["puppetparams.py"]' -r | base64 -d >puppetparams.py
```

Edit the file

```
patch=$(jq -n --arg newparams "$(cat puppetparams.py| base64)" '{"data":{"puppetparams.py":$newparams}}')
oc patch secrets puppetparams -p "$patch"
# secret "puppetparams" patched
```

# DUMP the tables definitons

1. Create a file with the following ocntent:

```
set heading off;
set echo off;
Set pages 999;
set long 90000;

/* This is to dump a single table */
/* SELECT DBMS_METADATA.GET_DDL('TABLE', 'PROJECT') ddl FROM DUAL; */

SELECT DBMS_METADATA.GET_DDL('TABLE', u.table_name) FROM USER_TABLES u;
```

2. Execute the following:

```
echo "@devdb11-dump.sql" | sqlplus -S $DBUSER/$DBPASSWORD@$DBNAME >export.sql
```
test
