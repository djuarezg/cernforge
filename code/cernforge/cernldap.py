'''
    Utilities to retrieve information from CERN's LDAP
'''

import re
import hashlib

from ldap3 import (
    Connection,
    Server,
    ALL_ATTRIBUTES,
)

from django.core.cache import cache

from cernforge.settings import (
    LDAP_HOST,
    PAGESIZE
)

from cernforge.log import forgelogger
from cernforge.cernldap_pagedsearch import cernldap_pagedsearch

LDAP_EGROUP_BASE_DN = 'OU=e-groups,OU=Workgroups,DC=cern,DC=ch'
LDAP_USER_BASE_DNS = (
    'OU=Users,OU=Organic Units,DC=cern,DC=ch', 'OU=Externals,DC=cern,DC=ch')


class UserFoundException(Exception):
    '''
        Disabled UserFoundException
    '''
    pass


class AccountNotFoundInLdap(Exception):
    '''
        Add account from AccountNotFoundInLdap to the parameter
    '''

    def __init__(self, account):
        self.account = account


class GroupNotFoundInLdap(Exception):
    '''
        Add group from GroupNotFoundInLdap to the parameter
    '''

    def __init__(self, group):
        self.group = group


def _filter_groups(dn_list):
    '''
        Expects a list of DN paths.
        Returns a list of the groups found in the DN paths.
    '''
    group_regex = re.compile(r'CN=([^,]+),%s' % LDAP_EGROUP_BASE_DN)
    return [match.group(1) for cn in dn_list for match in [group_regex.search(cn)] if match]


def _filter_users(dn_list):
    '''
        Expects a list of DN paths.
        Returns a list of the users found in the DN paths.
    '''
    user_regex = re.compile(r'CN=([^,]+)(?:,OU=[a-z0-9])?,(?:{userDNs})'.format(
        userDNs='|'.join(LDAP_USER_BASE_DNS))
                           )
    return [match.group(1) for cn in dn_list for match in [user_regex.search(cn)] if match]


def get_egroup_members(egroup, raise_ex_on_found_user=False):
    '''
        Gets the members of an e-group
        Parameters:
         - egroup - the name of the egroup
         - raise_ex_on_found_user
         - True: Raises an exception as soon as the algorithm finds a member that is a user.
           Useful if you're not interested in who the members are, or how many members there is,
           just that at least one exist.
           any member.
    '''
    if not egroup_exist(egroup):
        raise GroupNotFoundInLdap(egroup)

    conn = Connection(Server(LDAP_HOST))
    conn.bind()

    search_parameters = {'search_base': LDAP_EGROUP_BASE_DN,
                         'search_filter': '(CN=%s)' % egroup,
                         'attributes': ['member']}

    conn.search(**search_parameters)

    result = conn.entries[0].entry_attributes_as_dict['member']

    conn.unbind()

    members = list()

    groups = _filter_groups(result)

    for group in groups:
        try:
            members += get_egroup_members(
                egroup=group,
                raise_ex_on_found_user=raise_ex_on_found_user,
            )
        except GroupNotFoundInLdap:
            forgelogger(
                'error',
                "Found '%s' as a child group of '%s', but '%s' does not \
                    seem to exist in LDAP!" % (group, egroup, group),
                email_admins=True,
            )

    members += _filter_users(result)

    if raise_ex_on_found_user:
        if members:
            raise UserFoundException()

    return members

def egroup_exist(egroup):
    '''
        Verifies that the 'egroup' exists as a DN path in LDAP.
        Does not check if the egroup has user members or not.
    '''
    conn = Connection(Server(LDAP_HOST))
    conn.bind()

    search_parameters = {'search_base': LDAP_EGROUP_BASE_DN,
                         'search_filter': '(CN=%s)' % egroup,
                         'attributes': ['DN'],
                         'paged_size': PAGESIZE}
    conn.search(**search_parameters)

    try:
        entry_dn = conn.entries[0].entry_dn
        conn.unbind()

        if entry_dn == 'CN=%s,%s' % (egroup, LDAP_EGROUP_BASE_DN):
            return True
        return False
    except IndexError:
        return False


def egroup_exist_with_user_members(egroup):
    '''
        Verifies that the 'egroup' exists in LDAP, with user members.
    '''
    try:
        get_egroup_members(
            egroup=egroup,
            raise_ex_on_found_user=True,
        )
    except UserFoundException:
        return True
    else:
        return False


def get_user_info(login):
    '''
        Returns a dict of all information about a user in LDAP
    '''
    conn = Connection(Server(LDAP_HOST))
    conn.bind()

    res = list()
    for distinguished_name in LDAP_USER_BASE_DNS:
        search_parameters = {'search_base': distinguished_name,
                             'search_filter': '(CN=%s)' % login,
                             # Should be sAMAccountName?
                             'attributes': ALL_ATTRIBUTES,
                             'paged_size': PAGESIZE}
        conn.search(**search_parameters)

        if conn.entries:
            res.append(conn.entries)

    conn.unbind()

    if not res:
        raise AccountNotFoundInLdap(login)
    elif len(res) > 1:
        raise Exception(
            'Account {acc} found in LDAP more than once!'.format(acc=login))
    else:
        return res[0][0].entry_attributes_as_dict


def user_exist(login):
    '''
        Verifies that the 'user' exists as a DN path in LDAP.
    '''
    conn = Connection(Server(LDAP_HOST))
    conn.bind()

    res = list()
    for distinguished_name in LDAP_USER_BASE_DNS:
        search_parameters = {'search_base': distinguished_name,
                             'search_filter': '(CN=%s)' % login,
                             # Should be sAMAccountName?
                             'attributes': [''],
                             'paged_size': PAGESIZE}
        conn.search(**search_parameters)

        if conn.entries:
            res.append(conn.entries)

    conn.unbind()

    if not res:
        return False
    elif len(res) > 1:
        raise Exception(
            'Account {acc} found in LDAP more than once!'.format(acc=login))
    else:
        return True


def _find_user_dn(login):
    '''
        Helper function that finds the full DN of a given 'login'.
        Raises an exception if no, or more than 1 DN was found.
    '''
    conn = Connection(Server(LDAP_HOST))
    conn.bind()

    user_dns = list()
    for distinguished_name in LDAP_USER_BASE_DNS:
        search_parameters = {'search_base': distinguished_name,
                             'search_filter': '(CN=%s)' % login,
                             # Should be sAMAccountName?
                             'attributes': ['CN'],
                             'paged_size': PAGESIZE}
        conn.search(**search_parameters)

        if conn.entries:
            user_dns.append(conn.entries[0].entry_dn)

    conn.unbind()

    if not user_dns:
        raise AccountNotFoundInLdap(login)
    elif len(user_dns) > 1:
        raise Exception(
            'Account {acc} found in LDAP more than once!'.format(acc=login))
    else:
        return user_dns[0]

def get_user_memberships(login):
    '''
        Returns a list of all e-groups that has the 'login' has a direct or indirect member.

        Ex: If e-group1 has e-group2 as a member, and the 'login' is a member of e-group2, the
        returning list will contain both e-group1 and e-group2.
    '''

    if not user_exist(login):
        raise AccountNotFoundInLdap(login)

    conn = Connection(Server(LDAP_HOST))
    conn.bind()

    search_parameters = {'search_base': LDAP_EGROUP_BASE_DN,
                         'search_filter':
                             '(member:1.2.840.113556.1.4.1941:=%s)' % _find_user_dn(login),
                         'attributes': ['CN'],
                         'paged_size': PAGESIZE}

    conn.search(**search_parameters)

    toreturn = list()
    for ent in conn.entries:
        toreturn.append(ent.cn)

    conn.unbind()

    return toreturn


def is_user_egroup_member(login, egroup):
    '''
        Returns True if the 'login' is a member of the e-group, directly or indirectly.
    '''

    if not egroup_exist(egroup):
        raise GroupNotFoundInLdap(egroup)

    conn = Connection(Server(LDAP_HOST))
    conn.bind()

    user_dn = _find_user_dn(login)
    search_parameters = {'search_base': user_dn,
                         'search_filter':
                             '(memberOf:1.2.840.113556.1.4.1941:=CN=%s,%s)' %
                             (egroup, LDAP_EGROUP_BASE_DN),
                         'attributes': ['']}

    conn.search(**search_parameters)

    if conn.entries:
        conn.unbind()
        return True

    conn.unbind()
    return False


def all_users(attrlist=[''], cache_duration=10 * 60):
    '''
        Returns a dictionary of all cn's where objectClass == 'person' found in the DN's in the
        LDAP_USER_BASE_DNS tuple as the key, and the attrlist as the value.

        The result is by default cached 10 minutes.
    '''
    return _get_accounts(
        attrlist=attrlist,
        searchfilter='(objectClass=person)',
        cache_duration=cache_duration,
    )


def all_serviceaccounts(attrlist=[''], cache_duration=10 * 60):
    '''
        Returns a dictionary of all cn's where employeeType or cernAccountType
        is 'service' found in the DN's in the LDAP_USER_BASE_DNS tuple as the
        key, and the attrlist as the value.
        The result is by default cached 10 minutes.
    '''
    return _get_accounts(
        attrlist=attrlist,
        searchfilter='(|(employeeType=Service)(cernAccountType=Service))',
        cache_duration=cache_duration,
    )


def _get_accounts(attrlist, searchfilter, cache_duration):
    '''
        Returns a dictionary of all cn's found in the DN's in the LDAP_USER_BASE_DNS tuple with
        search filter 'searchfilter', where the key is the cn and the value is a dictionary of
        the 'attrlist' attributes passed (default none).
    '''

    # In addition to the caching done in cernldap_pagedsearch, we also apply a
    # layer of caching here, as some operations in this method takes time.
    if cache_duration:
        cache_key = [searchfilter] + attrlist
        cache_key = hashlib.md5(''.join(cache_key).encode('utf-8')).hexdigest()
        results = cache.get(cache_key)
        if results:
            return results

    accounts = dict()
    raw_data = dict()
    for distinguished_name in LDAP_USER_BASE_DNS:

        result = cernldap_pagedsearch(
            basedn=distinguished_name,
            searchfilter=searchfilter,
            attrlist=attrlist,
            cache_duration=cache_duration,
        )

        if result:
            raw_data.update(result)

    if raw_data:
        cn_regex = re.compile(r'CN=([^,]+)(?:,OU=[a-z0-9])?,(?:{userDNs})'.format(
            userDNs='|'.join(LDAP_USER_BASE_DNS)))

        for distinguished_name, attrs in raw_data.items():
            account_match = cn_regex.match(distinguished_name)
            account = account_match.group(1)
            if attrlist:
                for attr in attrs:
                    if isinstance(attrs[attr], list):
                        for i, val in enumerate(attrs[attr]):
                            cn_match = cn_regex.match(val)
                            if cn_match:
                                attrs[attr][i] = cn_match.group(1)

            accounts[account] = attrs

        # Read comment above about caching
        if cache_duration:
            cache.set(cache_key, accounts, cache_duration)

        return accounts
    else:
        raise Exception(
            'No accounts (search filter {0}) could be found in LDAP!'.format(searchfilter))


def all_groups(cache_duration=10 * 60):
    '''
        Returns all cn's found in the LDAP_EGROUP_BASE_DN as a list.
        The results from cernldap_pagedsearch is by default cached 10 minutes.
    '''
    res = cernldap_pagedsearch(
        basedn=LDAP_EGROUP_BASE_DN,
        searchfilter='(objectClass=group)',
        attrlist=[''],
        cache_duration=cache_duration,
    )

    if res:
        return _filter_groups(res)
    else:
        raise Exception('No groups found in DN {dn}'.format(
            dn=LDAP_EGROUP_BASE_DN))
