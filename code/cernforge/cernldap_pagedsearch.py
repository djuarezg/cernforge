"""This code is largely identical to "Update 3" published at:
http://mattfahrner.com/2014/03/09/using-paged-controls-with-python-and-ldap/"""

import hashlib

from ldap3 import Connection, Server

#from distutils.version import StrictVersion

from django.core.cache import cache

from cernforge.settings import (
    LDAP_HOST,
    PAGESIZE
    )

def cernldap_pagedsearch(basedn, searchfilter, attrlist, cache_duration):
    """Cernldap paged search"""
    if cache_duration:
        cache_key = [basedn + searchfilter] + attrlist
        cache_key = hashlib.md5(''.join(cache_key).encode('utf-8')).hexdigest()
        results = cache.get(cache_key)
        if results:
            return results

    conn = Connection(Server(LDAP_HOST))
    conn.bind()

    search_parameters = {'search_base': basedn,
                         'search_filter': searchfilter,
                         'attributes': attrlist,
                         'paged_size': PAGESIZE}

    results = dict()

    while True:
        conn.search(**search_parameters)

        for entry in conn.entries:
            results[entry.entry_dn] = entry.entry_attributes_as_dict
        cookie = conn.result['controls']['1.2.840.113556.1.4.319']['value']['cookie']
        if cookie:
            search_parameters['paged_cookie'] = cookie
        else:
            break

    conn.unbind()

    return results
