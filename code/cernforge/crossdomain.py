'''
    Class to set server's http headers to allow cross-domain requests for domain set in
    XS_SHARING_ALLOWED_ORIGINS. If you want to use it, just add it to the middleware
    classes in settings.py file.
'''

#import re

#from django.utils.text import compress_string
#from django.utils.cache import patch_vary_headers

from django import http

try:
    import settings
    XS_SHARING_ALLOWED_ORIGINS = settings.XS_SHARING_ALLOWED_ORIGINS
    XS_SHARING_ALLOWED_METHODS = settings.XS_SHARING_ALLOWED_METHODS
except:
    XS_SHARING_ALLOWED_ORIGINS = 'https://cernforge01.cern.ch'
    # here set allow domains, who can send cross-domain requests
    XS_SHARING_ALLOWED_METHODS = ['POST', 'GET', 'OPTIONS']
    XS_SHARING_ALLOWED_HEADERS = ['x-requested-with', 'x-requested-by']

class XsSharing(object):
    '''
        Class to set server's http headers to allow cross-domain requests for domain set in
        XS_SHARING_ALLOWED_ORIGINS. If you want to use it, just add it to the middleware
        classes in settings.py file.
    '''
    def process_request(self, request):
        '''
	    Processes the request by adding the Access-Control-Allow-* headers if
            HTTP_ACCESS_CONTROL_REQUEST_METHOD is in the request
	'''
        if 'HTTP_ACCESS_CONTROL_REQUEST_METHOD' in request.META:
            response = http.HttpResponse()
            response['Access-Control-Allow-Origin'] = XS_SHARING_ALLOWED_ORIGINS
            response['Access-Control-Allow-Methods'] = ",".join(XS_SHARING_ALLOWED_METHODS)
            response['Access-Control-Allow-Headers'] = ",".join(XS_SHARING_ALLOWED_HEADERS)
            response['Access-Control-Allow-Credentials'] = "true"
            return response

        return None

    def process_response(self, request, response):
        '''
            Adds the Access-Control-Allow-* headers if they are not already in the response
        '''
        # Avoid unnecessary work
        if response.has_header('Access-Control-Allow-Origin'):
            return response

        response['Access-Control-Allow-Origin'] = XS_SHARING_ALLOWED_ORIGINS
        response['Access-Control-Allow-Methods'] = ",".join(XS_SHARING_ALLOWED_METHODS)
        response['Access-Control-Allow-Headers'] = ",".join(XS_SHARING_ALLOWED_HEADERS)
        response['Access-Control-Allow-Credentials'] = "true"

        return response
