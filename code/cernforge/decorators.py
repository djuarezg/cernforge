'''
    Decorators
'''
import threading

from django.utils.functional import wraps

from cernforge.forgethread import jobs_queue, thread_worker
from cernforge.log import forgelogger

def PositionalArgumentException(Exception):
    def __init__(self, message):
        super(PositionalArgumentException, self).__init__(message)

def runAsynchronously(func):
    '''
        Decorator to execute the contents of a method asynchronously.
        It expects the 'username' argument in the first position in the method
        that's being decorated. It will put the method along with it's
        parameters in the job queue, then spawn a thread which looks in the
        queue and executes the next job.
    '''
    @wraps(func)
    def decorator(username, *args, **kwargs):
        jobs_queue.put((func, username, args, kwargs))
        thread = threading.Thread(
            target=thread_worker,
        )
        thread.daemon = True
        thread.start()
    return decorator
