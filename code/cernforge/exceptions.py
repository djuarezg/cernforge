"""Account not found in AFS exception class"""
class AccountNotFoundInAfs(Exception):
    """Account not found in AFS exception"""
    pass

class NonZeroReturnCode(Exception):
    """Non zero return code exception"""
    def __init__(self, returncode, stderr, stdout):
        self.stderr = stderr
        self.stdout = stdout
        self.returncode = int(returncode)

class Timeout(Exception):
    """Timeout exception"""
    pass

class NoLdap(Exception):
    """No LDAP exception"""
    pass
