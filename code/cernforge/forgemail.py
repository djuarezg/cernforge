'''
    Email function wrapper
'''
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context

def send_mail(subject, sender, reply_to, receiver, data, plaintext_template, html_template):
    '''
        Email function wrapper
    '''
    plaintext = get_template(plaintext_template)
    context = Context(data)
    text_content = plaintext.render(context)

    headers = {}
    if reply_to:
        headers['Reply-To'] = reply_to

    msg = EmailMultiAlternatives(subject, text_content, sender, [receiver],
                                 headers=headers)

    if html_template:
        html = get_template(html_template)
        html_content = html.render(context)
        msg.attach_alternative(html_content, "text/html")

    msg.send()
