'''
    Thread library
'''
import queue

import atexit

from cernforge.log import forgelogger
from cernforge.signals import connect_pre_save_log, disconnect_pre_save_log

# At import time, we create a 'jobs_queue' queue
jobs_queue = queue.Queue()

def _log_thread_exception(msg_id, func, ex_msg):
    '''
        _log_thread_exception is a helper function for the thread_worker function.
        It contains logging messages for the exception scenarios in the thread_worker function,
        and will output the message to the error log file, and send an email to the administrator
        if called.
    '''
    if func is not None:
        func_path = '%s.%s' % (func.__module__, func.func_name)
    else:
        func_path = ''

    msgs = {
        'job_get_ex':             "[THREAD] A thread was spawned, but no job was found in the jobs queue. This is most probably due to a fauly threading implementation",
        'pre_save_connect_ex':    "[THREAD] The 'pre_save' signal could not be connected before executing the function '%s'. Changes to the databases as a result of this thread will NOT be logged" % func_path,
        'func_execution_ex':      "[THREAD] An exception was thrown and not handled during the execution of the function '%s'" % func_path,
        'pre_save_disconnect_ex': "[THREAD] The 'pre_save' signal could not be disconnected after executing the function '%s'. This might lead to future database transactions being logged double" % func_path,
        'job_done_ex':            "[THREAD] The job executing the function '%s' could not be flagged as finished. This is most probably due to a faulty threading implementation" % func_path,
    }

    forgelogger(
        level='error',
        message="%s (exception message: '%s')." % (msgs[msg_id], ex_msg),
        email_admins=True,
    )

def thread_worker():
    '''
        thread_worker looks for a job (a function with the username parameter, along with optional args and
        kwargs) in the jobs_queue, and executes it.

        By calling the _log_thread_exception function, we log and send an email to the administrators if:
         - The jobs queue is empty when this function is called - this will make thread_worker return early.
         - The pre_save signal which logs changes to the DB cannot be connected.
         - During the execution of the job, an exception is raised which is not handled by the function itself.
         - The pre_save signal previously connected cannot be disconnected.
         - The job cannot be marked as finished
    '''

    try:
        func, username, args, kwargs = jobs_queue.get(block=False)
    except Exception as exc:
        _log_thread_exception('job_get_ex', None, exc.message)
        return

    try:
        connect_pre_save_log(
            username=username,
            fromThread=True,
        )
    except Exception as exc:
        pre_log_signal_connected = False
        _log_thread_exception('pre_save_connect_ex', func, exc.message)
    else:
        pre_log_signal_connected = True

    try:
        func(username, *args, **kwargs)
    except Exception as exc:
        _log_thread_exception('func_execution_ex', func, "{0} (Exception type {1})".format(exc.message, type(exc).__name__))
    finally:
        if pre_log_signal_connected:
            try:
                disconnect_pre_save_log(username)
            except Exception as exc:
                _log_thread_exception('pre_save_disconnect_ex', func, exc.message)
        try:
            jobs_queue.task_done()
        except Exception as exc:
            _log_thread_exception('job_done_ex', func, exc.message)

def ensure_graceful_exit():
    '''
        ensure_graceful_exit will call Queue.join() which block normal interpreter termination if there
        still is jobs in the queue.
    '''
    jobs_queue.join()

# Make sure that we cannot exit if there still are jobs in the 'jobs_queue' queue
atexit.register(ensure_graceful_exit)
