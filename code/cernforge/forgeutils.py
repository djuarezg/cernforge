'''
    Forge utils
'''
from rest_framework import status
from cernforge.globals import JSONResponse


def get_model_changed(existing_state, new_state):
    '''
        Compares existing_state with new_state.
        Returns a description of the changes if they differ, None if not.
    '''
    before_data = {}
    after_data = {}
    for field in new_state._meta.fields + existing_state._meta.fields:
        before_data[field.name] = getattr(existing_state, field.name)
        after_data[field.name] = getattr(new_state, field.name)

    return describe_dict_diff(before_data, after_data)


def describe_dict_diff(beforeDict, afterDict):
    '''
        Expects two dictionaries, and returns a summary of the added and deleted
        keys, as well as the entries where the value has changed.
    '''
    added = {}
    if afterDict:
        for key, val in afterDict.items():
            if key not in beforeDict:
                added[key] = val

    deleted = {}
    altered = {}
    if beforeDict:
        for key, val in beforeDict.items():
            if key not in afterDict:
                deleted[key] = val
            elif val != afterDict[key]:
                altered[key] = "'%s'=>'%s'" % (val, afterDict[key])

    res = []
    if added:
        res.append('ADDED: %s' % ', '.join("%s: '%s'" % (k, v) for (k, v) in added.items()))

    if deleted:
        res.append('DELETED: %s' % ', '.join("%s: (was) '%s'" % (k, v) for (k, v) in deleted.items()))

    if altered:
        res.append('ALTERED: %s' % ', '.join("%s: %s" % (k, v) for (k, v) in altered.items()))

    if res:
        return ', '.join(res)
    return None


def approve_reject(user, pk, choice, model):
    '''
        Approve or reject a SVN project
    '''
    if user['admin'] is False:
        return JSONResponse(
            {'errors': {'general': ["Only administrators can approve/rject SVN repositories. You ({0}) does not seem to be an admin".format(user['login'])]}},
            status=status.HTTP_403_FORBIDDEN,
        )

    try:
        repository = model.objects.get(pk=pk)
    except model.DoesNotExist:
        return JSONResponse(
            {'errors': {'general': ["The repository '{0}' does not exist".format(pk)]}},
            status=status.HTTP_404_NOT_FOUND,
        )

    if repository.status in ['approved', 'rejected']:
        return JSONResponse(
            {'message': "The repository '{0}' is already {1}!".format(repository.shortname, repository.status)},
            status=status.HTTP_304_NOT_MODIFIED,
        )

    if repository.status != 'requested':
        return JSONResponse(
            {'errors': {'status': ["The repository '{0}' has status '{1}', not 'requested'. It cannot be set to approved or rejected.".format(repository.shortname, repository.status)]}},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # Setting the status to 'approved'
    # The cron script creating repositories will now act upon this
    repository.status = choice
    repository.save()

    return JSONResponse(
        {'message': "The status for repository '{0}' has been successfully set to '{1}'.".format(repository.shortname, choice)},
        status=status.HTTP_200_OK,
    )
