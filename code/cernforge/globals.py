'''
    Global functions and values
'''
import json
import os
import subprocess
import signal
import time
import math

import requests
from requests.auth import HTTPBasicAuth
from requests import HTTPError

from django.http import HttpResponse
from django.shortcuts import render

from rest_framework.status import (
    HTTP_401_UNAUTHORIZED,
    HTTP_400_BAD_REQUEST,
)

from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from cernforge.exceptions import NonZeroReturnCode
from cernforge.log import forgelogger
from cernforge.cernldap import (
    get_user_info,
    is_user_egroup_member,
    AccountNotFoundInLdap
)

from cernforge.settings import (
    JIRA_HOST,
    ADMIN_EGROUP,
)

REMOVE_SVN_PROJECT_SCRIPT = "/usr/local/SVN/RemoveProject/remove-project.sh"
REMOVE_GIT_PROJECT_SCRIPT = "delete_repository"
CHANGE_SVN_LIBRARIAN_SCRIPT = "chlib.sh"
ENABLE_ANONYMOUS_SCRIPT = '/afs/cern.ch/project/svn/scripts/admin/enable-anonymous-read.sh'
DISABLE_ANONYMOUS_SCRIPT = '/afs/cern.ch/project/svn/scripts/admin/disable-anonymous-read.sh'

# Scripts to k5reauth to the appropriate user (svnadmin, gitadmin or jiradmin),
# for then to ssh into the appropriate cluster (svn.cern.ch, git.cern.ch or its.cern.ch).
# The scripts accept a command that will be executed on the cluster
CONNECT_TO_SVN = "/var/django/svn/ssh_as_svnadmin"
WASSH_SVNADMIN = "/var/django/svn/wassh_svnadmin"
CONNECT_TO_GIT = "/var/django/git/ssh_as_gitadmin"
CONNECT_TO_ITS = "/var/django/jira/ssh_as_jiradmin"

SVN_CLI_PROD = "svn/cli/prod"

SVN_CREATE_REPOSITORY_SCRIPT = '/usr/local/SVN/NewPerlProjectCreation/project_creation.pl'
SVN_WIPEOUT_REPOSITORY_SCRIPT = '/afs/cern.ch/project/svn/scripts/admin/wipe_out_rep.sh'
GIT_CHANGE_PERMISSION = 'create_repository --permission'
GIT_CHANGE_LIBEGROUP = 'create_repository --egroup'


# Path related to logfiles for last access and writing
HTTPD_LATEST_LOGFILE = "/var/log/httpd/latest_logfile"
SSH_LATEST_LOGFILE = "/var/log/svnserve/repository_logs/latest_logfile"

BUNDLE = '/etc/pki/tls/certs/CERN-bundle.pem'

EGROUP_REST_API = "https://%s/jira/rest/egroups/2.0/mapper" % JIRA_HOST
JIRA_REST_API = "https://%s/jira/rest/api/2" % JIRA_HOST
JIRA_REST_CERN_PROJECTS = "https://%s/jira/rest/cernProjects/1.0/cernProjects" % JIRA_HOST
JIRA_BROWSE = "https://%s/jira/browse" % JIRA_HOST
JIRA_MAIN = "https://%s/jira" % JIRA_HOST
JIRA_ADMIN = "https://%s/jira/plugins/servlet/project-config" % JIRA_HOST
JIRA_PROJECTS_REST_API = "https://%s/jira/rest/cernProjects/1.0/cernProjects/" % JIRA_HOST

SITE_ROOT = os.path.dirname(os.path.realpath(__file__))

# command to do something as forgeadmin
K5REAUTH_AS_FORADMIN = "/usr/bin/k5reauth -p foradmin -k /etc/keytabs/foradmin.keytab"
K5REAUTH_AS_SVNADMIN = "/usr/bin/k5reauth -p svnadmin -k /etc/keytabs/svnadmin.keytab"


class JSONResponse(HttpResponse):
    '''
        This class keeps response in JSON format.
    '''
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


def errorHandler(request, user, error=None, status=HTTP_400_BAD_REQUEST):
    '''
        This method handles errors depends on request method (json || html).
    '''
    return_data = {
        'displayName': user['fullname'],
        'username': user['login'],
        'impersonate': user['impersonate'],
        'admin': user['admin'],
        'error': error,
    }

    try:
        if request.accepted_renderer.format == 'html':
            return render(
                request,
                'cernforge/error_page.html',
                return_data,
                status=status,
            )
        return Response(
            {'error': error},
            status=status,
        )
    except:
        return render(
            request,
            'cernforge/error_page.html',
            return_data,
            status=status
        )


def getUserInfo(request):
    '''
        This method tries to retrieve user info from request headers (if user
        was logged in via sso) or from LDAP if user logged in via basic auth.
    '''

    userInfo = {
        'login': None,
        'fullname': None,
        'admin': None,
        'email': None,
        'egroups': [],  # So the code doesn't explode if no logged in user
        'impersonate': False,
    }

    try:
        login = request.META['HTTP_X_ADFS_LOGIN']
        egroups = request.META['HTTP_X_ADFS_GROUP'].split(';')
        fullname = request.META['HTTP_X_ADFS_FULLNAME']
        email = request.META['HTTP_X_ADFS_EMAIL']
    except KeyError:
        try:
            login = request.META['ADFS_LOGIN']
            egroups = request.META['ADFS_GROUP'].split(';')
            fullname = request.META['ADFS_FULLNAME']
            email = request.META['ADFS_EMAIL']
        except KeyError:
            try:
                login = request.META['HTTP_X_REMOTE_USER']
                info = get_user_info(login)
                fullname = info['displayName'][0]
                egroups = []
                email = info['mail'][0]
            except KeyError:
                forgelogger('error', "User not logged in. Cannot get user from Headers. " +
                            str(request.META), True)
                return userInfo

    try:
        # first try to get info from http headers if user was logged in via browser
        userInfo.update({
            'login': login,
            'fullname': fullname,
            'email': email,
            'egroups': egroups,
            'admin': isAdmin(login, egroups),
        })
    except (KeyError, UnboundLocalError):
        # There is no logged in user, return defaults
        forgelogger('error', "User not logged in. Cannot update userInfo. "+str(request.META), True)

    return userInfo

def getAfsInfo(path_to_repository):
    '''
        This method returns info about AFS volume for given path:
        {
            'volume_name': 'name',
            'quota': <total_quota>,
            'used_quota': used_quota,
            'percent_used': 'percent used'
        }
    '''
    try:
        output = runCommand(
            command='{connect_svn} "{afs_info} {repository_path}"'.format(
                connect_svn=CONNECT_TO_SVN,
                #k5reauth=K5REAUTH_AS_FORADMIN,
                afs_info='afs_admin lq',
                repository_path=path_to_repository,
            ),
            timeout=15.0,
            silentFail=False,
            shell=True,
        )

        output_as_table = [s.split() for s in output.splitlines()]
        try:
            return {
                'quota': math.floor(int(output_as_table[1][1]) / 1000),
                'used_quota': math.floor(int(output_as_table[1][2]) / 1000),
                'percent_used': output_as_table[1][3],
                'volume_name': output_as_table[1][0],
            }
        except IndexError:
            return None
    except (Timeout, NonZeroReturnCode):
        return None

def isAdmin(user, user_egroups=None):
    '''
        Determines if the 'user' is an admin
         - If a list 'user_egroups' is passed, return True if 'ADMIN_EGROUP'
           is in that list
         - If not, do a LDAP query checking if 'user' is a part of the 'ADMIN_EGROUP'
    '''
    if user_egroups:
        return ADMIN_EGROUP in user_egroups

    return is_user_egroup_member(user, ADMIN_EGROUP)


def useRestApiWithBasicAuth(url, user=None, password=None, method='GET', objectr=None,
                            email_admins_prevent=[]):
    '''
        This method connect to REST API with given url and logs in with basic auth as given user.

        * The default method is GET.
    '''

    auth = None

    if user is not None:
        auth = HTTPBasicAuth(user, password)
    #
    try:
        if method == 'POST':
            response = requests.post(
                url=url,
                auth=auth,
                json=objectr,
                verify=BUNDLE
                )
        elif method == 'GET':
            response = requests.get(
                url=url,
                auth=auth,
                verify=BUNDLE
            )
        elif method == 'PUT':
            response = requests.put(
                url=url,
                auth=auth,
                json=objectr,
                verify=BUNDLE
                )
        else:
            forgelogger(level='error', message='Method %s not supported' % method,
                        email_admins=True)
            return (False, None)

        try:
            data = response.json()
        except:
            data = None

    except HTTPError as err:
        if hasattr(err, 'code'):
            #data = err.read()
            try:
                data = response.json()
            except UnboundLocalError:
                try:
                    data = json.load(err)
                except ValueError:
                    data = {}
            data['code'] = err.code

            # Do not send the emails if  the error code is in email_admins_prevent
            email_admins = err.code not in email_admins_prevent
            forgelogger(
                level='error',
                message="""HTTPError:\n - URL: %s\n - USER: %s\n - STATUS: %s\n - REQUEST: %s
 - METHOD: %s\n - RESPONSE: %s\n - REASON: %s""" % (response.url, user,
                                                    response.status_code, str(objectr),
                                                    response.request.method, str(data),
                                                    err.reason),
                email_admins=email_admins,
            )
            return (False, data)

    if 200 <= response.status_code < 300:
        return (True, data)

    forgelogger(
        level='error',
        message="CONNECT_WITH_REST_API_FAILED:\n - URL: %s\n - STATUS: %s \n\
- METHOD: %s\n - REQUEST: %s\n - DATA: %s"
        % (url, str(response.status_code), method,
           str(objectr), str(data)),
        email_admins=True,
    )
    return (False, data)

def runCommand(command, timeout, silentFail, shell):
    '''
        This method runs given command (with given timeout) and rises an exception
        if returncode was different than zero. In this case will also send emails
        to admins (specified in cernforge.settings), unless silentFail was set.
    '''
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=shell)

    if timeout != None:
        timeToFinish = time.time() + timeout
        while process.poll() is None and timeToFinish > time.time():
            time.sleep(1)
        if timeToFinish < time.time():
            os.kill(process.pid, signal.SIGKILL)
            if not silentFail:
                forgelogger(
                    level='error',
                    message='COMMAND_TIMEOUT ({0} s) | {1}'.format(timeout, command),
                    email_admins=True,
                )
            raise Timeout

    output, error = process.communicate()

    if process.returncode != 0:
        if not silentFail:
            forgelogger(
                level='error',
                message="""\n###COMMAND_ERROR START###\n#COMMAND:#\n%s\n#ERROR:#
%s\n#RETURNCODE:#\n%s\n#OUTPUT:#\n%s\n###COMMAND_ERROR END###""" % (
    command, error.strip(), process.returncode, output.strip()
    ),
                email_admins=True,
            )
        raise NonZeroReturnCode(
            returncode=process.returncode,
            stderr=error.strip(),
            stdout=output.strip(),
        )
    else:
        return output


# RQF0824436
def is_afs_account(account):
    '''
        This method checks if given account exists in AFS.
    '''
    try:
        runCommand(
            command='%s "ssh svnadmin@lxplus getent passwd %s"' % (CONNECT_TO_SVN, str(account)),
            timeout=None,
            silentFail=True,
            shell=True,
        )
    except NonZeroReturnCode:
        return False

    return True


def findOwnerOfServiceAccount(account):
    '''
        This method returns info about the owner of the given service account or
        raises exception if account doesn't exist.
    '''
    info_libuser = get_user_info(account)
    info_owner = info_libuser['cernAccountOwner'][0].split(',')
    owner = info_owner[0].replace('CN=', '')

    info_owner = get_user_info(owner)
    email = info_owner['mail'][0]
    fullname = info_owner['displayName'][0]

    return {
        'login': owner,
        'fullname': fullname,
        'email': email,
    }


def ldap_authentication(my_view):
    '''
        This decorator authenticates users, works for both: basic authentication and SSO.
    '''
    def _authenticate(request, *args, **kwargs):
        try:
            user = getUserInfo(request)
        except AccountNotFoundInLdap:
            user = {
                'fullname': "Anonymous",
                'impersonate': False,
            }

            return errorHandler(
                request,
                user,
                'Could not determine the user visiting',
                status=HTTP_401_UNAUTHORIZED,
            )

        if user['login'] is None:
            return errorHandler(
                request,
                user,
                'An error has ocurred while trying to perform the authentication. \
Administrators have been notified.',
                status=HTTP_401_UNAUTHORIZED,
            )

        user['impersonate'] = False

        # in case of impersonation collect all info about impersonated user
        # (only user with admin permission can do this!)
        if '_impersonate' in request.session:
            if isAdmin(user['login']):
                fake_user = {
                    'login': request.session['_impersonate'],
                    'admin': request.session['_impersonate_admin'],
                    'fullname': request.session['_impersonate_fullname'],
                    'email': request.session['_impersonate_email'],
                    'egroups': request.session['_impersonate_ADFS_GROUP'].split(';'),
                    'impersonate': True,
                }
                user = fake_user
            else:
                request.session.pop('_impersonate', None)

        kwargs['user'] = user
        return my_view(request, *args, **kwargs)

    return _authenticate
