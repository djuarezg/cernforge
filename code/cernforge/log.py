'''
    Logging class where forgelogger is defined
'''
import logging
import time
#import socket

from django.core.mail import send_mail, mail_admins

from cernforge import settings

def forgelogger(level, message, email_admins=False):
    '''
        Logs an entry, the level goes from debug to critical and it can send emails to admins
    '''
    # Log every message to stdout
    print("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    # Log to specific level
    logger = {
        'debug': logging.getLogger('cernforge.logdebug'),
        'info': logging.getLogger('cernforge.loginfo'),
        'warning': logging.getLogger('cernforge.logwarning'),
        'error': logging.getLogger('cernforge.logerror'),
        'critical': logging.getLogger('cernforge.logcritical'),
    }[level]
    getattr(logger, level)(message)

    if email_admins:
        #recipient_list= [x[1] for x in settings.ADMINS] if not settings.DEV else settings.DEV_EMAIL
        if settings.DEV:
            send_mail(
                subject='{0} [{1}]'.format(settings.EMAIL_SUBJECT_PREFIX, level.capitalize()),
                message="%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message),
                from_email=settings.SERVER_EMAIL,
                recipient_list=[settings.DEV_EMAIL],
            )
        else:
            mail_admins(
                subject='[{0}]'.format(level.capitalize()),
                message="%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message),
            )
