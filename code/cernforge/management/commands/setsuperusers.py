'''
    Set super users in the egroup cernforge-admins
'''
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand #, CommandError
from cernforge import cernldap
class Command(BaseCommand):
    '''
        Finds primary account members in superuser_egroup and adds them as
        administrators in CERN FORGE (Django). Does also demoted administrators
        not in this e-group.
    '''
    help = """Finds primary account members in superuser_egroup and adds them as
            administrators in CERN FORGE (Django). Does also demote
            administrators not in this e-group."""
    def handle(self, *args, **options):
        superuser_egroup = 'cernforge-admins'

        try:
            pesis_users = cernldap.get_egroup_members(superuser_egroup)
            pesis_users_info = dict()
            for admin in pesis_users:
                info = cernldap.get_user_info(admin)
                #if info['employeeType'][0] == 'Primary':
                pesis_users_info[info['cn'][0]] = info

        except cernldap.GroupNotFoundInLdap as exc:
            self.stderr.write(
                "Could not set superusers: Could find expand e-group '%s'.", exc.group)
        except cernldap.AccountNotFoundInLdap as exc:
            self.stderr.write(
                "Could not set superusers: Could not expand account '%s'.", exc.account)
        else:
            active_superusers = User.objects.filter(is_superuser=True)
#            active_superusers = User.objects.filter(is_superuser=True)

            # Looping through active superusers, deactivating those who aren't
            # in the superuser_egroup
            for superuser in active_superusers:
                if superuser.username not in pesis_users_info:
                    self.stdout.write(
                        "Deactivating superuser account '%s'; it was not found in superuser \
e-group '%s'." % (superuser.username, superuser_egroup),
                    )
                    superuser.is_active = 0
                    superuser.is_superuser = 0
                    superuser.save()

            # Looping through the accounts found in superuser_egroup, and either
            # adding new superusers or modifying existing superusers
            for pesis_user in pesis_users_info:
                try:
                    superuser = User.objects.get(username=pesis_user)
                    if not superuser.is_superuser:
                        self.stdout.write(
                            "Activating superuser account '%s'; it was found in \
superuser e-group '%s'." % (superuser.username, superuser_egroup),
                        )
                    else:
                        self.stdout.write(
                            "Superuser account '%s'; already a superuser" % superuser.username
                        )

                    superuser.__dict__.update({
                        'first_name': pesis_users_info[pesis_user]['givenName'][0],
                        'last_name': pesis_users_info[pesis_user]['sn'][0],
                        'email': pesis_users_info[pesis_user]['mail'][0],
                        'is_active': 1,
                        'is_superuser': 1,
                        'is_staff': 1,
                    })
                    superuser.save()
                except User.DoesNotExist:
                    self.stdout.write(
                        "Creating user account '%s'" % pesis_users_info[pesis_user]['cn'][0]
                    )
                    User.objects.create_superuser(
                        username=pesis_users_info[pesis_user]['cn'][0],
                        password='',
                        email=pesis_users_info[pesis_user]['mail'][0],
                        first_name=pesis_users_info[pesis_user]['givenName'][0],
                        last_name=pesis_users_info[pesis_user]['sn'][0],
                    )
