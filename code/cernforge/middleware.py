from cernforge.signals import connect_pre_save_log, disconnect_pre_save_log

from cernforge.globals import getUserInfo

# to avoid CSRF token checking
class DisableCSRF(object):
    def process_request(self, request):
        setattr(request, '_dont_enforce_csrf_checks', True)

class PresaveLogMiddleware(object):
    def process_request(self, request):
        username = getUserInfo(request)['login']
        connect_pre_save_log(
            username=username,
            fromThread=False,
        )

    def process_response(self, request, response):
        username = getUserInfo(request)['login']
        disconnect_pre_save_log(username)
        return response
