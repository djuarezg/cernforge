'''
    Main settings page
'''
#import os, socket, sys
import os
import sys

try:
    from params import puppetparams
except ImportError as importe:
    print(importe)
    sys.exit(1)

DEBUG = puppetparams.DEBUG
DEV = DEBUG

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
#BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_DIR = os.path.dirname(os.path.realpath(__file__))

# SMELL - Should limit this
# https://docs.djangoproject.com/en/1.8/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['*']

# SMELL - Should not be hardcoded
# Make this unique, and don't share it with anybody.
SECRET_KEY = '32661exm5l3s$*mvkvzc7k&amp;d@zo8-ia=wf@n=@gfq9q&amp;r7rn0@'

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_logtail',
    'cernforge',
    #'git',
    'jira',
    'projecttable',
    'svn',
    'weblib',
)

LOGTAIL_FILES = {
    'cernforge_error': '/var/django/cernforge/logs/cernforge_error.log',
    'cernforge_info': '/var/django/cernforge/logs/cernforge_info.log',
    'cernforge_all': '/var/django/cernforge/logs/cernforge_all.log'
}

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    #'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.RemoteUserMiddleware',
    #'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    #'django.middleware.clickjacking.XFrameOptionsMiddleware',
    #'django.middleware.security.SecurityMiddleware',
    'django.middleware.common.BrokenLinkEmailsMiddleware',
    'cernforge.middleware.DisableCSRF',
    'cernforge.middleware.PresaveLogMiddleware',
    #'cernforge.crossdomain.XsSharing',
    # Uncomment the next line for simple clickjacking protection:
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.RemoteUserBackend',
)

ROOT_URLCONF = 'cernforge.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'debug': DEBUG,
        },
    },
]

WSGI_APPLICATION = 'cernforge.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': puppetparams.DB_ENGINE,
        'NAME': puppetparams.DB_NAME,
        'USER': puppetparams.DB_USER,
        'PASSWORD': puppetparams.DB_PASS,
        'HOST': '',
        'PORT': '',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'Europe/Zurich'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'STATIC_ROOT')
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]

# Mailing
# https://docs.djangoproject.com/en/1.8/ref/settings/#s-email
ADMINS = [('CERN Forge Adminstrators', puppetparams.ADMIN_EMAIL)]
EMAIL_HOST = 'cernmx.cern.ch'
EMAIL_PORT = 25
EMAIL_SUBJECT_PREFIX = '[CERN Forge Admin] '
EMAIL_USE_TLS = True
MANAGERS = ADMINS
SERVER_EMAIL = 'cernforge-admins@cern.ch'
DEV_EMAIL = puppetparams.DEV_EMAIL  # custom setting

#
LDAP_HOST = 'xldap.cern.ch'
PAGESIZE = 10000
#
FORGE_USER = puppetparams.FORGE_USER
FORGE_PASS = puppetparams.FORGE_PASS
GIT2JIRA_PASS = puppetparams.GIT2JIRA_PASS
JIRA_SERVICE_EMAIL = puppetparams.JIRA_SERVICE_EMAIL
TRAC_LOCATION = puppetparams.TRAC_LOCATION
TRAC_INIT_LOCKFILE = puppetparams.TRAC_INIT_LOCKFILE
GITLAB_ADMIN_TOKEN = puppetparams.GITLAB_ADMIN_TOKEN
JIRA_HOST = puppetparams.JIRA_HOST
ADMIN_EGROUP = puppetparams.ADMIN_EGROUP
SVN_REPS_PATH = puppetparams.SVN_REPS_PATH
SVN_MIN_QUOTA = puppetparams.SVN_MIN_QUOTA
SVN_MAX_QUOTA = puppetparams.SVN_MAX_QUOTA
#

try:
    MEMCACHED_SERVICE_HOST = os.environ['MEMCACHED_SERVICE_HOST']
except KeyError:
    MEMCACHED_SERVICE_HOST = '127.0.0.1'

try:
    MEMCACHED_SERVICE_PORT = os.environ['MEMCACHED_SERVICE_PORT']
except KeyError:
    MEMCACHED_SERVICE_PORT = '11211'

# Caching
# https://docs.djangoproject.com/en/1.8/topics/cache/
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache',
        'LOCATION': MEMCACHED_SERVICE_HOST+':'+MEMCACHED_SERVICE_PORT,
    }
}

# http://docs.djangoproject.com/en/dev/topics/logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'cernforge_handler_all': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            #'filename': os.path.join(BASE_DIR, 'logs/cernforge_all.log'),
            'filename': '/dev/null',
            'maxBytes': 0,
            'backupCount': 0,
            'formatter': 'verbose',
        },
        'cernforge_handler_debug': {
            'level': 'DEBUG',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR, 'logs/cernforge_debug.log'),
            'maxBytes': 1024*1024*15, # 15MB
            'backupCount': 10,
            'formatter': 'verbose',
        },
        'cernforge_handler_info': {
            'level': 'INFO',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR, 'logs/cernforge_info.log'),
            'maxBytes': 1024*1024*15, # 15MB
            'backupCount': 10,
            'formatter': 'verbose',
        },
        'cernforge_handler_warning': {
            'level': 'WARNING',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR, 'logs/cernforge_warning.log'),
            'maxBytes': 1024*1024*15, # 15MB
            'backupCount': 10,
            'formatter': 'verbose',
        },
        'cernforge_handler_error': {
            'level': 'ERROR',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR, 'logs/cernforge_error.log'),
            'maxBytes': 1024*1024*15, # 15MB
            'backupCount': 10,
            'formatter': 'verbose',
        },
        'cernforge_handler_critical': {
            'level': 'CRITICAL',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR, 'logs/cernforge_critical.log'),
            'maxBytes': 1024*1024*15, # 15MB
            'backupCount': 10,
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'cernforge.logall': {
            'handlers': ['cernforge_handler_all'],
            'level': 'DEBUG',
            'propagate':True,
        },
        'cernforge.logdebug': {
            'handlers': ['cernforge_handler_debug'],
            'level': 'DEBUG',
        },
        'cernforge.loginfo': {
            'handlers': ['cernforge_handler_info'],
            'level': 'INFO',
        },
        'cernforge.logwarning': {
            'handlers': ['cernforge_handler_warning'],
            'level': 'WARNING',
        },
        'cernforge.logerror': {
            'handlers': ['cernforge_handler_error'],
            'level': 'ERROR',
        },
        'cernforge.logcritical': {
            'handlers': ['cernforge_handler_critical'],
            'level': 'CRITICAL',
        },
    }
}
