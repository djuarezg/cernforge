'''
    Signal thread control
'''
from collections import defaultdict

from django.db.models import signals
from django.utils.functional import curry

# from git.log import gitlogger
from jira.log import jiralogger
from svn.log import svnlogger

# from cernforge.log import forgelogger
from cernforge.forgeutils import get_model_changed

APPROVED_LOGGERS = {
    # 'git': gitlogger,
    'jira': jiralogger,
    'svn': svnlogger,
}

dispatch_uid_counter = defaultdict(int)


def connect_pre_save_log(username, fromThread=False):
    '''
        Connect the pre_save signal to the pre_save_log function.
        This can either be called from cernforge.middleware or cernforge.forgethread
        The dispatch_uid ensures that the same signal is not registered twice for one
        user. The dispatch_uid_counter ensures that the signal stays connected until
        all sources that registered it are done (request-response-middleware or thread).
    '''
    dispatch_uid = 'pre_save_log_{0}'.format(username)
    dispatch_uid_counter[dispatch_uid] += 1
    signals.pre_save.connect(
        receiver=curry(pre_save_log, username, fromThread),
        dispatch_uid=dispatch_uid,
        weak=False,
    )


def disconnect_pre_save_log(username):
    '''
        Disconnects the pre_save signal.
        The amount of disconnect calls must match the amount of connect calls in order
        for the signal to be disconnected.
        Example: If the middleware registers the signal, then a thread, we don't want
        to disconnect the signal before the thread is finished.
    '''
    dispatch_uid = 'pre_save_log_{0}'.format(username)
    dispatch_uid_counter[dispatch_uid] -= 1
    if dispatch_uid_counter[dispatch_uid] < 1:
        signals.pre_save.disconnect(
            dispatch_uid=dispatch_uid,
        )


def pre_save_log(username, fromThread, sender, instance, **kwargs):
    '''
        Saves log from thread
    '''
    app_label = instance._meta.app_label
    if app_label in APPROVED_LOGGERS:
        instance_type = type(instance)
        instance_name = instance.__class__.__name__

        message_level = 'info'
        message = 'THREAD | ' if fromThread else ''
        email_admins = False

        if instance._state.adding:
            message += "USER %s | MODEL %s | Model created (primary key '%s')" % (
                username, instance_name, instance.pk
            )
            APPROVED_LOGGERS[app_label](
                level=message_level,
                message=message,
                email_admins=email_admins,
            )
        else:
            existing_instance = instance_type.objects.get(pk=instance.pk)
            changed_description = get_model_changed(existing_instance, instance)
            if changed_description:
                message = "USER %s | MODEL %s | Model updated (primary key '%s'), Changes: %s" % (
                    username, instance_name, instance.pk, changed_description
                )
                APPROVED_LOGGERS[app_label](
                    level=message_level,
                    message=message,
                    email_admins=email_admins,
                )
