
jQuery(function() {
  var ua = navigator.userAgent.toLowerCase();
  if (
       (
         ua.indexOf("safari/") !== -1 &&  // It says it's Safari
         ua.indexOf("windows") !== -1 &&  // It says it's on Windows
         ua.indexOf("chrom")   === -1     // It DOESN'T say it's Chrome/Chromium
       )
       || // OR
       (
         ua.indexOf("opera/") !== -1 &&  // It says it's Opera
         ua.indexOf("linux")  !== -1 &&  // It says it's on Linux
         ua.indexOf("chrom")  === -1     // It DOESN'T say it's Chrome/Chromium
       )
    ) {
    var message = '<div id="browser-not-supported-message">';
    message += "Your browser (user agent '" + navigator.userAgent + "') is not supported! Please upgrade or try another. Please contact us if you have questions regarding this error message.";
    message += '</div>';
    jQuery('body').prepend(message);
  }
});


function prepareForm() {
  if (jQuery('#id_is_basic').val() == "True") {
    jQuery(".optional").hide();
    jQuery('#showOptional').val('Advanced')
  } else {
    jQuery(".optional").show();
    jQuery('#showOptional').val('Basic')
  }
  jQuery(':checkbox').prop('checked',false);
}

function toggleElementById(id) {
  jQuery('#'+id).toggle();
}

function toggleSelectById(id) {
  if (jQuery('#'+id).prop('disabled') == false) {
    jQuery('#'+id).prop('disabled','disabled')
  } else {
    jQuery('#'+id).prop('disabled','')
  }
}

function toggleElementByClass(className) {
  jQuery('.'+className).toggle();
}

function changeElementVal(id, one, another) {
  var newVal =  jQuery('#'+id).val() == one ? another : one;
  jQuery('#'+id).val(newVal);
}

function toggleNavbar(navId,pickedId, action){
  jQuery('#'+navId).find(".aui-nav-selected").removeClass('aui-nav-selected');
  jQuery('#'+pickedId).addClass('aui-nav-selected');
  action();
}

function doDialogAjax(params) {

  // Check the params and output an error if they don't look fine
  var expected_params = ['dialog_id','type','url','data','successCallback','errorCallback'];
  for (var i = 0; i < expected_params.length; i++) {
    if (!(expected_params[i] in params)) {
      var message = "System error, parameter '" + expected_params[i] + "' not set for dialog AJAX request";
      if ('dialog_id' in params) {
        AJS.messages.error('#form-' + params['dialog_id'] + '-errors', {
          title: 'System error.',
          body: message
        });
      } else {
        alert(message);
      }
      return;
    }
  }

  // If everything checked out, do the AJAX request
  jQuery.ajax({
    headers: { Accept : "application/json" },
    type:    params['type'],
    url:     params['url'],
    data:    JSON.stringify(params['data']),
    beforeSend: function() {
      jQuery('#' + params['dialog_id'] + '-loader').show();
    },
    complete: function() {
      jQuery('#' + params['dialog_id'] + '-loader').hide();
    },
    success: function(data, textStatus, xhr) {
      AJS.dialog2('#' + params['dialog_id']).hide();
      params['successCallback'](data, textStatus, xhr);
    },
    error: function(xhr, textStatus, errorThrown) {

      var error_title = '';
      var error_message = '';

      error_title = params['errorCallback'](xhr, textStatus, errorThrown);

      err_response = JSON.parse(xhr.responseText);
      error_message = '';
      if (typeof err_response == 'object' && 'errors' in err_response) {
        error_messages = [];
        if (err_response['errors'].constructor === Array) {
          for (var i = 0; i < err_response['errors'].length; i++) {
            error_messages.push(err_response['errors'][i]);
          }
        } else {
          for (var attr in err_response['errors']) {
            var msg = undefined;
            if (err_response['errors'][attr].constructor === Array) {
              if (err_response['errors'][attr].length > 1) {
                msg = attr + ': <ul>';
                for (var i = 0; i < err_response['errors'][attr].length; i++) {
                  msg += '<li>' + err_response['errors'][attr][i] + '</li>';
                }
                msg += '</ul>';
              } else {
                msg = attr + ': ' + err_response['errors'][attr][0];
              }
            } else {
              msg = attr + ': ' + err_response['errors'][attr];
            }
            error_messages.push(msg);
          }
        }
        error_message = error_messages.join('<br />');
      } else {
        error_message = 'No reason was given.';
      }

      jQuery('div#form-' + params['dialog_id'] + '-message').empty();
      jQuery('div#form-' + params['dialog_id'] + '-error').empty();
      AJS.messages.error('div#form-' + params['dialog_id'] + '-error', {
        title: error_title,
        body: error_message
      })
    }
  });
}
