'''
    URLs, main list
'''
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import RedirectView

from cernforge import views

admin.autodiscover()

urlpatterns = [
    # Admin panel
    url(r'^admin/', include(admin.site.urls)),
    url(r'(?i)^administrate/(?P<path>)',
        RedirectView.as_view(url='/admin/%(path)s', permanent=True)),

    # Home page
    url(r'(?i)^$', views.home_page, name='home_page'),

    # Admin pages
    url(r'(?i)^requests/$',
        views.requests_status_page, name='requests_status_page'),
    url(r'(?i)^impersonate/$', views.impersonate, name='impersonate'),

    # REST API for Git
    #url(r'(?i)^api/(?P<api_version>[1-9])/(?:(?:basic|shib|kerb)/)?git/',
    #    include('git.apiurls')),

    # REST API for JIRA
    url(r'(?i)^api/(?P<api_version>[1-9])/(?:(?:basic|shib|kerb)/)?jira/',
        include('jira.apiurls')),

    # REST API for SVN
    url(r'(?i)^api/(?P<api_version>[1-9])/(?:(?:basic|shib|kerb)/)?svn/',
        include('svn.apiurls')),

    # App-specific pages
    #url(r'(?i)^git/', include('git.urls')),
    url(r'(?i)^jira/', include('jira.urls')),
    url(r'(?i)^svn/', include('svn.urls')),
]
