'''
    Validators
'''
from django.core.exceptions import ValidationError

from cernforge import cernldap

def validate_cern_account(value, attribute=None):
    '''
        Validates CERN account using LDAP
    '''
    if not cernldap.user_exist(value):
        raise_validationerror("CERN account '{0}' doesn't exist.".format(value), attribute)

#def validate_egroup(value, attribute=None):
#    '''
#        Validates CERN egroup using LDAP
#    '''
#    try:
#        if not cernldap.egroup_exist_with_user_members(value):
#            raise_validationerror("E-group '{0}' is empty.".format(value), attribute)
#    except cernldap.GroupNotFoundInLdap:
#        raise_validationerror("E-group '{0}' doesn't exist.".format(value), attribute)

def raise_validationerror(message, attribute=None):
    '''
        Raise a ValidationError
    '''
    raise ValidationError({attribute: message} if attribute else message)
