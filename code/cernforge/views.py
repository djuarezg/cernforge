'''
    CERNforge views
'''
from datetime import datetime, timedelta

from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import redirect
from django.utils import timezone

from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from rest_framework.response import Response
from rest_framework.parsers import JSONParser

from cernforge.globals import (
    JSONResponse,
    ldap_authentication,
    errorHandler,
    isAdmin,
)
from cernforge import cernldap

#from git.models import GitProject
from jira.models import JiraLinkedRepository
from svn.models import SvnProject

@ldap_authentication
@api_view(["GET"])
@renderer_classes((TemplateHTMLRenderer, TemplateHTMLRenderer))
def home_page(request, user):
    '''
        This view displays CERN Forge's home page.
    '''
    return Response(
        {
            'displayName': user['fullname'],
            'username': user['login'],
            'admin': user['admin'],
            'impersonate': user['impersonate'],
        },
        template_name='cernforge/home_page.html',
    )

@ldap_authentication
@api_view(["GET"])
@renderer_classes((TemplateHTMLRenderer, TemplateHTMLRenderer))
def requests_status_page(request, user):
    '''
        This view displays all projects, which have been requested recently, and are not created yet.
        Available only for administrators.
    '''
    if not user['admin']:
        return errorHandler(request, user, "Permission denied", status=status.HTTP_403_FORBIDDEN)

    # Display all SVN repositories in state requested, approved or creating, as well as rejected
    # repositories newer than 7 days old
    svn_requests = SvnProject.objects.filter(
        Q(status__in=['requested', 'approved', 'creating'])|
        Q(status='rejected', create_date__gte=datetime.now() - timedelta(days=7))
    )
    for svn_request in svn_requests:
        if svn_request.create_date < datetime.today().date():
            svn_request.stuck = True

    #git_requests = GitProject.objects.filter(
    #    Q(status__in=['requested', 'approved', 'creating'])|
    #    Q(status='rejected', create_date__gte=datetime.now() - timedelta(days=7))
    #)
    #for git_request in git_requests:
    #    if git_request.create_date < datetime.today().date():
    #        git_request.stuck = True

    jira_linkedrepository_requests = JiraLinkedRepository.objects.filter(status='creating')
    for jira_request in jira_linkedrepository_requests:
        if jira_request.request_date and jira_request.request_date < timezone.now() - timedelta(hours=1):
            jira_request.stuck = True

    return Response({
        'displayName': user['fullname'],
        'username': user['login'],
        'admin': user['admin'],
        'impersonate': user['impersonate'],
        'svn_list': svn_requests,
        #'git_list': git_requests,
        'jenkins_list': [],
        'jira_linkedrepository_requests': jira_linkedrepository_requests,
        'preferred_type': request.COOKIES['preferred_type'] if 'preferred_type' in request.COOKIES else 'all',
    }, template_name="cernforge/requests.html")

@ldap_authentication
@api_view(['POST'])
@renderer_classes((JSONRenderer, JSONRenderer))
def impersonate(request, user):
    '''
        This view allows administrators to impersonate given user.
        You can stop impersonation by calling stop_impersonate view or clearing browser cache.
    '''
    if request.session.pop('_impersonate', None):
        return JSONResponse(
            {},
            status=status.HTTP_200_OK,
        )

    if user['admin']:

        json_data = JSONParser().parse(request)
        if 'login' not in json_data:
            return JSONResponse(
                {'errors': ['A username to impersonate was not provided.']},
                status=status.HTTP_400_BAD_REQUEST,
            )

        login = json_data['login']
        if not cernldap.user_exist(login):
            return JSONResponse(
                {'errors': ['The username to impersonate ({0}) does not exist.'.format(login)]},
                status=status.HTTP_400_BAD_REQUEST,
            )

        fake_user_info = cernldap.get_user_info(login)
        egroups = cernldap.get_user_memberships(login)
        request.session['_impersonate_admin'] = isAdmin(login)
        request.session['_impersonate_fullname'] = fake_user_info['displayName'][0]
        request.session['_impersonate_email'] = fake_user_info['mail'][0]
        request.session['_impersonate_ADFS_GROUP'] = ';'.join(egroups)
        request.session['_impersonate'] = login

        return JSONResponse(
            {},
            status=status.HTTP_200_OK,
        )

    else:
        return JSONResponse(
            {'errors': ['You do not have the necessary permission to impersonate a user.']},
            status=status.HTTP_403_FORBIDDEN,
        )
