'''
    Admin pages set up
'''
from django.contrib import admin
from jira.models import (
    JiraProject,
    JiraProjectRequest,
    JiraProjectMember,
    JiraLinkedRepository,
    WorkflowScheme,
    IssueTypeScreenScheme,
    Category,
    ScreenScheme,
    NotificationScheme,
    PermissionScheme,
    IssueSecurity,
)

class JiraProjectMemberInline(admin.TabularInline):
    '''
        Class to set up admin pages
    '''
    model = JiraProjectMember

    fields = (
        'member_id',
        'member_value',
        'is_group',
        'role_value',
    )

    readonly_fields = ['member_id']

class JiraProjectAdmin(admin.ModelAdmin):
    '''
        Class to set up admin pages
    '''
    # Which attributes are shown in the overview listing SVN projects
    list_display = (
        'id',
        'key',
        'name',
        'owner_id',
    )

    # Which attributes can be searched in
    search_fields = (
        'id',
        'key',
        'name',
        'owner_id',
    )

    # Which field are shown on the details page
    fields = (
        'id',
        'key',
        'name',
        'owner_id',
    )

    # Which fields cannot be edited on the details page
    readonly_fields = ['id']

    inlines = [JiraProjectMemberInline]

admin.site.register(JiraProject, JiraProjectAdmin)

class JiraProjectRequestAdmin(admin.ModelAdmin):
    '''
        Class to set up admin pages
    '''
    fields = ['key', 'name', 'lead', ]
    search_fields = ['key', 'name', 'lead']
    list_display = ['key', 'name', 'lead', 'categoryId', 'notificationScheme',
                    'workflowScheme', 'issueTypeScreenScheme', 'screenScheme']

admin.site.register(JiraProjectRequest, JiraProjectRequestAdmin)

class WorkflowSchemeAdmin(admin.ModelAdmin):
    '''
        Class to set up admin pages
    '''
    fields = ['name']
    search_fields = ['name']
    list_display = ['name']

admin.site.register(WorkflowScheme, WorkflowSchemeAdmin)

class IssueTypeScreenSchemeAdmin(admin.ModelAdmin):
    '''
        Class to set up admin pages
    '''
    fields = ['name']
    search_fields = ['name']
    list_display = ['name']

admin.site.register(IssueTypeScreenScheme, IssueTypeScreenSchemeAdmin)

class CategoryAdmin(admin.ModelAdmin):
    '''
        Class to set up admin pages
    '''
    fields = ['name']
    search_fields = ['name']
    list_display = ['name']

admin.site.register(Category, CategoryAdmin)

class ScreenSchemeAdmin(admin.ModelAdmin):
    '''
        Class to set up admin pages
    '''
    fields = ['name']
    search_fields = ['name']
    list_display = ['name']

admin.site.register(ScreenScheme, ScreenSchemeAdmin)

class NotificationSchemeAdmin(admin.ModelAdmin):
    '''
        Class to set up admin pages
    '''
    fields = ['name', 'is_public']
    search_fields = ['name']
    list_display = ['name', 'is_public']

admin.site.register(NotificationScheme, NotificationSchemeAdmin)

class IssueSecurityAdmin(admin.ModelAdmin):
    '''
        Class to set up admin pages
    '''
    fields = ['name', 'is_public']
    search_fields = ['name']
    list_display = ['name', 'is_public']

admin.site.register(IssueSecurity, IssueSecurityAdmin)

class PermissionSchemeAdmin(admin.ModelAdmin):
    '''
        Class to set up admin pages
    '''
    fields = ['name', 'is_public']
    search_fields = ['name']
    list_display = ['name', 'is_public']

admin.site.register(PermissionScheme, PermissionSchemeAdmin)

class JiraLinkedRepositoryAdmin(admin.ModelAdmin):
    '''
        Class to set up admin pages
    '''
    all_fields = (
        'link_id',
        'repository_type',
        'repository_url',
        'repository_path',
        'last_commit_info',
        'requestor',
        'request_date',
        'status',
    )

    # Fields in listing linked repositories
    list_display = all_fields

    # Which attributes can be searched in
    search_fields = all_fields

    date_hierarchy = 'request_date'

    # Which fields to be present in the detailed view
    fields = (
        #'link_id',
        'repository_type',
        'repository_url',
        'repository_path',
        'last_commit_info',
        'requestor',
        'request_date',
        'status',
    )

admin.site.register(JiraLinkedRepository, JiraLinkedRepositoryAdmin)
