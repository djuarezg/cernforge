'''
    JIRA API urls
'''
from django.conf.urls import url
from jira import views

urlpatterns = [
    # url(r'^$', views.jira_api, name='jira_api'),
    url(r'^project/(?P<pk>[/\-\d]+)$', views.jira, name='jira'),
    url(r'^project$', views.jira, name='jira'),
    url(r'^notificationschemes/$', views.notificationschemes,
        name='notificationschemes'),
    url(r'^project/(?P<id>[0-9]+)/Permissionscheme/$',
        views.project_permissionscheme, name='project_permissionscheme'),
    url(r'^project/(?P<id>[0-9]+)/Notificationscheme/$',
        views.project_notificationscheme, name='project_notificationscheme'),
    url(r'^project/(?P<id>[0-9]+)/Category/$', views.project_category,
        name='project_category'),
    url(r'^project/(?P<id>[0-9]+)/Categoryscheme/$', views.project_category,
        name='project_category'),
    url(r'^project/(?P<id>[0-9]+)/Issuesecurityscheme/$',
        views.project_issuesecuritylevelscheme,
        name='project_issuesecuritylevelscheme'),
    url(r'^project/(?P<id>[0-9]+)/Issuetypescreenscheme/$',
        views.project_issuetypescreenscheme,
        name='project_issuetypescreenscheme'),
    url(r'^permissionschemes/$', views.permissionschemes,
        name='permissionschemes'),
    url(r'^Permissionschemes/$', views.permissionschemes,
        name='permissionschemes'),
    url(r'^schemeegroups/$', views.schemeegroups,
        name='schemeegroups'),
    # url(r'^(?P<pk>[/\-\w]+)/notificationschemes/$',
    #    views.notificationschemes, name='notificationschemes'),
]
