'''
    Forms
'''
import re

from django import forms
from django.forms import ModelForm
from django.utils.safestring import mark_safe

from cernforge.settings import (
    GIT2JIRA_PASS
)

from cernforge.globals import runCommand
from cernforge.exceptions import NonZeroReturnCode, Timeout

from jira.models import (
    JiraProject,
    JiraProjectRequest,
    JiraLinkedRepository,
    NotificationScheme,
    IssueTypeScreenScheme,
    # ScreenScheme,
    WorkflowScheme,
    Category,
    PermissionScheme,
    # ProjectType,
    ProjectTemplate,
)

from jira.utils import (
    fix_git_repository_url,
    get_repository_path,
)


class JiraForm(ModelForm):
    '''
        Form to create a JIRA project
    '''
    name = forms.RegexField(
        widget=forms.TextInput(attrs={'class': 'text'}),
        max_length=30,
        regex=r'^[a-zA-Z0-9\s\-]+$',
        error_messages={'invalid': "Only alphanumeric characters are allowed"},
    )

    key = forms.RegexField(
        widget=forms.TextInput(attrs={'class': 'text'}),
        max_length=10,
        regex=r'^[a-zA-Z][a-zA-Z0-9]+$',
        error_messages={
            'invalid': "Project keys must start with an uppercase letter, followed by one or more uppercase alphanumeric characters.",
            'mega_help': mark_safe("The project key:<br> - maximum 10 alphnumetic characters \
                <br>- must start by a letter \
                <br>- should be short and descriptive, as it prefixes each issue \
                in the project<br> - should not contain sensitive information, \
                as it may be visible to users who do not have permission to view \
                the project<br> - It can be changed once the project is created, but it is not a trivial task")
        },
        help_text="This is the unique project key.",
    )

    lead = forms.CharField(
        widget=forms.HiddenInput(),
        label="",
        required=False,
    )

    projectTemplateKey = forms.ModelChoiceField(
        label='Project Template',
        widget=forms.Select(attrs={'class': 'select'}),
        queryset=ProjectTemplate.objects.all(),
        empty_label=None,
        #required=False,
        required=True,
    )

    notificationScheme = forms.ModelChoiceField(
        label='Notification Scheme',
        widget=forms.Select(attrs={'class': 'select'}),
        #queryset=NotificationScheme.objects.all(),
        queryset=NotificationScheme.objects.filter(is_public='Y'),
        empty_label=None,
        required=False,
    )

    workflowScheme = forms.ModelChoiceField(
        label='Workflow Scheme',
        widget=forms.Select(attrs={'class': 'select'}),
        queryset=WorkflowScheme.objects.all(),
        empty_label=None,
        required=False,
    )

    permissionScheme = forms.ModelChoiceField(
        label='Permission Scheme',
        widget=forms.Select(attrs={'class': 'select'}),
        #queryset=PermissionScheme.objects.all(),
        queryset=PermissionScheme.objects.filter(is_public='Y'),
        empty_label=None,
        required=False,
    )

    issueTypeScreenScheme = forms.ModelChoiceField(
        label='Issue Type Screen Scheme',
        widget=forms.Select(attrs={'class': 'select'}),
        queryset=IssueTypeScreenScheme.objects.all(),
        empty_label=None,
        required=False,
    )

    categoryId = forms.ModelChoiceField(
        label='Category',
        widget=forms.Select(attrs={'class': 'select'}),
        queryset=Category.objects.all(),
        empty_label='-----',
        required=False,
    )

#   Not needed anymore
#    projecttype = forms.ModelChoiceField(
#        label='',
#        #label='Project Type',
#        widget=forms.HiddenInput(),
#        #widget=forms.Select(attrs={'class':'select'}),
#        queryset=ProjectType.objects.all(),
#        empty_label=None,
#        required=False,
#    )

    is_basic = forms.CharField(
        label="",
        widget=forms.HiddenInput(),
        initial=True,
        required=False,
    )

    def clean(self):
        cleaned_data = super(JiraForm, self).clean()
        projectkey = cleaned_data.get("key")

        # check if there's no CREATED JIRA project (already created projects are
        # in Jira Project table) with the same key
        if projectkey:
            try:
                JiraProject.objects.get(key=projectkey)
                msg = u"Project with this key already exists"
                self._errors["key"] = self.error_class([msg])
                del cleaned_data["key"]
            except:
                pass

        projectname = cleaned_data.get("name")

        # check if there's no CREATED JIRA project (already created projects are
        # in Jira Project table) with the same name
        if projectname:
            try:
                JiraProject.objects.get(name=projectname)
                msg = u"Project with this name already exists"
                self._errors["name"] = self.error_class([msg])
                del cleaned_data["name"]
            except:
                pass

        # Always return the full collection of cleaned data.
        return cleaned_data

    class Meta:
        '''
            Meta class for the form
        '''
        model = JiraProjectRequest
        fields = (
            'name',
            'key',
            'lead',
            'notificationScheme',
            'workflowScheme',
            'issueTypeScreenScheme',
            'categoryId',
        )


class JiraLinkedRepositoryForm(forms.ModelForm):
    '''
        Form to link GiT repositories to JiRA
    '''
    def check_repository_access(self):
        '''
            Check if we have access to the repository
        '''
        repository_url = self.cleaned_data['repository_url']
        if self.cleaned_data['repository_type'] == 'git':

            # For git.cern.ch/reps/ URL's, we need to embed jiradmin user and pass before checking
            # cloneability
            repository_url = repository_url.replace(
                'https://git.cern.ch/reps/',
                'https://{user}:{password}@git.cern.ch/reps/'.format(
                    user='git2jira',
                    password=GIT2JIRA_PASS,
                ),
            )

            try:
                runCommand(
                    command=['git', 'ls-remote', repository_url],
                    timeout=10.0,
                    silentFail=True,
                    shell=False,
                )
            except NonZeroReturnCode:
                raise forms.ValidationError({'repository_url': ['Could not access repository.']})
            except Timeout:
                raise forms.ValidationError({'repository_url': ['Could not access repository - operation timed out. Please try again later.']})

    # The requestor is a static, readonly value, it cannot change.
    def clean_requestor(self):
        '''
            The requestor is a static, readonly value, it cannot change.
        '''
        requestor = self.cleaned_data['requestor']
        if 'requestor' in self.changed_data:
            raise forms.ValidationError(
                "The requestor has been tampered with. It is supposed to be '{requestor}', but it is '{requestor_new}'.".format(
                    requestor=self.initial['requestor'],
                    requestor_new=requestor,
                ),
            )
        return requestor

    def clean(self):
        cleaned_data = super(JiraLinkedRepositoryForm, self).clean()

        # Strip leading and trailing whitespace from all attributes
        for key, val in cleaned_data.items():
            cleaned_data[key] = val.strip()

        repository_type = cleaned_data['repository_type']
        repository_url = cleaned_data['repository_url']

        if repository_type == 'git':
            repository_url = fix_git_repository_url(repository_url)

            # http://stackoverflow.com/a/22312124
            if not re.match(r'^((git|ssh|http(s)?)|(git@[\w\.]+))(:(//)?)([\w\.@\:/\-~]+)\.git/?$', repository_url):
                raise forms.ValidationError({'repository_url': ['This URL does not look like a valid Git clone URL.']})

        else:
            raise forms.ValidationError({'repository_type': ["Invalid repository type '{0}'".format(repository_type)]})

        cleaned_data['repository_url'] = repository_url


        repository_path = get_repository_path(repository_type, repository_url)
        if repository_path:
            cleaned_data['repository_path'] = repository_path
        else:
            raise forms.ValidationError('Could not generate a repository path on the JIRA server for this repository.')

        self.check_repository_access()

        return cleaned_data

    def __init__(self, *args, **kwargs):
        super(JiraLinkedRepositoryForm, self).__init__(*args, **kwargs)
        mega_help = {
            'repository_url': mark_safe("""
                    <p>URL that can be used to clone (Git) from git.cern.ch. For GitLab check <a href="https://cern.service-now.com/service-portal/article.do?n=KB0003166">KB0003166</a></p>
                    <p>Examples:
                        <ul>
                            <li>https://git.cern.ch/reps/myrep/</li>
                            <li>http://git.cern.ch/cernpub/myrep/</li>
                        </ul>
                    </p>
                """)
        }

        for key, value in mega_help.items():
            self.fields[key].error_messages['mega_help'] = mega_help[key]

        self.fields['requestor'].required = True

    class Meta:
        '''
            Meta class with information
        '''
        model = JiraLinkedRepository
        fields = ('repository_type', 'repository_url', 'repository_path', 'requestor')
        widgets = {
            'repository_type': forms.Select(attrs={'class': 'select medium-long-field'}),
            'repository_url': forms.TextInput(attrs={'class': 'text medium-long-field'}),
            'requestor': forms.TextInput(
                attrs={
                    'class': 'text medium-long-field',
                    'readonly': 'readonly'
                }
            ),
        }
