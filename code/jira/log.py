'''
    JIRA logger
'''
from cernforge.log import forgelogger
def jiralogger(message, level='error', email_admins=False):
    '''
        Logs a message with a given level and optionaly sends an email
    '''
    forgelogger(
        level=level,
        message='JIRA | %s' % message,
        email_admins=email_admins,
    )
