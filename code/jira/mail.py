'''
    JIRA mails
'''
from cernforge import forgemail
from cernforge import settings

from jira.utils import get_repository_name

def sendmail_repository_link_requested(user, jira_linked_repository):
    '''
        Send and email about link repository request
    '''
    j = jira_linked_repository
    subject = '[CERN-JIRA] New repository requested linked'
    sender = user['email']
    receiver = 'it-service-its@cern.ch' if not settings.DEV else SETTINGS.DEV_EMAIL
    data = {
        'user_displayname': user['fullname'],
        'user_email': user['email'],
        'repository_name': get_repository_name(j.repository_type, j.repository_url),
        'repository_path': j.repository_path,
    }
    plaintext_template = 'jira/mail/linked-repository/requested.html'
    html_template = None
    reply_to = None

    forgemail.send_mail(subject, sender, reply_to, receiver, data, plaintext_template, html_template)
