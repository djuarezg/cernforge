'''
    Lists unsynced JIRA project IDs
'''
from django.core.management.base import BaseCommand

from cernforge.settings import (
    FORGE_USER,
    FORGE_PASS,
)

from cernforge.globals import (
    useRestApiWithBasicAuth,
    JIRA_REST_API,
)

from jira.models import JiraProject

class Command(BaseCommand):
    '''
        Lists unsynced JIRA project IDs
    '''
    def handle(self, *args, **options):
        '''
            Gets a list of all JIRA projects from the REST API...
        '''
        self.stdout.write('Getting list of all JIRA projects from the REST API ...')
        result, data = useRestApiWithBasicAuth(
            url='{api_url}/project'.format(
                api_url=JIRA_REST_API
            ),
            user=FORGE_USER,
            password=FORGE_PASS,
            method='GET',
        )

        if result:
            self.stdout.write('List retrieved.')
            api_ids = set([str(d['id']) for d in data])
            self.stdout.write('{len} projects was retrieved from the JIRA API.'
                              .format(len=len(api_ids)))
            db_ids = set([str(p.id) for p in JiraProject.objects.all()])
            self.stdout.write('{len} projects was retrieved was from the local DB.'
                              .format(len=len(db_ids)))

            ids_in_db = list()
            for db_id in db_ids:
                if db_id not in api_ids:
                    ids_in_db.append(db_id)

            if ids_in_db:
                self.stderr.write("ERROR - The following ID's is in the local DB,\
 but not in the JIRA API: {ids}"
                                  .format(
                                      ids=','.join(ids_in_db)
                                  )
                                 )
            else:
                self.stdout.write('SUCCESS - All JIRA Projects in the local DB \
are also in the JIRA API.')

            ids_in_api = list()
            for db_id in api_ids:
                if db_id not in db_ids:
                    ids_in_api.append(db_id)

            if ids_in_api:
                self.stderr.write("ERROR - The following ID's is in the JIRA \
API, but not synced to the local DB: {ids}".format(ids=ids_in_api))
