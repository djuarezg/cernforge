import time

import traceback

from urllib.error import URLError

from django.core.management.base import BaseCommand
from django.shortcuts import get_list_or_404

from cernforge.settings import (
    FORGE_USER,
    FORGE_PASS,
)

from cernforge import cernldap
from cernforge.globals import (
    useRestApiWithBasicAuth,
    JIRA_REST_CERN_PROJECTS,
)

#from urllib2 import (
#    Request,
#    URLError,
#    HTTPError,
#    build_opener,
#    HTTPHandler
#)

from jira.models import (
    JiraProject,
    Category
)
from jira.log import jiralogger
from jira.utils import UpdateJiraProject, ActorNotFoundException

class Command(BaseCommand):
    help = 'This command synchronizes the CERN Forge database with the JIRA database to update JIRA projects info.'

    def write_stdout(self, message):
        self.stdout.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def write_stderr(self, message):
        self.stderr.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def handle(self, *args, **options):
        self.write_stdout('SYNC_JIRA_PROJECTS: Sync started.')

        start = time.time()

        self.write_stdout('Getting project from {0}'.format(JIRA_REST_CERN_PROJECTS))
        # get list of all JIRA projects from REST API
        result, data = useRestApiWithBasicAuth(
            url=JIRA_REST_CERN_PROJECTS,
            user=FORGE_USER,
            password=FORGE_PASS,
            method='GET',
        )

        result = True

        if not result:
            message = 'SYNC_JIRA_PROJECTS: Error - Unable to get list of JIRA Projects from REST API. Aborting job.'
            jiralogger(
                level='error',
                message=message,
                email_admins=True,
            )
            self.write_stderr(message)
        else:
            totalProjects = len(data)
            self.write_stdout('{0} JIRA projects found from REST API'.format(totalProjects))

            ldap_users = cernldap.all_users()

            self.write_stdout('CERN LDAP Users fetched')

            ldap_groups = cernldap.all_groups()

            self.write_stdout('CERN LDAP Groups fetched')

            categories = get_list_or_404(Category)

            self.write_stdout('Categories fetched from DB')

            projectIds = list()
            index = 0
            for project in data:
                index += 1
                projectIds.append(project['id'])
                message = None
                try:
                    self.write_stdout(u'({index}/{total}) Updating project ({id}) {key}: {name}'
                                      .format(id=project['id'], key=project['key'],
                                              name=project['name'], index=index,
                                              total=totalProjects).encode())
                    startProject = time.time()

                    UpdateJiraProject(project['key'], project['id'], projectData=project,
                                      ldap_users=ldap_users, ldap_groups=ldap_groups,
                                      categories=categories)
                    self.write_stdout(u'{0}s Updated'.format(time.time()-startProject))
                except ActorNotFoundException as ane:
                    message = u'SYNC_JIRA_PROJECTS: Error - Unable to synchronize project {projectKey} ({projectId}), could not get actors when querying the following url: {roles_api_url}'.format(
                        projectId=project['id'],
                        projectKey=project['key'],
                        roles_api_url=ane.rolesApiUrl,
                    )
                except URLError as uer:
                    message = u'SYNC_JIRA_PROJECTS: Error - Unable to synchronize project {projectKey} ({projectId}), could not get actors when querying the following url: {roles_api_url}'.format(
                        projectId=project['id'],
                        projectKey=project['key'],
                        roles_api_url=uer.rolesApiUrl,
                    )
                    break
                except Exception as ex:
                    message = u'SYNC_JIRA_PROJECTS: Error - Unable to synchronize project {projectId}, an unkown exception was raised: {ex}. {tb}'.format(
                        projectId=project['id'],
                        ex=ex,
                        tb=traceback.format_exc(),
                    )
                finally:
                    if message:
                        jiralogger(
                            level='error',
                            message=message,
                            email_admins=True,
                        )
                        self.write_stderr(message)

            self.removeOutOfDateProjects(projectIds)

        message = 'SYNC_JIRA_PROJECTS: ENDED. Took {0} seconds'.format(time.time()-start)
        jiralogger(
            level='info',
            message=message,
            email_admins=False,
        )
        self.write_stdout(message)

    def removeOutOfDateProjects(self, projectIds):
        '''
            Get all projects from the DB, and delete all which have field 'id' where
            it's value is not in projectIds.
        '''
        projects = JiraProject.objects.all()

        for pro in projects:
            if int(pro.id) not in projectIds:
                self.write_stdout(u'Removing project ({projectId}) {key}: {name}'
                                  .format(
                                      projectId=pro.id,
                                      key=pro.key,
                                      name=pro.name,
                                     )
                                 )
                pro.delete()
