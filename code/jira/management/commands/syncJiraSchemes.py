'''
    Synchronizes JIRA schemes
'''
import time

from django.core.management.base import BaseCommand

from django.http import Http404
from django.shortcuts import get_object_or_404

# from cernforge import cernldap
from cernforge.globals import (
    useRestApiWithBasicAuth,
    JIRA_REST_API,
)

from cernforge.settings import (
    FORGE_USER,
    FORGE_PASS,
)

from django.db.utils import IntegrityError

#from jira.models import JiraProject, NotificationScheme, PermissionScheme, Category, IssueSecurity
from jira.models import NotificationScheme, PermissionScheme, Category, IssueSecurity
from jira.log import jiralogger
#from jira.utils import UpdateJiraProject, ActorNotFoundException

class Command(BaseCommand):
    '''
        This command synchronizes the CERN Forge database with the JIRA database
        to update JIRA Schemes info.
    '''

    def write_stdout(self, message):
        '''
            Writes to standard output adding the time
        '''
        self.stdout.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def write_stderr(self, message):
        '''
            Writes to standard error output adding the time
        '''
        self.stderr.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def handle(self, *args, **options):
        self.write_stdout('SYNC_JIRA_SCHEMES: Sync started.')

        self.syncIssueSecurity()
        self.syncCategory()
        self.syncPermissionSchemes()
        self.syncNotificationSchemes()

    def syncIssueSecurity(self):
        '''
            Gets list of all JIRA Schemes from REST API
        '''
        result, data = useRestApiWithBasicAuth(
            url='{api_url}/issuesecurityschemes'.format(
                api_url=JIRA_REST_API
            ),
            user=FORGE_USER,
            password=FORGE_PASS,
            method='GET',
        )

        # wtf
        result = True

        if not result:
            message = 'SYNC_JIRA_SCHEMES: Error - Unable to get list of JIRA Categories from REST API. Aborting job.'
            jiralogger(
                level='error',
                message=message,
                email_admins=True,
            )
            self.write_stderr(message)
        else:
            self.write_stdout('{0} JIRA Security found from REST API'.format(len(data)))

            try:
                for iss in data['issueSecuritySchemes']:
                    self.write_stdout("%s - %s" % (iss['id'], iss['name']))
                    try:
                        issuesec = get_object_or_404(IssueSecurity, id=iss['id'])
                        issuesec.name = iss['name']
                    except Http404:
                        issuesec = IssueSecurity(id=iss['id'])
                        issuesec.id = iss['id']
                        issuesec.name = iss['name']
                        issuesec.is_public = 'N'
                    try:
                        issuesec.save()
                    except:
                        self.write_stdout("ERROR")
            except KeyError as ker:
                self.write_stdout("KeyError: %s" % ker)

        message = 'SYNC_JIRA_PROJECTS: ENDED.'
        jiralogger(
            level='info',
            message=message,
            email_admins=False,
        )
        self.write_stdout(message)



    def syncCategory(self):
        '''
            Gets list of all JIRA Schemes from REST API
        '''
        result, data = useRestApiWithBasicAuth(
            url='{api_url}/projectCategory'.format(
                api_url=JIRA_REST_API
            ),
            user=FORGE_USER,
            password=FORGE_PASS,
            method='GET',
        )

        # wtf?
        result = True

        if not result:
            message = 'SYNC_JIRA_SCHEMES: Error - Unable to get list of JIRA Categories from REST API. Aborting job.'
            jiralogger(
                level='error',
                message=message,
                email_admins=True,
            )
            self.write_stderr(message)
        else:
            self.write_stdout('{0} JIRA Categories found from REST API'.format(len(data)))

            for dat in data:
                self.write_stdout("%s - %s" % (dat['id'], dat['name']))
                category = Category(id=dat['id'])
                category.id = dat['id']
                category.name = dat['name']
                try:
                    category.description = dat['description']
                except KeyError:
                    pass
                try:
                    category.save()
                except IntegrityError as dbie:
                    self.write_stdout("DB integrity ERROR: %s" % dbie)
                    self.write_stdout("Id: %s, name %s, description: %s" % (category.id,
                                                                           category.name,
                                                                           category.description))

        message = 'SYNC_JIRA_PROJECTS: ENDED.'
        jiralogger(
            level='info',
            message=message,
            email_admins=False,
        )
        self.write_stdout(message)

    def syncPermissionSchemes(self):
        '''
            Gets list of all JIRA Schemes from REST API
        '''
        result, data = useRestApiWithBasicAuth(
            url='{api_url}/permissionscheme'.format(
                api_url=JIRA_REST_API
            ),
            user=FORGE_USER,
            password=FORGE_PASS,
            method='GET',
        )

        # wtf?
        result = True

        if not result:
            message = 'SYNC_JIRA_SCHEMES: Error - Unable to get list of JIRA Projects from REST API. Aborting job.'
            jiralogger(
                level='error',
                message=message,
                email_admins=True,
            )
            self.write_stderr(message)
        else:
            values = data['permissionSchemes']
            self.write_stdout('{0} JIRA Permission schemes found from REST API'.format(len(values)))

            for val in values:
                self.write_stdout("%s - %s" % (val['id'], val['name']))
                try:
                    permissionscheme = get_object_or_404(PermissionScheme, id=val['id'])
                    permissionscheme.name = val['name']
                except Http404:
                    permissionscheme = PermissionScheme(id=val['id'])
                    permissionscheme.id = val['id']
                    permissionscheme.name = val['name']
                    permissionscheme.is_public = 'N'  # Default in DB
                try:
                    permissionscheme.save()
                except:
                    self.write_stdout("ERROR")

        message = 'SYNC_JIRA_PROJECTS: ENDED.'
        jiralogger(
            level='info',
            message=message,
            email_admins=False,
        )
        self.write_stdout(message)

    def syncNotificationSchemes(self):
        '''
            Gets list of all JIRA Schemes from REST API
        '''
        result, data = useRestApiWithBasicAuth(
            url='{api_url}/notificationscheme'.format(
                api_url=JIRA_REST_API
            ),
            user=FORGE_USER,
            password=FORGE_PASS,
            method='GET',
        )

        # wtf?
        result = True

        if not result:
            message = 'SYNC_JIRA_SCHEMES: Error - Unable to get list of JIRA Projects from REST API. Aborting job.'
            jiralogger(
                level='error',
                message=message,
                email_admins=True,
            )
            self.write_stderr(message)
        else:
            values = data['values']
            self.write_stdout('{0} JIRA Notification schemes found from REST API'.format(len(values)))

            for val in values:
                self.write_stdout("%s - %s" % (val['id'], val['name']))
                try:
                    notificationscheme = get_object_or_404(NotificationScheme, id=val['id'])
                    notificationscheme.name = val['name']
                except Http404:
                    notificationscheme = NotificationScheme(id=val['id'])
                    notificationscheme.id = val['id']
                    notificationscheme.name = val['name']
                    notificationscheme.is_public = 'N'  # Default in DB
                try:
                    notificationscheme.save()
                except:
                    self.write_stdout("ERROR")

        message = 'SYNC_JIRA_PROJECTS: ENDED.'
        jiralogger(
            level='info',
            message=message,
            email_admins=False,
        )
        self.write_stdout(message)
