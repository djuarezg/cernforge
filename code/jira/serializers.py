'''
    Return JSON representation of JIRA object
'''
from rest_framework import serializers
from rest_framework.renderers import JSONRenderer

from jira.models import JiraProjectRequest, JiraProject


class JiraProjectRequestSerializer(serializers.ModelSerializer):
    '''
        Serializer for a JIRA project request
    '''
    class Meta:
        '''
            Meta class
        '''
        model = JiraProjectRequest

    def __str__(self):
        return JSONRenderer().render(self.data)


class ExistingJiraProjectSerializer(serializers.ModelSerializer):
    '''
        Serializer for an existing JIRA project
    '''
    class Meta:
        '''
            Meta class
        '''
        model = JiraProject

        fields = (
            'id',
            'key',
            'name',
            'owner_id',
        )

    def __str__(self):
        return JSONRenderer().render(self.data)

    def validate(self, data):
        '''
            Everything would validate
        '''
        return data

    def __init__(self, *args, **kwargs):
        super(ExistingJiraProjectSerializer, self).__init__(*args, **kwargs)

        # Add validators, but since we are able to do partial updates here,
        # don't require any fields
        # add_field_validation(
        #    instance=self,
        #    required_fields={},
        # )

        # No fields may be blanked
        for field_name in self.fields.keys():
            self.fields[field_name].allow_blank = False
