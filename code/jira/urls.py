'''
    Patterns for JIRA
'''
from django.conf.urls import url
from jira import views

urlpatterns = [
    url(r'(?i)^$', views.jira_main, name='jira_main'),
    url(r'(?i)^request/$', views.jira_form, name='jira_form'),
    url(r'(?i)^myprojects/$', views.myprojects, name='myprojects'),
    url(r'(?i)^myprojects/(?P<id>[0-9]+)$', views.viewproject,
        name='viewproject'),
    url(r'(?i)^myprojects/(?P<key>[A-Z0-9]+)$', views.viewproject,
        name='viewproject'),
    url(r'(?i)^allprojects/$', views.allprojects,
        name='allprojects'),
    url(r'(?i)^syncproject/$', views.synchronize_jira_project,
        name='synchronize_jira_project'),
    url(r'(?i)^egroup/(?P<action>sync)/$', views.egroup,
        name='egroup'),
    # JIRA schemes
    #
    url(r'(?i)^schemes-access/$', views.schemesaccess,
        name='schemesaccess'),
    url(r'(?i)^issuesecurityschemes/$', views.issuesecurityschemes,
        name='issuesecurityschemes'),
    url(r'(?i)^issuesecurityschemes/(?P<id>[0-9]+)/$', views.issuesecurityscheme,
        name='issuesecurityscheme'),
    url(r'(?i)^Issuesecurityschemes/(?P<id>[0-9]+)/$', views.issuesecurityscheme,
        name='issuesecurityscheme'),
    #
    url(r'(?i)^categories/$', views.categories, name='categories'),
    url(r'(?i)^categories/(?P<id>[0-9]+)/$', views.categories, name='categories'),
    url(r'(?i)^Categoryschemes/(?P<id>[0-9]+)/$', views.categories, name='categories'),
    #
    url(r'(?i)^permissionschemes/$', views.permissionschemes,
        name='permissionschemes'),
    url(r'(?i)^permissionschemes/(?P<id>[0-9]+)/$', views.permissionscheme,
        name='permissionscheme'),
    #
    url(r'(?i)^notificationschemes/$', views.notificationschemes,
        name='notificationschemes'),
    url(r'(?i)^notificationschemes/(?P<id>[0-9]+)/$', views.notificationscheme,
        name='notificationscheme'),

    # JIRA Linked Repositories
    url(r'^linked-repository/request/$',
        views.linkrep_request, name='linkrep_request'),
    url(r'(?i)^linked-repository/remove/$', views.linkrep_remove,
        name='linkrep_remove'),
    url(r'(?i)^linked-repository/list/$', views.linkrep_list,
        name='linkrep_list'),
]
