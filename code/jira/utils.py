'''
    Class to synchronize a JIRA project into the DB
'''
import re
import json

from django.db import (
    DatabaseError,
    IntegrityError
    )
from django.db.models import Q
from django.shortcuts import get_list_or_404

from cernforge.settings import (
    FORGE_USER,
    FORGE_PASS,
)

from cernforge import cernldap
from cernforge.exceptions import NonZeroReturnCode, Timeout
from cernforge.globals import (
    useRestApiWithBasicAuth,
    JIRA_PROJECTS_REST_API,
    runCommand,
    CONNECT_TO_ITS,
)

from jira.models import (
    Category,
    JiraProject,
    JiraProjectMember,
    JiraLinkedRepository,
    NotificationScheme,
    PermissionScheme,
    IssueSecurity,
    EgroupSchemes,
)

from jira.log import jiralogger


class ActorNotFoundException(Exception):
    '''
        Exception for when the user is not found
    '''
    def __init__(self, rolesApiUrl):
        Exception.__init__()
        self.rolesApiUrl = rolesApiUrl


class JiraProjectNotFoundException(Exception):
    '''
        Exception for when the project is not found
    '''
    def __init__(self, projectKey):
        Exception.__init__()
        self.projectKey = projectKey


class UpdateJiraProject(object):
    '''
        Given a project key, its representation, ldap_users, and ldap_groups
    '''
    def __init__(self, projectKey, projectId, projectData=None, ldap_users=None, ldap_groups=None, categories=None):
        self.projectKey = projectKey
        self.projectId = projectId
        if projectData is None:
            try:
                self.projectData = self.getProjectData()
            except JiraProjectNotFoundException:
                jiralogger(
                    level='error',
                    message='JiraProjectNotFoundException: Project: {project} not found.'.format(
                        project=projectKey,
                    ),
                    email_admins=False,
                )
        else:
            self.projectData = projectData

        self.categories = categories
        if categories is None:
            jiralogger(level='info', message='Getting all categories')
            self.categories = get_list_or_404(Category)

        self.deleteProject()

        if ldap_users is None:
            jiralogger(level='info', message='Calling cernldap.all_users', email_admins=False)
            self.ldap_users = cernldap.all_users()
        else:
            self.ldap_users = ldap_users

        if ldap_groups is None:
            jiralogger(level='info', message='Calling cernldap.all_groups', email_admins=False)
            self.ldap_groups = cernldap.all_groups()
        else:
            self.ldap_groups = ldap_groups

        self.syncProject()

    def getProjectData(self):
        '''
            Uses JIRA's stock API to get projects data
        '''

        jiralogger(level='info', message='Getting project data from {0}/(1}'.format(JIRA_PROJECTS_REST_API,
                                                                                    self.projectKey))

        result, data = useRestApiWithBasicAuth(
            url='{api_url}{projectKey}'.format(
                api_url=JIRA_PROJECTS_REST_API,
                projectKey=self.projectKey,
            ),
            user=FORGE_USER,
            password=FORGE_PASS,
            method='GET',
        )

        if not result:
            raise JiraProjectNotFoundException(self.projectKey)
        else:
            return data

    def deleteProject(self):
        '''
            Deletes a project. Needs a valid projectID (project Key).
        '''
        # First, we delete any project with members that has the same ID as the one we're currently
        # syncing
        try:
            existing_project = JiraProject.objects.get(pk=self.projectKey)
        except JiraProject.DoesNotExist:
            pass
        else:
            JiraProjectMember.objects.filter(project=existing_project).delete()
            existing_project.delete()

        # Then we delete any project that share the same key.
        # There might be an old project in the DB with the same key as the one we're updating,
        # but with another ID. In that case, delete that as well, along with it's members.
        try:
            existing_project = JiraProject.objects.get(key=self.projectData['key'])
        except JiraProject.DoesNotExist:
            pass
        else:
            JiraProjectMember.objects.filter(project=existing_project).delete()
            existing_project.delete()

    def syncProject(self):
        '''
            Creates the project in the DB, it should have been deleted beforehand. Not the best method
        '''
        # Create the project in the DB (we assume that is not existing at this point)
        self.jiraProject = JiraProject(id=self.projectId)
        self.jiraProject.key = self.projectData['key']
        self.jiraProject.name = self.projectData['name']

        try:
            for cat in self.categories:
                if cat.name == self.projectData['category']:
                    self.jiraProject.categoryid = cat.id
        except KeyError:
            jiralogger(level='info', message='Project {0} has no category'.format(self.jiraProject.key))

        try:
            self.jiraProject.owner_id = self.projectData['lead']['name']
        except KeyError:
            self.jiraProject.owner_id = self.projectData['lead']
        except TypeError:
            self.jiraProject.owner_id = self.projectData['lead']

        try:
            self.jiraProject.save()
        except IntegrityError as iee:
            jiralogger(level='error', message=iee)
            jiralogger(level='error', message='{0} ({1}) failed to be saved on DB.'.format(self.jiraProject.name, self.jiraProject.key))

        # Sync the project's members
        jiralogger(level='info', message='Syncing members of project {0}'.format(self.jiraProject.key))
        try:
            self.syncProjectMembers()
        except KeyError:
            jiralogger(level='error', message='Cannot synchronize members', email_admins=False)

    def syncProjectMembers(self):
        '''
            Excpects a dictionary with roles as keys and member dictionaries as values, where its keys are
            the jira member type and value a list of the members of that type.

            The local DB is inserted with the members, and exceptions are thrown if they could be found in
            LDAP.

            Finally, the local DB is purged of members that are no longer in the JIRA API.
        '''

        jiralogger(level='error', message='Getting project members')
        roleMembers = self.getProjectMembers()

        jiralogger(level='error', message='Adding project members')
        all_groups = list()
        all_users = list()
        for role, members in roleMembers.items():
            if 'atlassian-group-role-actor' in members:
                for group in members['atlassian-group-role-actor']:
                    if group in self.ldap_groups:
                        JiraProjectMember.objects.get_or_create(
                            project=self.jiraProject,
                            member_value=group,
                            is_group='Y',
                            role_value=role,
                        )
                        all_groups.append(group)

            if 'atlassian-user-role-actor' in members:
                for user in members['atlassian-user-role-actor']:
                    if user in self.ldap_users.keys():
                        try:
                            JiraProjectMember.objects.get_or_create(
                                project=self.jiraProject,
                                member_value=user,
                                is_group='N',
                                role_value=role,
                            )
                        except DatabaseError as dbe:
                            jiralogger(
                                level='error',
                                message='DatabaseError: Project: {project}, member_value: {member_value}, is_group: {is_group}, role_value: {role_value}.\n{dbe}'.format(
                                    project=self.jiraProject,
                                    member_value=user,
                                    is_group='N',
                                    role_value=role,
                                    dbe=dbe,
                                ),
                                email_admins=False,
                            )
                        all_users.append(user)

        jiralogger(level='error', message='Remove out of date members')
        # remove out-of-date members
        for role in roleMembers.keys():
            entries = JiraProjectMember.objects.filter(
                project=self.jiraProject,
                role_value=role,
            )
            for entry in entries:
                if entry.is_group == 'Y' and entry.member_value not in all_groups:
                    entry.delete()
                if entry.is_group == 'N' and entry.member_value not in all_users:
                    entry.delete()

    def getProjectMembers(self):
        '''
            Expects a list of valid JIRA role names. Looks up the role API paths in self, and queries them
            for its members. The returned result is a dict that should look like this:
            {
                'Users': {
                    'atlassian-group-role-actor': ['group1','group2', ...],
                    'atlassian-user-role-actor': ['user1','user2', ...],
                }
                'Developers': ...
            }
        '''
        role_members = dict()
        for role in self.projectData['roles'].keys():
            #jiralogger(message='{0} {1}'.format(role, self.projectData['roles'][role]))
            members = dict()
            members['atlassian-user-role-actor'] = list()
            members['atlassian-group-role-actor'] = list()
            for actorrole in self.projectData['roles'][role]:
                members[actorrole['type']].append(actorrole['name'])
            role_members[role] = members

        return role_members

def sync_linked_repositories():
    '''
        Performs a synchronization of GiT reposiories
    '''
    try:
        timeout = 300.0
        output = runCommand(
            command="{connect_its} '{extract_git_info}'".format(
                connect_its=CONNECT_TO_ITS,
                extract_git_info='cd /var/git; for d in ./*/; do (cd "$d"; echo "{jsonstring}";); done'.format(
                    jsonstring='{\\"dirname\\":\\"$d\\",\\"repository_url\\":\\"`git config --get remote.origin.url`\\",\\"last_commit_info\\":\\"`git rev-parse HEAD`\\"}',
                ),
            ),
            timeout=timeout,
            silentFail=True,
            shell=True,
        )
    except NonZeroReturnCode as nze:
        jiralogger(
            level='error',
            message='Could not connect to its.cern.ch to sync Git repositories.\nReturn code: {0}\nStderr: {1}\nStdout: {2}'.format(
                nze.returncode,
                nze.stderr,
                nze.stdout,
            ),
            email_admins=True,
        )
    except Timeout:
        jiralogger(
            level='error',
            message='Could not connect to its.cern.ch to sync Git repositories, operation timed out (limit {0} seconds)'.format(timeout),
            email_admins=True,
        )
    else:
        for line in output.splitlines():
            try:
                jsondata = json.loads(line.strip())
            except ValueError:
                pass
            else:
                try:
                    linked_rep = JiraLinkedRepository.objects.get(
                        repository_type='git',
                        repository_url=fix_git_repository_url(jsondata['repository_url']),
                    )
                except JiraLinkedRepository.DoesNotExist:
                    linked_rep = JiraLinkedRepository(
                        repository_type='git',
                        repository_url=fix_git_repository_url(jsondata['repository_url']),
                        repository_path='/var/git/{0}'.format(re.sub(r'^\./([^/]+)/?$', r'\1', jsondata['dirname'])),
                    )
                else:
                    linked_rep.repository_path = '/var/git/{0}'.format(re.sub(r'^\./([^/]+)/?$', r'\1', jsondata['dirname']))

                if jsondata['last_commit_info'] != 'HEAD':
                    linked_rep.last_commit_info = jsondata['last_commit_info']
                linked_rep.status = 'active'
                linked_rep.save()

def fix_git_repository_url(url):
    '''
        Changes git URLs to safer options
    '''
    url = url.strip()
    if 'git.cern.ch' in url:
        # For git.cern.ch repositories, Kerberos authentication URL's won't work.
        # But we can convert the url to a LDAP authentication URL without telling the user.
        url = url.replace(
            ':@git.cern.ch/kerberos/',
            'git.cern.ch/reps/',
        )

        # We can also help if the user has entered the gitweb public url, converting it
        # to the URL with LDAP authentication instead of giving an error message to the
        # user.
        url = url.replace(
            'git.cern.ch/web/',
            'git.cern.ch/reps/',
        )

        # cernpub urls must be http
        # all other urls must be https
        if 'https' in url:
            if '/cernpub/' in url:
                url = url.replace('https', 'http')
        else:
            url = url.replace('http', 'https')

    # Make sure that the url ends with .git/
    url = re.sub(r'(?:\.git)?/?$', '.git', url)

    return url

def get_repository_name(repository_type, repository_url):
    '''
        Given a git URL, returns the repositiory name
    '''
    repository_name = None
    if repository_type == 'git':
        search = re.search(r'/([^/]+?)(?:\.git)?/?$', repository_url)
        if search:
            repository_name = search.group(1)
    return repository_name

def get_repository_path(repository_type, repository_url):
    '''
        get_repository_path will find a unique new path for a given repository on the central JIRA server,
        (currently its.cern.ch).
    '''
    repository_path = None
    repository_name = get_repository_name(repository_type, repository_url)
    if repository_name:
        if repository_type == 'git':
            repository_path = '/var/git/{0}'.format(repository_name)

    if repository_path:
        original_repository_path = repository_path
        conflicts = 0

        success = False
        while True:
            try:
                runCommand(
                    command="{connect_its} 'test -d {repository_path}'".format(
                        connect_its=CONNECT_TO_ITS,
                        repository_path=repository_path,
                    ),
                    timeout=15.0,
                    silentFail=True,
                    shell=True,
                )
            except NonZeroReturnCode:
                success = True
                break
            except Timeout:
                break
            else:
                if conflicts < 5:
                    repository_path = '{0}_{1}'.format(original_repository_path, conflicts + 1)
                else:
                    break

        if success:
            return repository_path

    return None

def getPrivateSchemes(user, schemeType):
    '''
        Given a user, with its egroups field, and a scheme type, returns all the schemes he has access to
    '''
    switcher = {
        'N': NotificationScheme,
        'P': PermissionScheme,
        'S': IssueSecurity,
    }

    theReturned = []
    try:
        for egs in EgroupSchemes.objects.filter(Q(schemeType=schemeType, egroup__in=user['egroups'])):
            theReturned.append(switcher.get(schemeType).objects.get(id=egs.id))
    except AttributeError:
        return []
    return theReturned
