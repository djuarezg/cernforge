'''
    JIRA views
'''
import json
import re
import copy
import time
import calendar
import os.path
import urllib
import requests
import types

import traceback

from django.core.cache import cache
from django.core.mail import EmailMultiAlternatives
from django.core.files import File
from django.db.models import Q
from django.utils.safestring import mark_safe
from django.utils import timezone
from django.template import loader
from django.shortcuts import redirect, render
from django.shortcuts import get_object_or_404, get_list_or_404
from django.views.decorators.cache import cache_page

from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.exceptions import ParseError

from cernforge.exceptions import (
    NonZeroReturnCode,
    Timeout
)

from cernforge.decorators import runAsynchronously
from cernforge.globals import (
    runCommand,
    errorHandler,
    useRestApiWithBasicAuth,
    ldap_authentication,
    JSONResponse,
    JIRA_REST_API,
    JIRA_BROWSE,
    JIRA_ADMIN,
    JIRA_MAIN,
    JIRA_PROJECTS_REST_API,
    EGROUP_REST_API,
    CONNECT_TO_ITS,
    BUNDLE,
)

from cernforge.settings import (
    DEV,
    DEV_EMAIL,
    FORGE_USER,
    FORGE_PASS,
    JIRA_SERVICE_EMAIL,
)


from cernforge import cernldap

from projecttable.utils import createFieldData, limitQueryset

from jira.forms import (
    JiraForm,
    JiraLinkedRepositoryForm
)

from jira.utils import (
    UpdateJiraProject,
    sync_linked_repositories,
    getPrivateSchemes,
)

from jira.models import (
    JiraProject,
    JiraProjectMember,
    JiraProjectRequest,
    JiraLinkedRepository,
    NotificationScheme,
    PermissionScheme,
    IssueTypeScreenScheme,
    ScreenScheme,
    WorkflowScheme,
    Category,
    IssueSecurity,
    ProjectType,
    ProjectTemplate,
    EgroupSchemes,
)

from jira.serializers import JiraProjectRequestSerializer, ExistingJiraProjectSerializer
from jira.log import jiralogger
from jira.mail import sendmail_repository_link_requested
from jira.utils import fix_git_repository_url

################################################################################
# @jiraForm view creates JIRA repository via html form
# JIRA projects are not actually created in db
# view just sends email to servicedesk and they create real project
################################################################################
@ldap_authentication
@api_view(["GET", "POST"])
@renderer_classes((TemplateHTMLRenderer, TemplateHTMLRenderer))
def jira_form(request, user):
    '''
        JIRA form to create new projects
    '''
    DEFAULT_NOTIFICATION_SCHEME = NotificationScheme.objects.filter(\
    name='Default Notification Scheme')[0]

    DEFAULT_WORKFLOW_SCHEME = WorkflowScheme.objects.filter(\
    name='CERN default workflow scheme')[0]

    DEFAULT_ISSUE_SCHEME = IssueTypeScreenScheme.objects.filter(\
    name='Default Issue Type Screen Scheme')[0]

    DEFAULT_CATEGORY = None

    DEFAULT_TYPE = ProjectType.objects.filter(\
    name='software')[0]

    DEFAULT_TEMPLATE = ProjectTemplate.objects.filter(\
    name='Basic software development')[0]

    if request.method == 'POST':
        post = request.POST.copy()

        # if user chose basic form, reset advanced fields to default values
        if post['is_basic'] == "True":
            post['notificationScheme'] = None if not DEFAULT_NOTIFICATION_SCHEME \
                else DEFAULT_NOTIFICATION_SCHEME.id
            post['workflowScheme'] = None if not DEFAULT_WORKFLOW_SCHEME else \
                DEFAULT_WORKFLOW_SCHEME.id
            post['issueTypeScreenScheme'] = None if not DEFAULT_ISSUE_SCHEME \
                else DEFAULT_ISSUE_SCHEME.id
            post['categoryId'] = None if not DEFAULT_CATEGORY \
                else DEFAULT_CATEGORY.id
            post['projectTypeKey'] = None if not DEFAULT_TYPE \
                else DEFAULT_TYPE.name
            post['projectTemplateKey'] = None if not DEFAULT_TEMPLATE \
                else DEFAULT_TEMPLATE.key

        post['key'] = post['key'].upper()

        form = JiraForm(post)

        if form.is_valid():

            # Do not save the project in the database, yet
            newProject = form.save(commit=False)
            newProjectToSerialize = post.copy()

            del newProjectToSerialize['workflowScheme']
            del newProjectToSerialize['issueTypeScreenScheme']
            del newProjectToSerialize['is_basic']
            #newProjectSerialized = json.dumps(newProjectToSerialize)

            result, data = useRestApiWithBasicAuth(url='%s/project' % JIRA_REST_API,
                                                   user=FORGE_USER,
                                                   password=FORGE_PASS,
                                                   method='POST',
                                                   objectr=newProjectToSerialize,
                                                   email_admins_prevent=[400])

            if result is False:
                # add validation errors sent by JIRA
                try:
                    #
                    # jiralogger(level='info',message=data,email_admins=False)
                    # data = json.loads(data)
                    for key, value in data['errors'].items():
                        form._errors[key] = value
                except Exception as exc:
                    jiralogger(
                        level='error',
                        message="EXCEPTION " + str(type(exc)) + " -> " + str(exc),
                        email_admins=True,
                    )

                    # Only if the error sent from JIRA (data) is not JSON.
                    # Normally a stack trace.
                    sendEmailToServiceDesk(user, newProject)

                    # ITS-286
                    return Response({
                        'displayName': user['fullname'],
                        'username': user['login'],
                        'admin': user['admin'],
                        'impersonate': user['impersonate'],
                        'error': "Thanks for your request, a ticket has been\
                        created to handle this problem. You'll be notified\
                        when your project will be ready.",
                        },
                                    template_name='cernforge/error_page.html',
                                    status=400,
                                   )

                jiralogger(level='info', message=json.dumps(data), email_admins=False)

                return Response(
                    {
                        'jira_main': JIRA_MAIN,
                        'displayName': user['fullname'],
                        'username': user['login'],
                        'admin': user['admin'],
                        'impersonate': user['impersonate'],
                        'form': form,
                        'error': 'Something went wrong, please correct the errors \
and try again. <a href=\'https://cern.service-now.com/u_request_fulfillment.do?sysparm_stack=u_request_fulfillment_list.do&sys_id=-1\'>Contact us</a> if you need any help',
                    },
                    template_name='jira/jira_form.html',
                    status=400,
                )

            if post['is_basic'] is False:
                # Change workflow
                workflow = {}
                workflow['workflowScheme'] = WorkflowScheme.objects.filter(
                    id=post['workflowScheme'])[0].name

                workflow['issueTypeScreenScheme'] = IssueTypeScreenScheme.objects.filter(
                    id=post['issueTypeScreenScheme'])[0].name

                result, data = useRestApiWithBasicAuth(url='%s%s' % (JIRA_PROJECTS_REST_API, post['name']),
                                                       user=FORGE_USER,
                                                       password=FORGE_PASS,
                                                       method='PUT',
                                                       objectr=workflow,
                                                       email_admins_prevent=[400])

                if result is False:

                    jiralogger(
                        level='error',
                        message="Project creation succesfull, workflow change failed",
                        email_admins=True,
                    )

                    return Response({
                        'displayName': user['fullname'],
                        'username': user['login'],
                        'admin': user['admin'],
                        'impersonate': user['impersonate'],
                        'error': "Your project has been created, but we couldn't change the workflow",
                    },
                                    template_name='cernforge/error_page.html',
                                    status=400,
                                   )

            #

            #data = json.loads(data)
            # Create the project in the DB (we assume that is not existing at this point)
            jiraProject = JiraProject(id=data['id'])
            jiraProject.key = data['key']
            jiraProject.name = post['name']
            jiraProject.owner_id = post['lead']
            jiraProject.save()

            #
            # Store the project to the database
            form.save()

            # ITS-289
            return Response(
                {
                    'displayName': user['fullname'],
                    'username': user['login'],
                    'admin': user['admin'],
                    'impersonate': user['impersonate'],
                    'success_message': mark_safe('JIRA project {name} was successfully created.<br/>You can find your newly created project <a href="{jira_browse}/{key}/">here</a>.<br/>You will find the adminsitration panel <a href="{jira_admin}/{key}">here</a>.'.format(
                        jira_browse=JIRA_BROWSE,
                        jira_admin=JIRA_ADMIN,
                        name=post['name'],
                        key=post['key'],
                        )
                                                ),
                },
                template_name='cernforge/success_page.html',
                status=200,
            )

        else:

            jiralogger(level='info', message='Form is invalid', email_admins=False)

            return Response(
                {
                    'jira_main': JIRA_MAIN,
                    'displayName': user['fullname'],
                    'username': user['login'],
                    'admin': user['admin'],
                    'impersonate': user['impersonate'],
                    'form': form,
                    'error': 'Something went wrong, please correct the errors and try again.',
                },
                template_name='jira/jira_form.html',
                status=400,
            )

    elif request.method == "GET":
        form = JiraForm(
            initial={
                'lead': user['login'],
                'notificationScheme':  DEFAULT_NOTIFICATION_SCHEME,
                'workflowScheme': DEFAULT_WORKFLOW_SCHEME,
                'issueTypeScreenScheme': DEFAULT_ISSUE_SCHEME,
                'categoryId': DEFAULT_CATEGORY,
                'projecttype': DEFAULT_TYPE,
                'projecttemplate': DEFAULT_TEMPLATE,
            }
        )

        return Response(
            {
                'jira_main': JIRA_MAIN,
                'displayName': user['fullname'],
                'username': user['login'],
                'admin': user['admin'],
                'impersonate': user['impersonate'],
                'form': form,
            },
            template_name='jira/jira_form.html',
            status=200,
        )

@ldap_authentication
@api_view(["GET", "POST"])
@renderer_classes((TemplateHTMLRenderer, TemplateHTMLRenderer))
def jira_main(request, user):
    '''
        JIRA form to create new projects
    '''

    return Response(
        {
            'jira_main': JIRA_MAIN,
            'displayName': user['fullname'],
            'username': user['login'],
            'admin': user['admin'],
            'impersonate': user['impersonate'],
        },
        template_name='jira/main.html',
        status=200,
    )

@ldap_authentication
@api_view(["GET"])
@renderer_classes((JSONRenderer, JSONRenderer))
def jira(request, user, api_version, pk=None):
    '''
        Shows all jira projects
    '''
    if request.method == 'GET':
        if pk == None:
            serializer = ExistingJiraProjectSerializer(JiraProject.objects.all(), many=True)
        else:
            repository = get_object_or_404(JiraProject, pk=pk)
            serializer = ExistingJiraProjectSerializer(instance=repository)

        return JSONResponse(serializer.data)


@ldap_authentication
#@api_view(["GET", "PUT"])
@api_view(["GET"])
@renderer_classes((JSONRenderer, JSONRenderer))
def project_issuetypescreenscheme(request, user, api_version, id=None):
    '''
        Shows/Modifies the Issue Type Screen scheme for a project
    '''

    if request.method == 'GET':
        return Response(
            {
                'id': data['id'],
                'name': data['name'],
            },
            status=200,
        )


@ldap_authentication
@api_view(["GET", "PUT"])
@renderer_classes((JSONRenderer, JSONRenderer))
def project_notificationscheme(request, user, api_version, id=None):
    '''
        Shows/Modifies the notification scheme for a project
    '''

    if request.method == 'GET':
        result, data = useRestApiWithBasicAuth(
            url='{0}/project/{1}/notificationscheme'.format(JIRA_REST_API, id),
            user=FORGE_USER,
            password=FORGE_PASS,
            method='GET',
            email_admins_prevent=[404],
        )

        if result:
            return Response(
                {
                    'id': data['id'],
                    'name': data['name'],
                },
                status=200,
            )
        else:
            return Response(
                {
                    'id': '0',
                    'name': 'None',
                },
                status=200,
            )
    elif request.method == 'PUT':

        if not userAllowedProject(user, id):
            return Response(
                {
                    'errors': "The user "+user['login']+" is not allowed to do this"
                },
                status=403,
            )
        else:
            new = JSONParser().parse(request)['NotificationScheme']
            objectr = {"notificationScheme":new}
            result, data = useRestApiWithBasicAuth(
                url='{0}/project/{1}/'.format(JIRA_REST_API, id),
                user=FORGE_USER,
                password=FORGE_PASS,
                method='PUT',
                objectr=objectr,
            )
            if not result:
                return Response(
                    {
                        'errors': ["The request failed", "Not implemented"],
                        'data': data
                    },
                    status=data['code'],
                )
            else:
                jiralogger(
                    level='info',
                    message='User ('+user['login']+'). Changed project: '+
                    id+' set new notificationScheme: '+new,
                    email_admins=False,
                )
                return Response(
                    {
                        'errors': "Every went fine"
                    },
                    status=200,
                )

@ldap_authentication
@api_view(["GET", "PUT"])
@renderer_classes((JSONRenderer, JSONRenderer))
def project_permissionscheme(request, user, api_version, id=None):
    '''
        Shows the notification scheme for a project
    '''

    if request.method == 'GET':
        result, data = useRestApiWithBasicAuth(
            url='{0}/project/{1}/permissionscheme'.format(JIRA_REST_API, id),
            user=FORGE_USER,
            password=FORGE_PASS,
            method='GET',
        )

        if result:
            return Response(
                {
                    'id': data['id'],
                    'name': data['name'],
                },
                status=200,
            )
        else:
            return Response(
                {
                    'id': "-1",
                    'name': 'None?',
                },
                status=200,
            )
    elif request.method == 'PUT':
        if not userAllowedProject(user, id):
            return Response(
                {
                    'error': "The user "+user['login']+" is not allowed to do this"
                },
                status=403,
            )
        else:
            new = JSONParser().parse(request)['PermissionScheme']
            objectr = {"id": new}
            result, data = useRestApiWithBasicAuth(
                url='{0}/project/{1}/permissionscheme'.format(JIRA_REST_API, id),
                user=FORGE_USER,
                password=FORGE_PASS,
                method='PUT',
                objectr=objectr,
            )
            if not result:
                return Response(
                    {
                        'errors': ["The request to ("+JIRA_REST_API+"/project/"+id+"/permissionscheme) failed"],
                        'data': data
                    },
                    status=400,
                )
            else:
                jiralogger(
                    level='info',
                    message='User ('+user['login']+'). Changed project: '+
                    id+' set new permissionscheme: '+new,
                    email_admins=False,
                )
                return Response(
                    {
                        'errors': "Every went fine"
                    },
                    status=200,
                )

@ldap_authentication
@api_view(["GET", "PUT"])
@renderer_classes((JSONRenderer, JSONRenderer))
def project_category(request, user, api_version, id=None):
    '''
        Shows the category of the given project
    '''

    if request.method == 'GET':
        result, data = useRestApiWithBasicAuth(
            url='{0}/project/{1}'.format(JIRA_REST_API, id),
            user=FORGE_USER,
            password=FORGE_PASS,
            method='GET',
        )

        if not result:
            return Response(
                {
                    'id': '0',
                    'name': 'None',
                },
                status=200,
            )
        else:
            try:
                return Response(
                    {
                        'id': data['projectCategory']['id'],
                        'name': data['projectCategory']['name'],
                    },
                    status=200,
                )
            except KeyError:
                return Response(
                    {
                        'id': '0',
                        'name': 'None',
                    },
                    status=200,
                )
    elif  request.method == 'PUT':
        if not userAllowedProject(user, id):
            return Response(
                {
                    'error': ["The user "+user['login']+" is not allowed to do this"]
                },
                status=403,
            )
        else:
            new = JSONParser().parse(request)['CategoryScheme']
            objectr = {'categoryId': new}
            result, data = useRestApiWithBasicAuth(
                url='{0}/project/{1}'.format(JIRA_REST_API, id),
                user=FORGE_USER,
                password=FORGE_PASS,
                method='PUT',
                objectr=objectr,
            )
            if not result:
                return Response(
                    {
                        'errors': ["The request to ("+JIRA_REST_API+"/project/"+id+") failed"],
                        'data': data
                    },
                    status=400,
                )
            else:
                jiralogger(
                    level='info',
                    message='User ('+user['login']+'). Changed project: '+
                    id+' set new categoryId: '+new,
                    email_admins=False,
                )
                return Response(
                    {
                        'errors': "Every went fine"
                    },
                    status=200,
                )

@ldap_authentication
@api_view(["GET", "PUT"])
@renderer_classes((JSONRenderer, JSONRenderer))
def project_issuesecuritylevelscheme(request, user, api_version=2, id=None):
    '''
        Shows the issuesecurity of the given project
    '''

    if request.method == 'GET':
        result, data = useRestApiWithBasicAuth(
            url='{0}/project/{1}/issuesecuritylevelscheme'.format(JIRA_REST_API, id),
            user=FORGE_USER,
            password=FORGE_PASS,
            method='GET',
            email_admins_prevent=[404],
        )

        if result:
            return Response(
                {
                    'id': data['id'],
                    'name': data['name'],
                },
                status=200,
            )
        else:
            try:
                return Response(
                    {
                        'id': data['id'],
                        'name': data['name'],
                    },
                    status=200,
                )
            except KeyError:
                return Response(
                    {
                        'id': '0',
                        'name': 'None',
                    },
                    status=200,
                )
    elif request.method == 'PUT':
        if not userAllowedProject(user, id):
            return Response(
                {
                    'error': "The user "+user['login']+" is not allowed to do this"
                },
                status=403,
            )
        else:
            new = JSONParser().parse(request)['IssuesecurityScheme']
            objectr = {"issueSecurityScheme": new}
            result, data = useRestApiWithBasicAuth(
                url='{0}/project/{1}'.format(JIRA_REST_API, id),
                user=FORGE_USER,
                password=FORGE_PASS,
                method='PUT',
                objectr=objectr,
            )
            if not result:
                return Response(
                    {
                        'errors': ["The request to ("+JIRA_REST_API+"/project/"+id+") failed"],
                        'data': data
                    },
                    status=400,
                )
            else:
                jiralogger(
                    level='info',
                    message='User ('+user['login']+'). Changed project: '+
                    id+' set new issueSecurityScheme: '+new,
                    email_admins=False,
                )
                return Response(
                    {
                        'errors': "Every went fine"
                    },
                    status=200,
                )
    # https://its.cern.ch/jira/rest/api/2/project/ id /issuesecuritylevelscheme


@ldap_authentication
@api_view(['GET'])
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def myprojects(request, user):
    '''
        This view displays all the projects where the user is either the owner,
        or one of the roles.
    '''

    field_names = [
        'key',
        'name',
        'owner_id',
        'roles',
        'details',  # To have a 'edit in forge' column
    ]

    # https://docs.djangoproject.com/en/1.7/ref/models/querysets/#field-lookups
    field_lookups = {
        'key': 'icontains',
        'name': 'icontains',
        'owner_id': 'icontains',
    }

    field_data = createFieldData(
        field_names=field_names,
        admin_fields=[],
        field_lookups=field_lookups,
        model=JiraProject,
        extra_dict={
            'roles': {
                'is_admin': False,
                'verbose_name': 'Your roles',
                'help_text': 'The JIRA roles you are a member of',
            },
            'details': {
                'verbose_name': 'Details',
                'help_text': 'Edit and view project schemes',
            }
        }
    )

    memberships = JiraProjectMember.objects.filter(
        Q(
            is_group='N',
            member_value=user['login'],
        ) |
        Q(
            is_group='Y',
            member_value__in=user['egroups'],
        )
    )

    type = request.GET.get('type')
    if type:
        if type == 'owner':
            projects = JiraProject.objects.filter(
                Q(id__in=set([m.project.id for m in memberships if m.role_value == 'Administrators'])) |
                Q(owner_id__exact=user['login'])
            )
    else:
        projects = JiraProject.objects.filter(
            Q(id__in=set([m.project.id for m in memberships])) |
            Q(owner_id__exact=user['login'])
        )

    projects = limitQueryset(
        queryset=projects,
        request=request,
        field_data=field_data,
    )

    for project in projects:

        roles = ['Owner'] if project.owner_id == user['login'] else list()
        is_in_admin_role = False
        for m in memberships:
            if m.project.id == project.id:
                if m.role_value not in roles:
                    roles.append(m.role_value)
                if m.role_value == 'Administrators':
                    is_in_admin_role = True

        project.roles = mark_safe('&nbsp;'.join(['<span class="aui-lozenge \
            aui-lozenge-subtle aui-lozenge-default">{0}</span>'.format(r)
                                                 for r in roles]))

        # Edit the link for 'id' with title 'Edit in forge'
        if user['login'] == project.owner_id or is_in_admin_role:
            project.details = mark_safe('<a href="{1}/{0}">Admin</a>'.format(project.key,
                                                                        JIRA_ADMIN))
        else:
            project.details = mark_safe('<a href="{1}/{0}">Browse</a>'.format(project.key,
                                                                         JIRA_BROWSE))

        project.key = mark_safe('<a href="/jira/myprojects/{0}">{0}</a>'.format(project.key))

    data = {
        'projecttable': {
            'projects': projects,
            'field_data': field_data,
            'field_names': field_names,
        },
        'admin': user['admin'],
    }

    if not request.is_ajax():
        data.update({
            'displayName': user['fullname'],
            'username': user['login'],
            'impersonate': user['impersonate'],
        })
        data['projecttable'].update({
            'showheader': True,
            'search_placeholder': 'Search in my JIRA projects ...',
            'sortable': True,
        })

        return Response(
            data,
            template_name='jira/jira_myprojects.html',
        )

    return JSONResponse(
        {
            'projecttable_tbody': loader.render_to_string(
                'projecttable/projecttable_tbody.html',
                data,
            ),
        },
        status=status.HTTP_200_OK,
    )


@ldap_authentication
@api_view(['GET'])
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def viewproject(request, user, id=None, key=None):
    '''
        Displays the give project by its id
    '''
    if id:
        data = get_object_or_404(JiraProject, id=id)
    elif key:
        data = get_object_or_404(JiraProject, key=key)
        id = data.id

    if not userAllowedProject(user, id):
        if type(user['login']) is type(None):
            error_msg = '401: You are not logged in'
        else:
            error_msg = "403: You ("+user['login']+") are not authorized to view this content"
        return Response(
            {
                'displayName': user['fullname'],
                'username': user['login'],
                'admin': user['admin'],
                'impersonate': user['impersonate'],
                'error': error_msg,
            },
            template_name='cernforge/error_page.html',
        )

    admins_array = JiraProjectMember.objects.filter(
        Q(
            role_value='Administrators',
            project_id=id,
        )
    )

    Uadmins = []
    Gadmins = []
    for admin in admins_array:
        if admin.is_group == 'N':
            Uadmins.append(admin.member_value)
        else:
            Gadmins.append(admin.member_value)

    schemes = {}

    if user['admin']:
        schemes['Notification'] = get_list_or_404(NotificationScheme)
        schemes['Permission'] = get_list_or_404(PermissionScheme)
        schemes['Issuesecurity'] = get_list_or_404(IssueSecurity)
    else:
        schemes['Notification'] = get_list_or_404(NotificationScheme, is_public='Y')+getPrivateSchemes(user, 'N')
        schemes['Permission'] = get_list_or_404(PermissionScheme, is_public='Y')+getPrivateSchemes(user, 'P')
        schemes['Issuesecurity'] = get_list_or_404(IssueSecurity, is_public='Y')+getPrivateSchemes(user, 'S')

    # Sorting
    schemes['Notification'] = sorted(schemes['Notification'], key=lambda noti: noti.name)

    schemes['Permission'] = sorted(schemes['Permission'], key=lambda noti: noti.name)

    schemes['Issuesecurity'] = sorted(schemes['Issuesecurity'], key=lambda noti: noti.name)

    schemes['IssueTypeScreenScheme'] = get_list_or_404(IssueTypeScreenScheme)
    schemes['Category'] = get_list_or_404(Category)

    data.category = 'None'
    for cat in schemes['Category']:
        if data.categoryid == cat.id:
            data.category = cat.name
            data.categoryId = cat.id

    return Response(
        {
            'displayName': user['fullname'],
            'username': user['login'],
            'admin': user['admin'],
            'impersonate': user['impersonate'],
            'data': data,
            'Uadmins': Uadmins,
            'Gadmins': Gadmins,
            'schemes': schemes,
            'jira_browse': JIRA_BROWSE,
            'jira_admin': JIRA_ADMIN,
        }, template_name='jira/viewproject.html')


@ldap_authentication
@api_view(['GET'])
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def allprojects(request, user):
    '''
        This view displays all Jira instances.
    '''

    field_names = (
        'id',
        'key',
        'name',
        'owner_id',
    )

    admin_fields = ['id']

    # https://docs.djangoproject.com/en/1.7/ref/models/querysets/#field-lookups
    field_lookups = {
        'key': 'icontains',
        'name': 'icontains',
        'owner_id': 'icontains',
    }

    field_data = createFieldData(
        field_names=field_names,
        admin_fields=admin_fields,
        field_lookups=field_lookups,
        model=JiraProject,
    )

    projects = limitQueryset(
        queryset=JiraProject.objects.all(),
        request=request,
        field_data=field_data,
    )

    for project in projects:
        if user['login'] == project.owner_id or user['admin']:
            project.key = mark_safe('<a href="{1}/{0}">{0}</a>'.format(project.key, JIRA_BROWSE))
            project.id = mark_safe('<a href="/jira/myprojects/{0}">{0}</a>'.format(project.id))

    data = {
        'projecttable': {
            'projects': projects,
            'field_data': field_data,
            'field_names': field_names,
        },
        'admin': user['admin'],
    }

    if not request.is_ajax():
        data.update({
            'displayName': user['fullname'],
            'username': user['login'],
            'impersonate': user['impersonate'],
        })

        data['projecttable'].update({
            'showheader': True,
            'search_placeholder': 'Search in all JIRA projects ...',
            'sortable': True,
        })

        return Response(
            data,
            template_name='jira/jira_allprojects.html',
        )
    else:
        return JSONResponse(
            {
                'projecttable_tbody': loader.render_to_string(
                    'projecttable/projecttable_tbody.html',
                    data,
                ),
            },
            status=status.HTTP_200_OK,
        )

@ldap_authentication
@api_view(['GET', 'POST'])
@renderer_classes((TemplateHTMLRenderer, TemplateHTMLRenderer))
def linkrep_request(request, user):


    return Response(
        {
            'displayName': user['fullname'],
            'username': user['login'],
        },
        template_name='jira/linked-repository/request-blocked.html',
    )

@ldap_authentication
@api_view(['POST'])
@renderer_classes((TemplateHTMLRenderer, TemplateHTMLRenderer))
def linkrep_remove(request, user):

    # Remove when FORGE-64
    if not user['admin']:
        return redirect('/')

    respdata = {
        'message_type': 'error',
        'message_title': 'Unauthorized',
        'message_body': 'You are not authorized to visit this page.',
    }
    status = 401

    if user['admin']:
        link_id = request.POST['link_id']
        linked_rep = JiraLinkedRepository.objects.get(pk=link_id)

        command = "{connect_its} 'if [ -d \"{repository_path}\" ]; then rm -rf \"{repository_path}\"; fi'".format(
            connect_its=CONNECT_TO_ITS,
            repository_path=linked_rep.repository_path,
        )
        try:
            runCommand(
                command=command,
                timeout=60.0,
                silentFail=False,
                shell=True,
            )
        except NonZeroReturnCode:
            jiralogger(
                level='error',
                message='Falied to remove GIT repository ('+linked_rep.repository_path+')in JIRA',
                email_admins=True,
            )

        linked_rep.status = 'deleted'
        linked_rep.save()

        respdata = {
            'message_type': 'success',
            'message_title': 'Repository link successfully removed',
            'message_body':
                mark_safe("""
                        <p>The repository {repository_url} (type {repository_type}) has been successfully removed from the JIRA server, and set as deleted in CERN Forge DB.</p>
                        <p><strong>NB:</strong> You still have to remove the repository from the JIRA web interface. You can find instructions on how to do this <a href="{linkrep_twikidoc}">here</a>.</p>
                    """.format(
                        repository_url=linked_rep.repository_url,
                        repository_type=linked_rep.repository_type,
                        linkrep_twikidoc='https://twiki.cern.ch/twiki/bin/viewauth/IT/ITSTechnicalDoc#Link_repository',
                    )
                         )
        }
        status = 200

    respdata.update({
        'admin': user['admin'],
        'displayName': user['fullname'],
        'username': user['login'],
        'impersonate': user['impersonate'],
    })
    return render(
        request,
        'jira/linked-repository/remove.html',
        respdata,
        status=status,
    )

@ldap_authentication
@api_view(['GET'])
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def linkrep_list(request, user):
    '''
        Display a list of all linked repositories
    '''

    # Re-sync linked repository list if it hasn't been synced for 60 seconds
    syncfile = '/tmp/sync_linked_repositories'
    create_syncfile = False
    #if os.path.isfile(syncfile):
    #    if calendar.timegm(time.gmtime()) - os.path.getmtime(syncfile) > 60:
    #        create_syncfile = True
    #else:
    #    create_syncfile = True

    if create_syncfile:
        with open(syncfile, 'w') as f:
            myfile = File(f)
            myfile.write('Created or updated: {0}'.format(time.strftime("%Y-%m-%d %H:%M:%S")))
        jiralogger(
            level='info',
            message='Synchronizing linked repositories START',
            email_admins=False,
        )
        sync_linked_repositories()
        jiralogger(
            level='info',
            message='Synchronizing linked repositories END',
            email_admins=False,
        )

    field_names = (
        'link_id',
        'repository_type',
        'repository_url',
        'repository_path',
        'last_commit_info',
        'requestor',
        'request_date',
        'remove', # Custom field, not in JiraLinkedRepository
    )

    admin_fields = (
        'link_id',
        'repository_path',
    )

    # https://docs.djangoproject.com/en/1.7/ref/models/querysets/#field-lookups
    field_lookups = {
        'repository_url': 'icontains',
        'requestor': 'exact',
        'request_date': 'contains',
    }

    field_data = createFieldData(
        field_names=field_names,
        admin_fields=admin_fields,
        field_lookups=field_lookups,
        model=JiraLinkedRepository,
        extra_dict={
            'remove': {
                'is_admin': True,
                'verbose_name': 'Delete link',
                'help_text': "Remove the repository from the JIRA server and \
                    set the status as 'deleted' in the local DB.",
            }
        }
    )

    projects = limitQueryset(
        queryset=JiraLinkedRepository.objects.filter(~Q(status='deleted')),
        request=request,
        field_data=field_data,
    )

    for project in projects:
        # IMPORTANT: Mask password !!
        project.repository_url = re.sub(r'([^/@]+)@', '\1', project.repository_url)

        if project.repository_type == 'git' and len(project.last_commit_info) > 6:
            project.last_commit_info = project.last_commit_info[:6]
        project.last_commit_info = mark_safe('<pre>{0}</pre>'.format(project.last_commit_info))

        project.repository_type = mark_safe('<span class="aui-lozenge aui-lozenge-subtle aui-lozenge-default">{0}</span>'.format(project.repository_type))
        project.remove = mark_safe('<form method="POST" action="/jira/linked-repository/remove/"><input type="hidden" name="link_id" value="{link_id}"/><input type="submit" value="Remove" /></form>'.format(
            link_id=project.link_id,
        ))

    data = {
        'projecttable': {
            'projects': projects,
            'field_data': field_data,
            'field_names': field_names,
        },
        'admin': user['admin'],
    }

    if not request.is_ajax():
        data.update({
            'displayName': user['fullname'],
            'username': user['login'],
            'impersonate': user['impersonate'],
        })

        data['projecttable'].update({
            'showheader': True,
            'search_placeholder': 'Search in all linked repositories linked to JIRA ...',
            'sortable': True,
        })

        return Response(
            data,
            template_name='jira/linked-repository/list.html',
        )
    else:
        return JSONResponse(
            {
                'projecttable_tbody': loader.render_to_string(
                    'projecttable/projecttable_tbody.html',
                    data,
                ),
            },
            status=status.HTTP_200_OK,
        )

#@runAsynchronously
#def link_repository_async(username, jira_linked_repository):
#    j = jira_linked_repository
#    try:
#        if j.repository_type == 'git':
#            runCommand(
#                command="{connect_its} 'su tomcat -s /bin/sh -c \"git clone --mirror {repository_url} {repository_path}\"'".format(
#                    connect_its=CONNECT_TO_ITS,
#                    repository_url=j.repository_url,
#                    repository_path=j.repository_path,
#                ),
#                timeout=None,
#                silentFail=False,
#                shell=True,
#            )
#    except NonZeroReturnCode:
#        pass
#    else:
#        j.status = 'active'
#        j.save()

################################################################################
# @synchronize_jira_project view synchronize info about Jira Project in db
################################################################################
@ldap_authentication
@api_view(["GET", "POST"])
@renderer_classes((TemplateHTMLRenderer, TemplateHTMLRenderer))
def synchronize_jira_project(request, user):

    if request.method == 'GET':
        return Response(
            {
                'displayName': user['fullname'],
                'username': user['login'],
                'admin': user['admin'],
            },
            template_name='jira/sync_jira_project.html',
        )

    elif request.method == 'POST':

        return_data = {
            'displayName': user['fullname'],
            'username': user['login'],
            'admin': user['admin'],
            'impersonate': user['impersonate'],
        }

        error_msg = None
        key_or_id = request.POST.get('key_or_id')
        key_or_id = key_or_id.upper()
        if key_or_id:
            result, data = useRestApiWithBasicAuth(
                url='{0}{1}'.format(JIRA_PROJECTS_REST_API, key_or_id),
                user=FORGE_USER,
                password=FORGE_PASS,
                method='GET',
            )
            if result:
                key = data['key']
                id = data['id']
                try:
                    UpdateJiraProject(projectKey=key, projectId=id, projectData=data)
                except Exception as exception:
                    error_msg = 'Could not syncronize project {0} ({1}) - Internal problem.'.format(key, id)
                    jiralogger(
                        level='error',
                        message='UpdateJiraProject {0}: {1} {2}'.format(type(exception).__name__,
                                                                        exception, traceback.format_exc()),
                        email_admins=True,
                    )
                else:
                    return_data.update({'success_message': 'JIRA Project {0} ({1}) successfully synchronized.'.format(key, id)})
                    return Response(
                        return_data,
                        template_name='cernforge/success_page.html',
                    )

            else:
                error_msg = 'This project ({0}) does not exist'.format(key_or_id)

        else:
            error_msg = 'No key or ID received, try again'

        return_data.update({'error': error_msg})
        return Response(
            return_data,
            template_name='jira/sync_jira_project.html',
            status=status.HTTP_400_BAD_REQUEST,
        )

@ldap_authentication
@api_view(["GET"])
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def issuesecurityschemes(request, user, api_version=2):
    '''
    List all issuesecurityschemes
    '''
#         https://its.cern.ch/jira/rest/api/2/issuesecurityschemes/

    if user['admin']:
        projects = get_list_or_404(IssueSecurity)
    else:
        projects = get_list_or_404(IssueSecurity, is_public='Y')+getPrivateSchemes(user, 'S')

    projects = sorted(projects, key=lambda noti: noti.name)

    return Response(
        {
            'displayName': user['fullname'],
            'username': user['login'],
            'admin': user['admin'],
            'impersonate': user['impersonate'],
            'permissions': projects,
        },
        template_name='jira/issuesecurityschemes.html',
    )

@ldap_authentication
@api_view(["GET"])
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def issuesecurityscheme(request, user, id, api_version=2):

#    permissionNames = getPermissionNames()
#    roleNames = getRoleNames()

    if id == 0:
        result = False
    else:
        result, data = useRestApiWithBasicAuth(
            url='{0}/issuesecurityschemes/{1}'.format(JIRA_REST_API, id),
            user=FORGE_USER,
            password=FORGE_PASS,
            method='GET',
            email_admins_prevent=[404],
        )

    if result:
        rdata = []
        for prm in data['levels']:
            default = False
            if str(data['defaultSecurityLevelId']) == prm['id']:
                default = True
            rdata.append({'name': prm['name'], 'description': prm['description'],
                          'default': default})

        return Response(
            {
                'displayName': user['fullname'],
                'username': user['login'],
                'admin': user['admin'],
                'impersonate': user['impersonate'],
                'schemename': data['name'],
                'permissions': rdata,
            },
            template_name='jira/issuesecurityscheme.html',
        )
    else:
        rdata = [{'name':'Issue security is currently not enabled for this project.',
                 }]
        return Response(
            {
                'displayName': user['fullname'],
                'username': user['login'],
                'admin': user['admin'],
                'impersonate': user['impersonate'],
                'schemename': 'Issue security is currently not enabled for this project.',
                'permissions': rdata,
            },
            template_name='jira/issuesecurityscheme.html',
        )

@ldap_authentication
@api_view(["GET"])
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def categories(request, user, id=None, api_version=2):
    '''
    List all project categories
    '''

    if id is None:
        data = sorted(get_list_or_404(Category), key=lambda noti: noti.name)

        projects = []
        for pr in data:
            projects.append({'name': pr.name, 'id': pr.id, 'description': pr.description})

        return Response(
            {
                'displayName': user['fullname'],
                'username': user['login'],
                'admin': user['admin'],
                'impersonate': user['impersonate'],
                'permissions': projects,
            },
            template_name='jira/categories.html',
        )

    else:
        data = JiraProject.objects.filter(Q(categoryid=id))

        try:
            category = Category.objects.get(id=id)
        except Category.DoesNotExist:
            return Response(
                {
                    'displayName': user['fullname'],
                    'username': user['login'],
                    'admin': user['admin'],
                    'impersonate': user['impersonate'],
                    'title': 'None',
                    'permissions': None,
                },
                template_name='jira/category.html',
            )

        projects = []
        for pr in data:
            projects.append({'name': pr.name, 'key': pr.key})

        projects = sorted(projects, key=lambda p: p['key'])

        return Response(
            {
                'displayName': user['fullname'],
                'username': user['login'],
                'admin': user['admin'],
                'impersonate': user['impersonate'],
                'title': category.name,
                'permissions': projects,
            },
            template_name='jira/category.html',
        )


@ldap_authentication
@api_view(["GET", "POST"])
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def permissionschemes(request, user, api_version=2):

    if user['admin']:
        projects = get_list_or_404(PermissionScheme)
    else:
        projects = get_list_or_404(PermissionScheme, is_public='Y')+getPrivateSchemes(user, 'P')

    projects = sorted(projects, key=lambda noti: noti.name)

    return Response(
        {
            'displayName': user['fullname'],
            'username': user['login'],
            'admin': user['admin'],
            'impersonate': user['impersonate'],
            'permissions': projects,
        },
        template_name='jira/permissionschemes.html',
    )


#@cache_page(9000) # 15min
@ldap_authentication
@api_view(["GET", "POST"])
@renderer_classes((TemplateHTMLRenderer, TemplateHTMLRenderer))
def permissionscheme(request, id, user):
    '''
        API entry for showing a permission scheme
    '''
    permissionNames = getPermissionNames()
    roleNames = getRoleNames()

    data = cache.get('permissionscheme_%s' % id)
    if not data:
        result, data = useRestApiWithBasicAuth(
            url='{0}/permissionscheme/{1}?expand=permissions'.format(JIRA_REST_API, id),
            user=FORGE_USER,
            password=FORGE_PASS,
            method='GET',
        )

        if result is False:
            return Response(
                {
                    'displayName': user['fullname'],
                    'username': user['login'],
                    'admin': user['admin'],
                    'impersonate': user['impersonate'],
                    'schemename': str(data['code']) + " - cannot access JIRA's API",
                    'permissions': None,
                },
                template_name='jira/permissionscheme.html',
            )
        cache.set('permissionscheme_%s' % id, data, 5*60)

    rdata = {}
    for prm in data['permissions']:
        rdata[prm['permission']] = {
            'title': permissionNames[prm['permission']]['name'],
            'description': permissionNames[prm['permission']]['description'],
            'permissions': []
        }

    for prm in data['permissions']:
        rdata[prm['permission']]['permissions'].append(prettyHolder(prm['holder'], roleNames))
#
#        prm['holder']

    return Response(
        {
            'displayName': user['fullname'],
            'username': user['login'],
            'admin': user['admin'],
            'impersonate': user['impersonate'],
            'schemename': data['name'],
            'permissions': rdata,
        },
        template_name='jira/permissionscheme.html',
    )

def prettyHolder(json, roleNames):

    if json['type'] == 'projectRole':
        to_return = 'Project Role: %s' % roleNames[json['parameter']]
    elif json['type'] == 'projectLead':
        to_return = 'Project lead'
    elif json['type'] == 'reporter':
        to_return = 'Reporter'
    elif json['type'] == 'group':
        to_return = 'Group: %s' % json['parameter']
    elif json['type'] == 'assignee':
        to_return = 'Assignee'
    elif json['type'] == 'anyone':
        to_return = 'Anyone'
    elif json['type'] == 'applicationRole':
        to_return = 'Application Role: Any logged in user'
    elif json['type'] == 'user':
        to_return = 'Single User: %s' % json['parameter']
    else:
        to_return = json

    return to_return


def getPermissionNames():
    '''
        Aks to the API the names of all Permission names (p.e.: Modify Reporter)
    '''
    result, data = useRestApiWithBasicAuth(
        url='{0}/permissions/'.format(JIRA_REST_API),
        user=FORGE_USER,
        password=FORGE_PASS,
        method='GET',
    )
    try:
        return data['permissions']
    except KeyError:
        return {}


def getRoleNames():

    result, roles = useRestApiWithBasicAuth(
        url='{0}/role/'.format(JIRA_REST_API),
        user=FORGE_USER,
        password=FORGE_PASS,
        method='GET',
    )

    rdata = {}
    if result:
        for role in roles:
            rdata[str(role['id'])] = role['name']

    return rdata

@ldap_authentication
@api_view(["GET", "POST"])
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def notificationschemes(request, user, api_version=2):

    if user['admin']:
        notifications = get_list_or_404(NotificationScheme)
    else:
        notifications = get_list_or_404(NotificationScheme, is_public='Y')+getPrivateSchemes(user, 'N')

    notifications = sorted(notifications, key=lambda noti: noti.name)

    if not request.is_ajax():
        return Response(
            {
                'displayName': user['fullname'],
                'username': user['login'],
                'admin': user['admin'],
                'impersonate': user['impersonate'],
                'notifications': notifications,
            },
            template_name='jira/notificationschemes.html',
        )

    return JSONResponse(
        {
            'projecttable_tbody': loader.render_to_string(
                'projecttable/projecttable_tbody.html',
                data,
            ),
        },
        status=status.HTTP_200_OK,
    )

#@cache_page(9000) # 15min
@ldap_authentication
@api_view(["GET", "POST"])
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def notificationscheme(request, id, user, api_version=2):

#        permissionNames = getPermissionNames()
#        roleNames = getRoleNames()

    data = cache.get('notificationscheme_%s' % id)
    if not data:
        result, data = useRestApiWithBasicAuth(
            url='{0}/notificationscheme/{1}?expand=all'.format(JIRA_REST_API, id),
            user=FORGE_USER,
            password=FORGE_PASS,
            method='GET',
            email_admins_prevent=[404],
        )

        if not result:
            return Response(
                {
                    'displayName': user['fullname'],
                    'username': user['login'],
                    'admin': user['admin'],
                    'impersonate': user['impersonate'],
                    'schemename': 'None',
                    'permissions': '',
                },
                template_name='jira/notificationscheme.html',
            )

        cache.set('notificationscheme_%s' % id, data, 5*60)

    rdata = []
    for prm in data['notificationSchemeEvents']:
        entry = {'title': prm['event']['name'],
                }

        entry['notifications'] = []
        for noti in prm['notifications']:
            if noti['notificationType'] == 'ProjectRole':
                entry['notifications'].append('Project Role: '+noti['projectRole']['name'])
            elif noti['notificationType'] == 'GroupCustomField':
                entry['notifications'].append('Group Custom Field: '+noti['field']['name'])
            elif noti['notificationType'] == 'Group':
                entry['notifications'].append('Group: '+noti['group']['name'])
            else:
                entry['notifications'].append(noti['notificationType'])

        rdata.append(entry)

    return Response(
        {
            'displayName': user['fullname'],
            'username': user['login'],
            'admin': user['admin'],
            'impersonate': user['impersonate'],
            'schemename': data['name'],
            'permissions': rdata,
        },
        template_name='jira/notificationscheme.html',
    )

################################################################################
# @mapEgroup view for mapping or syncing egroups to JIRA
################################################################################
@ldap_authentication
@api_view(["GET", "POST"])
@renderer_classes((TemplateHTMLRenderer, TemplateHTMLRenderer))
def egroup(request, action, user):

    template_name = 'jira/egroup_{0}.html'.format(action)
    template_name_success = 'jira/egroup_{0}_success.html'.format(action)

    return_data = {
        'displayName': user['fullname'],
        'username': user['login'],
        'admin': user['admin'],
        'impersonate': user['impersonate'],
    }

    if request.method == 'GET':
        return Response(
            return_data,
            template_name=template_name,
        )

    elif request.method == 'POST':

        egroup = request.POST.get('egroup')

        error_msg = None
        if not cernldap.egroup_exist(egroup):
            error_msg = 'E-group {0} does not exist.'.format(egroup)
            error_status = status.HTTP_400_BAD_REQUEST
        elif not cernldap.egroup_exist_with_user_members(egroup):
            error_msg = 'E-group {0} does not have any members.'.format(egroup)
            error_status = status.HTTP_400_BAD_REQUEST

        if not error_msg:
            try:
                rresult = requests.get(
                    url='{0}/{1}?eGroup={2}'.format(EGROUP_REST_API, action, egroup),
                    auth=(FORGE_USER, FORGE_PASS),
                    timeout=15,
                    verify=BUNDLE,
                )
            except requests.exceptions.SSLError:
                error_msg = 'The request to the JIRA instance got a SSLError'
                error_status = status.HTTP_400_BAD_REQUEST
            except requests.exceptions.Timeout:
                error_msg = 'The request to %s timed out. Please try again later.' % EGROUP_REST_API,
                error_status = status.HTTP_408_REQUEST_TIMEOUT
            else:
                if rresult.status_code == 200:
                    return_data.update({'egroup': egroup})
                    return Response(
                        return_data,
                        template_name=template_name_success,
                    )
                else:
                    # Hacky, I know ...
                    if rresult.status_code == 400 and 'mapping already exists' in rresult.text.lower():
                        error_msg = 'The mapping already exists'
                        error_status = status.HTTP_400_BAD_REQUEST
                    else:
                        jiralogger(
                            level='error',
                            message="Error {0}ing e-group '{1}'. Response code: {2}, Response: {3}".format(action, egroup, rresult.status_code, rresult.text),
                            email_admins=True,
                        )

                        # Redirect to error page to try to prevent spamming
                        # email warnings to the administrators.
                        return errorHandler(
                            request=request,
                            user=user,
                            error="An error occurred synchronising e-group '{0}' in JIRA. The CERN Forge administrators has been notified. Please try again at a later time.".format(egroup),
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                        )

        # If we have gotten so far, it means that there's a error_msg set
        return_data.update({'error': error_msg})
        return Response(
            return_data,
            template_name=template_name,
            status=error_status,
        )

def userAllowedProject(user, id):

    if type(user['login']) is type(None):
        return False

    admins_array = JiraProjectMember.objects.filter(
        Q(
            role_value='Administrators',
            project_id=id,
        )
    )

    Uadmins = []
    for admin in admins_array:
        if admin.is_group == 'N':
            Uadmins.append(admin.member_value)
        else:
            for gadmin in cernldap.get_egroup_members(admin.member_value):
                Uadmins.append(gadmin)

    data = get_object_or_404(JiraProject, id=id)

    jiralogger(
        level='info',
        message='User ('+user['login']+'). Data.owner is '+ data.owner_id+ \
            '. And Adminusers are '+str(Uadmins),
        email_admins=False,
    )


    if user['login'] not in Uadmins and user['login'] != data.owner_id and not \
        user['admin']:
        return False

    return True

def sendEmailToServiceDesk(user, project):
    '''
        Sends an email to Service Desk
    '''
    to_address = JIRA_SERVICE_EMAIL if not DEV else DEV_EMAIL
    subject, from_email = 'Jira project request (automatic API failed): ' + \
        project.name + ' by ' + user['fullname'], user['email']
    text_content = ' Project name: ' + project.name + ".\n Project key: " + \
        project.key + ".\n User: " + user['fullname']+ \
        ".\n\n NotificationScheme: " + project.notificationScheme + \
        ".\n WorkflowScheme: " + project.workflowScheme + \
        ".\n IssueTypeScreenScheme: " + project.issueTypeScreenScheme + \
        ".\n Category: " + str(project.categoryId) + ".\n"

    html_content = '<strong>Username</strong>: ' + user['fullname']+ \
        '</br><strong>Project name</strong>: ' + project.name + \
        '</br><strong>Project key</strong>: ' + project.key+ \
        '</br><strong>NotificationScheme</strong>: ' + \
        project.notificationScheme+ '</br><strong>WorkflowScheme</strong>: ' \
        + project.workflowScheme + \
        '</br><strong>IssueTypeScreenScheme</strong>: ' + \
        project.issueTypeScreenScheme+ '</br><strong>Category</strong>: ' + \
        str(project.categoryId)
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to_address])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

@ldap_authentication
@api_view(["GET", "POST"])
@renderer_classes((TemplateHTMLRenderer, TemplateHTMLRenderer))
def schemesaccess(request, user):
    '''
        Give or remove access to an egroup for a Scheme
    '''

    mappings = EgroupSchemes.objects.all().order_by('id')
    notificationschemes = NotificationScheme.objects.all()
    permissionschemes = PermissionScheme.objects.all()
    issuesecurity = IssueSecurity.objects.all()

    for map in mappings:
        try:
            if map.schemeType == 'N':
                map.name = notificationschemes.get(id=map.id)
            elif map.schemeType == 'P':
                map.name = permissionschemes.get(id=map.id)
            elif map.schemeType == 'S':
                map.name = issuesecurity.get(id=map.id)
        except IssueSecurity.DoesNotExist:
            map.name = 'DoesNotExist'

    return Response({
        'displayName': user['fullname'],
        'username': user['login'],
        'admin': user['admin'],
        'impersonate': user['impersonate'],
        'mappings': mappings,
        'notificationschemes': notificationschemes,
        'permissionschemes': permissionschemes,
        'issuesecurity': issuesecurity,
        },
                    template_name='jira/schemes_access.html',
                    status=200,
                   )

@ldap_authentication
@api_view(["GET", "POST", "DELETE"])
@renderer_classes((JSONRenderer, JSONRenderer))
def schemeegroups(request, user, api_version):
    '''
        Create and remove links between schemes and egroups
    '''

    if not user['admin']:
        return JSONResponse(status=status.HTTP_403_FORBIDDEN)

    try:
        jsonRequest = JSONParser().parse(request)
    except ParseError:
        return JSONResponse(
            {'errors': 'Error, cannot parse the JSON'},
            status=status.HTTP_400_BAD_REQUEST,
        )

    try:
        if request.method == 'POST':
            EgroupSchemes.objects.create(schemeType=jsonRequest['type'],
                                         id=jsonRequest['id'],
                                         egroup=jsonRequest['egroup'])
            return JSONResponse(
                {'id': jsonRequest['id'],
                 'type': jsonRequest['type'],
                 'egroup': jsonRequest['egroup']},
                status=status.HTTP_200_OK,
            )
        elif request.method == 'DELETE':
            for egs in EgroupSchemes.objects.filter(Q(schemeType=jsonRequest['type'],
                                                      id=jsonRequest['id'],
                                                      egroup=jsonRequest['egroup'])):
                egs.delete()
            return JSONResponse(
                {'id': jsonRequest['id'],
                 'type': jsonRequest['type'],
                 'egroup': jsonRequest['egroup']},
                status=status.HTTP_200_OK,
            )
    except KeyError as keye:
        return JSONResponse(
            {'errors': 'Error, cannot find value in json %s' % str(keye)},
            status=status.HTTP_400_BAD_REQUEST,
        )
    except Exception as exc:
        return JSONResponse(
            {'errors': 'Generic error %s' % str(exc)},
            status=status.HTTP_400_BAD_REQUEST,
        )

