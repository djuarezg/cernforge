'''
    http://stackoverflow.com/a/1112236/945506
'''

import re
from django import template
from django.conf import settings

NUMERIC_TEST = re.compile(r"^\d+$")
register = template.Library()

def getattribute(value, arg):
    """Gets an attribute of an object dynamically from a string name"""

    if hasattr(value, str(arg)):
        return getattr(value, arg)
    elif hasattr(value, 'has_key') and value.has_key(arg):
        return value[arg]
    elif NUMERIC_TEST.match(str(arg)) and len(value) > int(arg):
        return value[int(arg)]
    #else:
    return '?'

register.filter('getattribute', getattribute)
