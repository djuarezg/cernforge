'''
    Utils for project Tables
'''
import re
import operator

from django.template import loader
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.core import exceptions

from functools import reduce

from rest_framework import status
from rest_framework.response import Response

#from cernforge.globals import JSONResponse
#from cernforge.log import forgelogger

def limitQueryset(queryset, request, field_data, is_admin=False):
    '''
        Make a limited query, paginated (?)
    '''
    # Optionally searching the queryset
    search_string = request.GET.get('q')
    if search_string:
        queries = list()
        for field_name, field_data_item in field_data.items():
            if 'field_lookup' in field_data_item and (not field_data_item['is_admin'] or is_admin):
                queries.append(
                    Q(**{'{0}__{1}'.format(field_name,
                                           field_data_item['field_lookup']): search_string})
                )
        qgroup = reduce(operator.or_, queries)
        queryset = queryset.filter(qgroup)

    # Optionally sorting the queryset
    orderby = request.GET.get('orderby')
    if orderby:
        match = re.match(r'^(?P<col_id>.+)_(?P<orderby_direction>asc|desc)$', orderby)
        if match:
            col_id = match.group('col_id')
            orderby_direction = match.group('orderby_direction')
            order_by = col_id if orderby_direction != 'desc' else '-' + col_id
            queryset = queryset.order_by(order_by)

    # Creating the pagination
    try:
        perpage = int(request.GET.get('perpage'))
    except (TypeError, ValueError):
        perpage = 10

    try:
        page = int(request.GET.get('page'))
    except (TypeError, ValueError):
        page = 1

    paginator = Paginator(queryset, perpage)

    try:
        queryset_paginated = paginator.page(page)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        page = paginator.num_pages
        queryset_paginated = paginator.page(page)

    start_page = page - 2 if page - 2 >= 1 else 1
    end_page = page + 3 if page + 3 <= paginator.num_pages + 1 else paginator.num_pages + 1
    queryset_paginated.paginator.nearby_pages = range(start_page, end_page, 1)
    queryset_paginated.paginator.page = page
    queryset_paginated.paginator.perpage = perpage

    return queryset_paginated

def createFieldData(field_names, admin_fields, field_lookups, model, extra_dict=None):
    '''
        Given a list of fields, it returns a single field data structure
    '''
    field_data = dict()
    for field_name in field_names:
        try:
            field = model._meta.get_field(field_name)
            field_data[field_name] = {
                'is_admin': field_name in admin_fields,
                'verbose_name': field.verbose_name,
                'help_text': field.help_text,
            }
        except exceptions.FieldDoesNotExist:
            pass
        if field_name in field_lookups:
            field_data[field_name]['field_lookup'] = field_lookups[field_name]

    if extra_dict:
        for key, val in extra_dict.items():
            field_data[key] = val

    return field_data
