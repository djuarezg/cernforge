#!/usr/bin/env python
'''
    Script to run the tests
'''

import os
import sys

import django
from django.conf import settings
from django.test.utils import get_runner

if __name__ == "__main__":
    os.environ['DJANGO_SETTINGS_MODULE'] = 'tests.test_settings'
    django.setup()
    TESTRUNNER = get_runner(settings)
    TEST_RUNNER = TESTRUNNER()
    FAILURES = TEST_RUNNER.run_tests(["tests"])
    sys.exit(bool(FAILURES))
