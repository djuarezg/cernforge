'''
    Admin pages set up
'''
from django.contrib import admin

from svn.models import SvnProject
from svn.forms import SvnAdminForm

class SvnProjectAdmin(admin.ModelAdmin):
    '''
        Class to set up admin pages
    '''
    form = SvnAdminForm

    # Which attributes are shown in the overview listing SVN projects
    list_display = (
        'shortname',
        'create_date',
        'longname',
        'requestor',
        'libuser',
        'status',
    )

    # Which attributes can be searched in
    search_fields = (
        'shortname',
        'longname',
        'requestor',
        'libuser',
        'status',
    )

    list_filter = ['create_date']

    date_hierarchy = 'create_date'

    fieldsets = (
        (None, {
            'fields': ['requestor']
        }),
        ('Mandatory fields in SVN request form', {
            'fields': ('shortname', 'longname', 'libuser')
        }),
        ('Optional fields in SVN request form', {
            'fields': ['restrictionlevel']
        }),
        ('Misc', {
            'fields': ('respuser', 'status', 'location', 'hash', 'create_date')
        }),
        ('Extra', {
            'fields':  ('content',),
            'classes':('collapse',),
        }),
    )

admin.site.register(SvnProject, SvnProjectAdmin)
