'''
    URL for the REST API, the URLS have a common base
'''
from django.conf.urls import url
from svn import views


urlpatterns = [
    url(r'^$', views.svn, name='svn'),
    url(r'(?i)^unmanaged/$',
        views.svn_unmanaged,
        name='svn_unmanaged'),
    url(r'(?i)^modified/$',
        views.svn_last_modified,
        name='svn_last_modified'),
    url(r'^(?P<pk>[/\-\w]+)/quota/$',
        views.svn_quota, name='svn_quota'),
    url(r'^(?P<pk>[/\-\w]+)/reset/$',
        views.svn_reset_repository,
        name='svn_reset_repository'),
    url(r'^(?P<pk>[/\-\w]+)/(?P<choice>approved|rejected)/$',
        views.svn_approve_reject, name='svn_approve_reject'),
    url(r'^(?P<pk>[/\-\w]+)/trac/(?P<action>(init|resync|upgrade))/$',
        views.trac, name='trac'),
    url(r'^(?P<pk>[/\-\w]+)/trac/migrate/$',
        views.trac_migrate, name='trac_migrate'),
    url(r'^(?P<pk>[/\-\w]+)/svn_archive/',
        views.svn_archive_repo, name='svn_archive_repo'),
    url(r'^(?P<pk>[/\-\w]+)/$', views.svn, name='svn'),
]
