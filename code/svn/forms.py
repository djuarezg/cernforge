"""
    SVN admin forms
"""
from django import forms
from django.core import validators
from django.forms import ModelForm
from django.utils.safestring import mark_safe
from django.utils.functional import curry

from cernforge.globals import is_afs_account
from cernforge import cernldap

from svn.models import (
    SvnProject,
    is_new_repository
)

from svn.log import svnlogger
from svn.validators import (
    add_field_validation,
    is_unique_caseinsensitive,
    validate_shortname,
    validate_longname,
    validate_cern_account,
    validate_libuser,
)

class SvnAdminForm(ModelForm):
    """SVN admin class form"""
    def clean(self):
        """Validate only those fields that have changed to a non-empty value"""
        cleaned_data = super(SvnAdminForm, self).clean()

        validators = {
            'shortname': validate_shortname,
            'longname': validate_longname,
            'requestor': validate_cern_account,
            'libuser': curry(validate_libuser, cleaned_data['shortname']),
            'respuser': validate_cern_account,
        }

        if 'shortname' in cleaned_data:
            existing_repository = is_new_repository(cleaned_data['shortname'])
        else:
            existing_repository = None

        for field_name, field_value in cleaned_data.items():
            if field_value and field_name in validators:
                if field_value and (not existing_repository
                                    or field_value != getattr(existing_repository,
                                                              field_name)):
                    validators[field_name](field_value, field_name)

        return cleaned_data

    class Meta:
        """Sub meta class"""
        model = SvnProject
        fields = '__all__'

class SvnForm(ModelForm):
    """Main SVN form"""
    justification = forms.CharField(
        help_text="Give a few words why you need this repository in SVN and not in GITLAB.",
        widget=forms.Textarea(attrs={'class': 'textarea medium-long-field'}),
    )

    is_basic = forms.CharField(
        label="",
        initial='True',
        widget=forms.HiddenInput(),
    )

    agreement = forms.BooleanField(
        help_text='I agree not to add or commit into SVN any files containing \
passwords or confidental information.',
        widget=forms.CheckboxInput(attrs={'class':'checkbox'}),
    )

    def __init__(self, *args, **kwargs):
        super(SvnForm, self).__init__(*args, **kwargs)
        mega_help = {
            'shortname': "The ID of the repository, e.g. 'lhcmon', no spaces, \
max %d characters." % self.fields['shortname'].max_length,
            'longname': "E.g. 'LHC Monitoring Project', max %d characters." %
                        self.fields['longname'].max_length,
            'libuser': mark_safe("""
                <p>The librarian account will be able to administer the repository here of CERN Forge.</p>
                <p>It will also be added to the list of librarians to the authz file for the repository.</p>
                <p>
                    This has to be a service account to ensure that the repository is not without owner <br />
                    if your account gets deactivated.
                </p>
            """),
            'restrictionlevel': mark_safe("""
                Restricted - only for a defined set of users. <br />
                WORLD - anonymous read only access from anywhere in the world.
            """),
            'justification': mark_safe("""
                GitLab is now the recommended tool for version control software.<br />
                SVN is still supported, but we would like to learn about which scenarios that GitLab can not cover.<br />
                Please write some words about what this repository will be used for, who that is going to use it.
            """),
        }

        for key, helptext in mega_help.items():
            self.fields[key].error_messages['mega_help'] = helptext

        add_field_validation(
            instance=self,
            required_fields={'shortname', 'longname', 'requestor', 'libuser',
                             'justification', 'agreement'},
        )

    def clean_shortname(self):
        """
            Cleans the shortname, so all characters are valid. Also avoids
            atlas- repositories to be creatred badly
        """
        shortname = self.cleaned_data['shortname']
        if shortname.startswith('atlas-'):
            raise forms.ValidationError(mark_safe('atlas-* repositories are reserved \
for personal ATLAS repositories. You can request one <a href="https://cernforge.cern.ch/svn/request/atlas-personal/">here</a>.'))
        return shortname

    def clean_requestor(self):
        """The requestor is a static, readonly value, it cannot change."""
        requestor = self.cleaned_data['requestor']
        if 'requestor' in self.changed_data:
            raise forms.ValidationError(
                "The requestor has been tampered with. It is supposed to be '{requestor}', \
but it is '{requestor_new}'.".format(
    requestor=self.initial['requestor'],
    requestor_new=requestor,
                ),
            )
        return requestor

    # If the user chose 'Advanced', then altered some of the advanced inputs, then
    # chooses basic, we want these inputs to have their default values, not the ones
    # that the user set.
    def clean(self):
        cleaned_data = super(SvnForm, self).clean()

        if cleaned_data['is_basic'] == 'True':
            for field in self.fields:
                if not self.fields[field].required:
                    if field in self.initial:
                        cleaned_data[field] = self.initial[field]

        if 'shortname' in cleaned_data:
            is_unique_caseinsensitive(cleaned_data['shortname'], 'shortname')

            if 'libuser' in cleaned_data:
                validate_libuser(cleaned_data['shortname'], cleaned_data['libuser'], 'libuser')

        return cleaned_data

    class Meta:
        """Meta sub class"""
        model = SvnProject
        fields = ('requestor', 'shortname', 'longname', 'libuser',
                  'restrictionlevel')

        widgets = {
            'requestor': forms.TextInput(
                attrs={
                    'class': 'text medium-long-field',
                    'readonly': 'readonly',
                }
            ),
            'longname': forms.TextInput(attrs={'class': 'text medium-long-field'}),
            'shortname': forms.TextInput(attrs={'class': 'text medium-long-field'}),
            'libuser': forms.TextInput(attrs={'class': 'text medium-long-field'}),
            'restrictionlevel': forms.Select(attrs={'class':'select medium-long-field'}),
        }

class SvnFormAtlasPersonal(SvnForm):
    """
        Form created specially for atlas-* repositories
    """
    def __init__(self, *args, **kwargs):
        super(SvnFormAtlasPersonal, self).__init__(*args, **kwargs)
        for field in self.fields:
            if field not in ['agreement', 'justification']:
                self.fields[field].widget.attrs['readonly'] = 'readonly'

    def clean_shortname(self):
        return self.cleaned_data['shortname']

    def clean(self):
        cleaned_data = super(SvnFormAtlasPersonal, self).clean()
        if self.has_changed():
            for field_name in self.changed_data:
                if field_name not in ['agreement', 'justification']:
                    raise forms.ValidationError('The submitted values does not \
match the initial ones.')

        return cleaned_data

class SvnDetailsStatusForm(forms.Form):
    """
        Form created specially for status updates
    """
    CHOICES_RADIO = [
        ('1', 'We are not using this anymore (Please delete or archive the repository)'),
        ('2', 'We are in the process of migration'),
        ('3', 'We have plans to migrate at a later date'),
        ('4', 'We have not made any plans yet '),
        ('5', 'We are trying but it is not working as expected'),
        ('6', 'I do not know or remember this repository')]

    CHOICES_CHECKBOX = [
        ('1', 'We have software/workflow that only supports SVN'),
        ('2', 'We do not have the knowledge to migrate'),
        ('3', 'Insufficient time'),
        ('4', 'There is not enough documentation')]

    migration_status = forms.ChoiceField(choices=CHOICES_RADIO, label="...",
                                         widget=forms.RadioSelect())
    migration_prevents = forms.MultipleChoiceField(
        choices=CHOICES_CHECKBOX,
        label="...",
        required=False,
        widget=forms.CheckboxSelectMultiple)

    def clean(self):
        cleaned_data = super(SvnDetailsStatusForm, self).clean()
        return cleaned_data

    def __init__(self, *args, **kwargs):
        super(SvnDetailsStatusForm, self).__init__(*args, **kwargs)
