'''
    Log function for SVN
'''
from cernforge.log import forgelogger
def svnlogger(level, message, email_admins):
    '''
        Calls the main log function, adds SVN to the beggining
    '''
    forgelogger(
        level=level,
        message='SVN | %s' % message,
        email_admins=email_admins,
    )
