'''
    Mail sendinf commands
'''
import socket

from cernforge import forgemail
from cernforge import settings

# command updateRespusers
def sendNotifyNewRespuser(inherited_repositories, respuser_new, respuser_email,
                          respuser_displayName):
    '''
        Creates a ticket in the name of the new reponsible user of a repository
    '''
    subject = '[svn.cern.ch] You are the new administrator of one or more SVN repositories.'
    sender = respuser_email
    receiver = 'SVN.Support@cern.ch' if not settings.DEV else settings.DEV_EMAIL
    data = {
        'respuser_displayName': respuser_displayName,
        'respuser_new': respuser_new,
        'inherited_repositories': inherited_repositories
    }
    plaintext_template = 'svn/mail/notifyNewRespuser.txt'
    html_template = None
    reply_to = None
    forgemail.send_mail(subject, sender, reply_to, receiver, data, plaintext_template, html_template)

# command updateRespusers
def sendUpdateRespusersSummary(totalrepscount, repositories_changed):
    '''
        Sends an email with a summary of the update of the responsible users accounts
    '''
    subject = '[CERN Forge] updateRespusers command status report'
    sender = 'no-reply@cern.ch'
    receiver = 'it-service-vcs-forward@cern.ch' if not settings.DEV else settings.DEV_EMAIL
    data = {
        'hostname': socket.gethostname(),
        'repositories_changed': repositories_changed,
        'totalrepscount': totalrepscount,
    }
    plaintext_template = 'svn/mail/updateRespusersSummary.txt'
    html_template = None
    reply_to = receiver
    forgemail.send_mail(subject, sender, reply_to, receiver, data, plaintext_template, html_template)

# command checkLibusers
def sendNotifyInvalidLibuser(invalid_repositories, respuser, respuser_email, respuser_displayName):
    '''
        Creates a ticket in the name of a SVN responsible user, when an invalid
        librarian account was detected.
    '''
    subject = '[svn.cern.ch] Correcting invalid SVN repositories'
    sender = respuser_email
    receiver = 'SVN.Support@cern.ch' if not settings.DEV else settings.DEV_EMAIL
    data = {
        'invalid_repositories': invalid_repositories,
        'respuser_displayName': respuser_displayName,
        'respuser': respuser,
    }
    plaintext_template = 'svn/mail/notifyInvalidLibuser.txt'
    html_template = None
    reply_to = None
    forgemail.send_mail(subject, sender, reply_to, receiver, data, plaintext_template, html_template)

# command checkLibusers
def sendCheckLibusersSummary(totalrepscount, repositories_changed):
    '''
        Sends summary about the librarians users changes
    '''
    subject = '[CERN Forge] checkLibusers command status report'
    sender = 'no-reply@cern.ch'
    receiver = 'it-service-vcs-forward@cern.ch' if not settings.DEV else settings.DEV_EMAIL
    data = {
        'hostname': socket.gethostname(),
        'repositories_changed': repositories_changed,
        'totalrepscount': totalrepscount,
    }
    plaintext_template = 'svn/mail/checkLibusersSummary.txt'
    html_template = None
    reply_to = receiver
    forgemail.send_mail(subject, sender, reply_to, receiver, data, plaintext_template, html_template)

# command checkOrphans
def sendCheckOrphansSummary(totalrepscount, repositories_changed):
    '''
        Sends summary about the orphans repositories
    '''
    subject = '[CERN Forge] checkOrphans command status report'
    sender = 'no-reply@cern.ch'
    receiver = 'it-service-vcs-forward@cern.ch' if not settings.DEV else settings.DEV_EMAIL
    data = {
        'hostname': socket.gethostname(),
        'repositories_changed': repositories_changed,
        'totalrepscount': totalrepscount,
    }
    plaintext_template = 'svn/mail/checkOrphansSummary.txt'
    html_template = None
    reply_to = receiver
    forgemail.send_mail(subject, sender, reply_to, receiver, data, plaintext_template, html_template)



def sendRequestedRepository(repo, sender, justification=None):
    '''
        Sends email about a requested repository
    '''
    subject = "[svn.cern.ch] New repository '{0}' requested".format(repo.shortname)
    sender = sender
    receiver = 'SVN.Support@cern.ch' if not settings.DEV else settings.DEV_EMAIL
    data = {
        'hostname': socket.gethostname(),
        'repo': repo,
        'justification': justification,
    }
    plaintext_template = 'svn/mail/requestedRepository.txt'
    html_template = None
    reply_to = None
    forgemail.send_mail(subject, sender, reply_to, receiver, data, plaintext_template, html_template)
