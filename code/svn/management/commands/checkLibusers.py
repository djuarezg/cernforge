'''
    Check librarian users validity and notifies ownsers in case the librarian account is invalid
'''
import time
#import re
#import collections

from django.core.management.base import BaseCommand

from svn.models import SvnProject
from svn.mail import sendNotifyInvalidLibuser, sendCheckLibusersSummary

from cernforge import cernldap
#from cernforge.globals import findOwnerOfServiceAccount

class Command(BaseCommand):
    '''
        Command class
    '''
    def write_stdout(self, message):
        '''
            Writes to standard output adding the time
        '''
        self.stdout.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def write_stderr(self, message):
        '''
            Writes to standard error output adding the time
        '''
        self.stderr.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def handle(self, *args, **options):
        '''
            This command iterates over all SVN repositories, and checks if the
            librarian account is invalid. If it is, and the owner on record (respuser)
            is valid, the owner is notified and asked to either update the librarian,
            or to delete the repository.
        '''

        self.write_stdout('Getting all users ...')
        users = cernldap.all_users()
        self.write_stdout('{0} users retrieved.'.format(len(users)))

        self.write_stdout('Getting all active repositories ...')
        reps = SvnProject.objects.filter(status='active')
        totalrepscount = len(reps)
        self.write_stdout('{0} repositories found.'.format(totalrepscount))

        self.write_stdout('Iterate over all active SVN repositories and finding libusers not in LDAP ...')

        libusers_invalid = dict()
        repositories_changed = dict()
        repcount = 0
        for rep in reps:
            repcount += 1

            # If the respuser is valid, but the libuser isn't
            if rep.respuser in users and rep.libuser not in users:
                self.write_stdout("Repo {0}/{1}: Name '{2}': libuser '{3}' is not found in LDAP. Will alert previously registered owner '{4}'.".format(
                    repcount,
                    totalrepscount,
                    rep.shortname,
                    rep.libuser,
                    rep.respuser,
                ))

                # Store for every user that is associated with a project (shortname)
                # with an invalid libuser
                if rep.respuser not in libusers_invalid:
                    libusers_invalid[rep.respuser] = dict()
                libusers_invalid[rep.respuser][rep.shortname] = rep.libuser

                repositories_changed[rep.shortname] = {
                    'libuser': rep.libuser,
                    'respuser': rep.respuser,
                }

        if len(libusers_invalid):
            self.write_stdout('Notifying {0} owners (respusers) of invalid libusers ...'.format(
                len(libusers_invalid)),
                            )
            for respuser, invalid_repositories in libusers_invalid.items():
                user_info = cernldap.get_user_info(respuser)
                sendNotifyInvalidLibuser(
                    invalid_repositories=invalid_repositories,
                    respuser=respuser,
                    respuser_email=user_info['mail'][0],
                    respuser_displayName=user_info['displayName'][0],
                )
            self.write_stdout('Done.')

            self.write_stdout('Notifying admins about invalid libusers ...')
            sendCheckLibusersSummary(totalrepscount, repositories_changed)

        self.write_stdout('DONE!')
