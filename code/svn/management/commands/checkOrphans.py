'''
    Checks for orphan projects, i.e.: Projects with no valid owner.
'''
import time
#import re
#import collections

import configparser

from django.core.management.base import BaseCommand

from svn.models import SvnProject
#from svn.mail import sendNotifyInvalidLibuser, sendCheckOrphansSummary
from svn.mail import sendCheckOrphansSummary

from cernforge import cernldap
#from cernforge.globals import findOwnerOfServiceAccount

class Command(BaseCommand):
    '''
        Class to host the main code for the command
    '''
    def write_stdout(self, message):
        '''
            Writes to standard out. with timestamp
        '''
        self.stdout.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def write_stderr(self, message):
        '''
            Writes to standard error. with timestamp
        '''
        self.stderr.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def handle(self, *args, **options):
        '''
            This command iterates over all SVN repositories, and checks if the librarian account is invalid. If it is,
            and the owner on record (respuser) is valid, the owner is notified and asked to either update the librarian, or to delete the repository.

            If the librarian (libuser) is a service account deleted from LDAP, we will still have it's owner on record. This scripts requests action from this owner.
        '''

        self.write_stdout('Getting all users ...')
        users = cernldap.all_users()
        self.write_stdout('{0} users retrieved.'.format(len(users)))

        self.write_stdout('Getting all active repositories ...')
        reps = SvnProject.objects.filter(status='active')
        totalrepscount = len(reps)
        self.write_stdout('{0} repositories found.'.format(totalrepscount))

        self.write_stdout('Iterate over all active SVN repositories and finding libusers not in LDAP ...')

        config = configparser.ConfigParser()
        libusers_invalid = dict()
        repositories_changed = dict()
        repcount = 0
        for rep in reps:
            repcount += 1

            # If the respuser and libuser are not valid
            if rep.respuser not in users and rep.libuser not in users:
                self.write_stdout("Repo {0}/{1}: Name '{2}': libuser '{3}' and respuser '{4}' are not found in LDAP.".format(
                    repcount,
                    totalrepscount,
                    rep.shortname,
                    rep.libuser,
                    rep.respuser,
                ))

                new_librarians = []
                # Check users in the authz file
                try:
                    config.read(rep.location + "conf/authz")
                except Exception as exc:
                    self.write_stderr('Error while reading '+rep.location + "conf/authz")
                    self.write_stderr(exc)
                    continue

                try:
                    librarians = config['groups']['librarian'].split(',')
                except KeyError as kye:
                    self.write_stderr(kye)
                except TypeError as tpe:
                    self.write_stderr('ERROR while reading '+rep.location + "conf/authz")
                    self.write_stderr(tpe)
                else:
                    for lib in librarians:
                        if lib in users:
                            new_librarians.append(lib)

                # Store for every user that is associated with a project (shortname)
                # with an invalid libuser
                if rep.respuser not in libusers_invalid:
                    libusers_invalid[rep.respuser] = dict()
                libusers_invalid[rep.respuser][rep.shortname] = rep.libuser

                repositories_changed[rep.shortname] = {
                    'libuser': rep.libuser,
                    'respuser': rep.respuser,
                    'new_librarians': new_librarians,
                }


        if len(libusers_invalid):
            self.write_stdout('Notifying admins about orphan projects ...')
            sendCheckOrphansSummary(totalrepscount, repositories_changed)

        self.write_stdout('DONE!')
