'''
    Looks for approved SVN repositories and creates them
'''
import time

from django.core.management.base import BaseCommand

from cernforge.globals import (
    CONNECT_TO_SVN,
    SVN_CREATE_REPOSITORY_SCRIPT,
    runCommand,
)
from cernforge.exceptions import NonZeroReturnCode

from svn.models import SvnProject
from svn.log import svnlogger

class Command(BaseCommand):
    '''
        This method runs script which creates every approved SVN repository (status=approved).
    '''
    help = 'This method runs script which creates every approved SVN repository (status=approved).'

    def write_log(self, message):
        '''
            Writes log to STD out
        '''
        self.stdout.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def handle(self, *args, **options):
        approved_projects = SvnProject.objects.filter(status='approved')

        self.write_log('START COMMAND createApprovedRepositories (SVN)')
        self.write_log('Found {0} approved projects.'.format(approved_projects.count()))

        success = list()
        error = list()
        for project in approved_projects:
            try:
                self.write_log("Creating repository '{0}' ...".format(project.shortname))
                project.status = 'creating'
                project.save()
                runCommand(
                    command='{connect_svn} "{create_script} {shortname} agonzale@cern.ch"'.format(
                        connect_svn=CONNECT_TO_SVN,
                        create_script=SVN_CREATE_REPOSITORY_SCRIPT,
                        shortname=project.shortname,
                    ),
                    timeout=None,
                    silentFail=False,
                    shell=True,
                )
            except NonZeroReturnCode as nze:
                svnlogger('error', 'NonZeroReturnCode (%s): %s' % (nze.returncode,
                                                                   nze.stdout), False)
                svnlogger('error', 'NonZeroReturnCode (%s): %s' % (nze.returncode,
                                                                   nze.stderr), False)
                return nze.returncode
            except Exception:
                project.status = 'approved'
                project.save()
                error.append(project.shortname)
            else:
                success.append(project.shortname)


        if success:
            message = 'Successfully created the following repositories: {replist}'.format(
                replist=', '.join(success)
            )
            svnlogger(
                level='info',
                message='CRON | {0}'.format(message),
                email_admins=False,
            )
            self.write_log(message)

        if error:
            message = 'The following repositories could not be created: {replist}'.format(
                replist=', '.join(error)
            )
            svnlogger(
                level='error',
                message='CRON | {0}'.format(message),
                email_admins=True,
            )
            self.write_log(message)

        self.write_log('END COMMAND createApprovedRepositories (SVN)')
