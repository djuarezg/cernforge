'''
    Iterates over all SVN repositories and outputs its last commit date.
'''
import time
# import re
# import collections

from django.core.management.base import BaseCommand

from svn.models import SvnProject
# from svn.mail import sendNotifyInvalidLibuser, sendCheckOrphansSummary

from svn.views import svn_lastModifiedDate

# from cernforge import cernldap
# from cernforge.globals import findOwnerOfServiceAccount


class Command(BaseCommand):
    '''
        Iterates over all SVN repositories and outputs its last commit date.
    '''
    def write_stdout(self, message):
        '''
            Writes output to STD out
        '''
        self.stdout.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def write_stderr(self, message):
        '''
            Writes output to STD error
        '''
        self.stderr.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def handle(self, *args, **options):
        '''
            This command iterates over all SVN repositories and outputs its last
            commit date.
        '''

        # self.write_stdout('Getting all users ...')
        # users = cernldap.all_users()
        # self.write_stdout('{0} users retrieved.'.format(len(users)))

        self.write_stdout('Getting all active repositories ...')
        reps = SvnProject.objects.filter(status='active')
        totalrepscount = len(reps)
        self.write_stdout('{0} repositories found.'.format(totalrepscount))

        self.write_stdout('Iterate over all active SVN repositories ...')

        for rep in reps:
            self.write_stdout('{1} ({0})'.format(rep.shortname, svn_lastModifiedDate(rep)))

        self.write_stdout('DONE!')
