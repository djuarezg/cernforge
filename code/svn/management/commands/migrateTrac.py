'''
    Migrates Trac database from a SVN repository into a Gitlab project.
'''
import time
#import re
#import collections

from django.core.management.base import BaseCommand

import svn.utils
#from svn.mail import sendNotifyInvalidLibuser, sendCheckOrphansSummary

#from cernforge.globals import findOwnerOfServiceAccount

class Command(BaseCommand):
    '''
        Class to host the main code for the command
    '''
    def write_stdout(self, message):
        '''
            Writes to standard out. with timestamp
        '''
        self.stdout.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def write_stderr(self, message):
        '''
            Writes to standard error. with timestamp
        '''
        self.stderr.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def add_arguments(self, parser):
        parser.add_argument('dbHost', type=str)
        parser.add_argument('dbPort', type=int)
        parser.add_argument('dbUser', type=str)
        parser.add_argument('dbPasswd', type=str)
        parser.add_argument('dbOriginName', type=str)
        parser.add_argument('paramGitlabUrl', type=str)
        parser.add_argument('destProjectName', type=str)


    def handle(self, *args, **options):
        '''
            This command calls the utility for trac migration. It will read from the Trac database and create issues on the
            Gitlab project, adapting data if necessary to fit in.
        '''

        db_Host = options['dbHost']	
        db_Port = options['dbPort']
        db_User = options['dbUser']
        db_passwd = options['dbPasswd']
        db_origin_name = options['dbOriginName']
        param_gitlab_url = options['paramGitlabUrl']
        dest_project_name = options['destProjectName']	

        svn.utils.trac_to_gitlab(self, db_Host, db_Port, db_User, db_passwd, db_origin_name, param_gitlab_url, dest_project_name)

