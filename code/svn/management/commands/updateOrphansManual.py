'''
    Reassigns a project to a new responsible, and sends an email that create a ticket
'''
import time

from django.core.management.base import BaseCommand

from svn.models import SvnProject
from svn.mail import sendNotifyNewRespuser#, sendUpdateRespusersSummary

from cernforge import cernldap
#from cernforge.globals import findOwnerOfServiceAccount
from cernforge import settings

class Command(BaseCommand):
    '''
        Reassigns a project to a new responsible, and sends an email that create a ticket
    '''
    def write_stdout(self, message):
        '''
            Writes output to STD out
        '''
        self.stdout.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def write_stderr(self, message):
        '''
            Writes output to STD error
        '''
        self.stderr.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def add_arguments(self, parser):
        parser.add_argument('newresp')
        parser.add_argument('project')

    def handle(self, *args, **options):
        self.write_stdout("Setting new responsible '%s' for the project '%s'" %
                          (options['newresp'], options['project']))

        reps = SvnProject.objects.filter(shortname=options['project'])

        for repo in reps:
            self.write_stdout("Respuser\n\tCurrent: %s\n\tNew: %s" %
                              (repo.respuser, options['newresp']))

            respuser_old = repo.respuser
            repo.respuser = options['newresp']
            repo.save()

            owner_info = cernldap.get_user_info(repo.respuser)

            self.write_stdout("Sending email to: %s on behalf of %s" %
                              (settings.DEV_EMAIL, owner_info['mail'][0]))

            # exit(1)

            inherited_repositories = {options['project']: {'libuser': options['newresp'],
                                                           'respuser_old': respuser_old}}

            sendNotifyNewRespuser(
                inherited_repositories=inherited_repositories,
                respuser_new=options['newresp'],
                respuser_email=owner_info['mail'][0],
                respuser_displayName=owner_info['displayName'][0],
            )
            self.write_stdout("Done!")
