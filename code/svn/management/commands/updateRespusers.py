import time, re, collections

from django.core.management.base import BaseCommand

from svn.models import SvnProject
from svn.mail import sendNotifyNewRespuser, sendUpdateRespusersSummary

from cernforge import cernldap
from cernforge.globals import findOwnerOfServiceAccount

class Command(BaseCommand):
    def write_stdout(self, message):
        self.stdout.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def write_stderr(self, message):
        self.stderr.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def handle(self, *args, **options):
        '''
            This command iterates over all SVN repositories, and checks if the owner (respuser) of the libuser has
            changed. If it has, a new respuser is set, and notified about inheriting the repository.

            Please note that "atlas-*" repositories are excluded, since they require a respuser other than the owner
            of the libuser. A repository like atlas-foo typicall has librarian alibrari and respuser foo.
        '''

        help = 'Iterates over active SVN repositories (not atlas-*), and updates the owner (respuser) of libusers using LDAP. If an update was made, the respuser is notified.'

        self.write_stdout('Get all service accounts with owners ...')
        serviceaccs = cernldap.all_serviceaccounts(['cernAccountOwner'])
        self.write_stdout('Found {0} service accounts.'.format(len(serviceaccs)))

        self.write_stdout("Get all active repositories which does not start with atlas- ...")
        reps = SvnProject.objects.filter(status='active').exclude(shortname__startswith='atlas-').order_by('shortname')
        totalrepscount = len(reps)
        self.write_stdout('Found {0} repositories.'.format(totalrepscount))

        respusers_changed = dict()
        repositories_changed = dict()
        repcount = 0
        for rep in reps:
            repcount += 1

            # If the libuser is a service account ...
            if rep.libuser in serviceaccs:
                respusers = serviceaccs[rep.libuser]['cernAccountOwner']

                # Update the owner (respuser) if the LDAP value conflicts with the stored value ...
                if len(respusers) == 1 and rep.respuser not in respusers[0]:
                    respuser_new = respusers[0]
                    respuser_old = rep.respuser

                    self.write_stdout("Repo {0}/{1}: Name '{2}', libuser '{3}': owner (respuser) updated from '{4}' to '{5}'".format(
                        repcount,
                        totalrepscount,
                        rep.shortname,
                        rep.libuser,
                        respuser_old,
                        respuser_new,
                    ))

                    rep.respuser = respuser_new
                    rep.save()

                    if rep.respuser not in respusers_changed:
                        respusers_changed[rep.respuser] = dict()
                    respusers_changed[rep.respuser][rep.shortname] = {
                        'respuser_old': respuser_old,
                        'libuser': rep.libuser,
                    }

                    repositories_changed[rep.shortname] = {
                        'libuser': rep.libuser,
                        'respuser_old': respuser_old,
                        'respuser_new': respuser_new,
                    }

        if len(respusers_changed) > 0:

            self.write_stdout('Notifying new owners (respusers) ...')
            for respuser_new, inherited_repositories in respusers_changed.items():
                owner_info = cernldap.get_user_info(respuser_new)
                sendNotifyNewRespuser(
                    inherited_repositories=inherited_repositories,
                    respuser_new=respuser_new,
                    respuser_email=owner_info['mail'][0],
                    respuser_displayName=owner_info['displayName'][0],
                )
            self.write_stdout('{0} new owners notified.'.format(len(respusers_changed)))

            self.write_stdout('Notifying admins about new owners ...')
            sendUpdateRespusersSummary(totalrepscount, repositories_changed)
        else:
            self.write_stdout('No changes in libuser owner found')

        self.write_stdout('DONE!')
