'''
    Updates the last access on SVN projects
'''
from urllib.error import URLError

import time

from django.core.management.base import BaseCommand

import svn.utils


class ProjectAccessInfo:
    '''
        Project Access info class
    '''
    name = ""

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "ProjectAccessInfo()"

    def __str__(self):
        return self.name + " // " + self.date


class Command(BaseCommand):
    '''
        this command updates SVN project last writing access on the database.
    '''
    help = 'This command updates SVN project last writing access on the database.'

    def write_stdout(self, message):
        '''
            Writes log to standard out
        '''
        self.stdout.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def write_stderr(self, message):
        '''
            Writes log to standard error
        '''
        self.stderr.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def handle(self, *args, **options):
        '''
             Runs the Update last access
        '''
        self.write_stdout('Update last access date on SVN projects: Update started.')
        svn.utils.get_last_access()
