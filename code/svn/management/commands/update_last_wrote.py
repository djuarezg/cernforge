'''
    Updates the last modification on SVN projects
'''
import time
#import datetime

from django.core.management.base import BaseCommand

import svn.utils
#from cernforge import cernldap
from svn.models import SvnProject

class Command(BaseCommand):
    '''
        This command updates SVN project last writing access on the database.
    '''
    help = 'This command updates SVN project last writing access on the database.'

    def write_stdout(self, message):
        '''
            Writes log to standard out
        '''
        self.stdout.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def write_stderr(self, message):
        '''
            Writes log to standard error
        '''
        self.stderr.write("%s | %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), message))

    def handle(self, *args, **options):
        '''
            Runs the Update last wrote
        '''
        self.write_stdout('Update last modification on SVN projects: Update started.')
        projects = SvnProject.objects.all()

        for pro in projects:
            output = svn.utils.get_svn_last_wrote(pro.shortname)
            pro.last_wrote = output
            pro.save()
