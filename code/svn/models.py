'''
    SVN Projects models
'''
import re

from datetime import date

from django.db import models

from cernforge import cernldap

from cernforge.settings import (
    SVN_REPS_PATH
)

DEFAULT_SVN_PERMISSION = 'R'
CHOICES_SVN_PERMISSION = (
    ('R', 'Restricted'),
    ('W', 'WORLD'),
)

DEFAULT_SVN_VC = 'svn'
DEFAULT_SVN_STATUS = 'requested'

SVN_STATUS_CHOICES = (
    ('requested', 'requested'),
    ('approved', 'approved'),
    ('rejected', 'rejected'),
    ('creating', 'creating'),
    ('active', 'active'),
    ('deleting', 'deleting'),
    ('deleted', 'deleted'),
    ('obsolete', 'obsolete'),
)

class SvnProject(models.Model):
    '''
        SVN Projects model
    '''
    shortname = models.CharField(
        verbose_name='Repository name',
        help_text='An unique ID for the SVN repository.',
        max_length=256,
        primary_key=True,
        error_messages={'unique': 'Repository with this name already exists.'},
    )

    longname = models.CharField(
        verbose_name='Description',
        help_text='A description for the SVN Repository.',
        max_length=100,
        blank=True,
    )

    requestor = models.CharField(
        verbose_name='Requester',
        help_text='The initial requester of the repository. Cannot change.',
        max_length=20,
        blank=True,
    )

    libuser = models.CharField(
        verbose_name='Librarian',
        help_text='Librarian account, must be a service account.',
        max_length=20,
        blank=True,
    )

    restrictionlevel = models.CharField(
        verbose_name='Access',
        help_text='A description for the SVN Repository,  (R)estricted or (W)orld.',
        max_length=1,
        choices=CHOICES_SVN_PERMISSION,
        default=DEFAULT_SVN_PERMISSION,
    )

    status = models.CharField(
        verbose_name='Status',
        help_text='The status of the repository, can be requested, creating, active, \
deleting or deleted.',
        max_length=20,
        choices=SVN_STATUS_CHOICES,
        default=DEFAULT_SVN_STATUS,
    )

    migration_status = models.IntegerField(
        verbose_name='migration_status',
        help_text='The status of the repository concerning the migration.',
    )

    migration_prevents = models.CharField(
        verbose_name='migration_prevents',
        help_text='The reasons that prevents the repository from migrating.',
        max_length=40,
    )

    jirasync = models.IntegerField(
        verbose_name='Jira synchronized',
        help_text='Indicates if this repository is synchronized with Jira.',
    )

    email_sent_date = models.DateField(
        verbose_name='Date of the migration email (form)',
        help_text='Date of the migration email (form)',
        default=date.today,
        blank=True,
    )

    respuser = models.CharField(
        verbose_name='Librarian owner',
        help_text='The owner of the librarian, or the requestor in atlas-* repositories. \
Should be calculated automatically by CERN Forge.',
        max_length=20,
        blank=True,
    )

    location = models.CharField(
        verbose_name='AFS Location',
        help_text='Location of the repository in AFS.',
        max_length=60,
        blank=True,
    )

    create_date = models.DateField(
        verbose_name='Create date',
        help_text='When the repository was created.',
        default=date.today,
        blank=True,
    )

    last_wrote = models.DateField(
        verbose_name='Last wrote',
        help_text='Last time repository was modified',
        default=date.today,
        blank=True,
    )

    last_access = models.DateField(
        verbose_name='Last access',
        help_text='Last time repository was accessed',
        default=date.today,
        blank=True,
    )

    last_user_access = models.CharField(
        verbose_name='Last user access',
        help_text='Username who made the last access',
        max_length=60,
        blank=True,
    )

    last_user_write = models.CharField(
        verbose_name='Last user writing',
        help_text='Username who made the last writing',
        max_length=60,
        blank=True,
    )

    hash = models.CharField(
        verbose_name='AFS Hash',
        help_text='AFS Volume hash where the SVN repository is stored.',
        max_length=14,
        blank=True,
    )

    def __str__(self):
        '''
            Returns the string
        '''
        return '%s' % (self.shortname)

    def __unicode__(self):
        '''
            Returns the unicode string
        '''
        return u'%s' % (self.shortname)

    class Meta:
        '''
            Meta class
        '''
        db_table = u'project'
        verbose_name = u'SVN Project'
        verbose_name_plural = u'SVN Projects (project)'

    def clean(self):
        # Warning: be wary of placing any validation logic here:
        # Django Forms will use it, but not the serializers of Django Rest Framework 3+.
        pass

    def save(self, *args, **kwargs):
        existing_repository = is_new_repository(self.pk)

        if existing_repository:
            existing_attrs = {
                'location': existing_repository.location,
                'respuser': existing_repository.respuser,
            }
        else:
            existing_attrs = {
                'location': self.location,
                'respuser': self.respuser,
            }

        # Only calculate location and respuser if this a new repository, or an update
        # where the user has not manually touched these fields.

        if self.location == existing_attrs['location']:
            # Also, don't update the location if this is an update with the location empty
            if not existing_repository or existing_attrs['location']:
                location = "{svn_reps_path}/{shortname}/".format(
                    svn_reps_path=SVN_REPS_PATH,
                    shortname=self.shortname
                )
                if location != existing_attrs['location']:
                    self.location = location

        if self.respuser == existing_attrs['respuser']:
            respuser = existing_attrs['respuser']
            if self.shortname.startswith('atlas-'):
                if self.requestor:
                    # For old repositories, requestor is NULL
                    respuser = self.requestor
                else:
                    # Deduct responsible by looking at the username embedded in the shortname
                    match = re.match('atlas-(?P<respuser>[^-]+)$', self.shortname)
                    if match:
                        respuser = match.group('respuser')
            elif cernldap.user_exist(self.libuser):
                libuser_info = cernldap.get_user_info(self.libuser)
                if 'cernAccountOwner' in libuser_info:
                    match = re.match(r'CN=(?P<owner_cn>[^,]+),',
                                     libuser_info['cernAccountOwner'][0])
                    if match:
                        respuser = match.group('owner_cn')

            if respuser != existing_attrs['respuser']:
                self.respuser = respuser

        super(SvnProject, self).save(*args, **kwargs)



def is_new_repository(primaryk):
    '''
        True if the project/repository is new
    '''
    try:
        existing = SvnProject.objects.get(pk=primaryk)
    except SvnProject.DoesNotExist:
        return None
    else:
        return existing
