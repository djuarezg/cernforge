from django.core.exceptions import ValidationError

from rest_framework.exceptions import ValidationError as DRFValidationError
from rest_framework import serializers
from rest_framework.renderers import JSONRenderer

from svn.models import SvnProject
from svn.log import svnlogger
from svn.validators import add_field_validation, is_unique_caseinsensitive, validate_libuser

class NewSvnProjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = SvnProject

        fields = (
            # allowed fields:
            'shortname',
            'longname',
            'requestor',
            'libuser',
            'restrictionlevel',
            #
            'respuser',
            'location',
            'status',
            'create_date',
            'last_wrote',
            'last_access',
            'last_user_write',
            'last_user_access',
            'hash',
            'migration_status',
            'migration_prevents',
            'email_sent_date',
            'jirasync',
        )

        read_only_fields = (
            'respuser',
            'location',
            'status',
            'create_date',
            'last_wrote',
            'last_access',
            'last_user_write',
            'last_user_access',
            'hash',
            'migration_status',
            'migration_prevents',
            'email_sent_date',
            'jirasync',
        )

    def __str__(self):
        return JSONRenderer().render(self.data)

    def validate(self, data):

        try:
            if 'shortname' in data:
                is_unique_caseinsensitive(data['shortname'], 'shortname')

                if 'libuser' in data:
                    validate_libuser(data['shortname'], data['libuser'], 'libuser')
        except ValidationError as verr:
            raise DRFValidationError(verr.message_dict)

        return data

    def __init__(self, *args, **kwargs):
        super(NewSvnProjectSerializer, self).__init__(*args, **kwargs)
        required_fields = {'shortname', 'longname', 'requestor', 'libuser'}
        add_field_validation(
            instance=self,
            required_fields=required_fields,
        )

        # No fields may be blanked
        for field_name in self.fields.keys():
            self.fields[field_name].allow_blank = False

class ExistingSvnProjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = SvnProject

        fields = (
            # editable fields:
            'longname',
            'libuser',
            'restrictionlevel',
            #
            'shortname',
            'requestor',
            'respuser',
            'location',
            'status',
            'create_date',
            'last_wrote',
            'last_access',
            'last_user_write',
            'last_user_access',
            'hash',
            'migration_status',
            'migration_prevents',
            'email_sent_date',
            'jirasync',
        )

        read_only_fields = (
            'shortname',
            'requestor',
            'respuser',
            'location',
            'status',
            'create_date',
            'last_wrote',
            'last_access',
            'last_user_write',
            'last_user_access',
            'hash',
            'migration_status',
            'migration_prevents',
            'email_sent_date',
            'jirasync',
        )

    def __str__(self):
        return JSONRenderer().render(self.data)

    def validate(self, data):

        try:
            if 'libuser' in data:
                validate_libuser(self.instance.shortname, data['libuser'], 'libuser')
        except ValidationError as verr:
            raise DRFValidationError(verr.message_dict)

        return data

    def __init__(self, *args, **kwargs):
        super(ExistingSvnProjectSerializer, self).__init__(*args, **kwargs)

        # Add validators, but since we are able to do partial updates here, don't require any fields
        add_field_validation(
            instance=self,
            required_fields={},
        )

        # No fields may be blanked
        for field_name in self.fields.keys():
            self.fields[field_name].allow_blank = False
