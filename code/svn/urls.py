'''
    URLs for SVN views
'''
from django.conf.urls import url
from svn import views

urlpatterns = [
    url(r'(?i)^request/$', views.svn_form, name='svn_form'),
    url(r'^request/atlas-personal/$',
        views.svn_form_atlaspersonal, name='svn_form_atlaspersonal'),
    url(r'(?i)^myprojects/$', views.svn_myprojects, name='svn_myprojects'),
    url(r'(?i)^allprojects/$', views.svn_allprojects, name='svn_allprojects'),
    url(r'(?i)^allprojects-obsolete/$', views.svn_allprojects_obsolete,
        name='svn_allprojects_obsolete'),
    url(r'(?i)^allprojects-unmanaged/$', views.svn_allprojects_unmanaged,
        name='svn_allprojects_unmanaged'),
    url(r'(?i)^details/(?P<pk>[/\-\w]+)/$', views.svn_details,
        name='svn_details'),
    url(r'(?i)^details_form/(?P<pk>[/\-\w]+)/$', views.svn_status_form,
        name='svn_status_form'),
]
