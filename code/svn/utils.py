'''
    Utils for SVN
'''
import datetime
import random
import urllib
import time
import json
from datetime import date
import requests
import mysql.connector
import gitlab

from cernforge.log import forgelogger
from cernforge.globals import (
    runCommand,
    CONNECT_TO_SVN,
    WASSH_SVNADMIN,
    SVN_CLI_PROD,
    HTTPD_LATEST_LOGFILE,
    SSH_LATEST_LOGFILE,
    BUNDLE
)

from cernforge.settings import (
    GITLAB_ADMIN_TOKEN,
    SVN_REPS_PATH,
)

from cernforge.exceptions import NonZeroReturnCode
from svn.log import svnlogger
from svn.models import SvnProject


def generate_color():
    '''
        Generates a random RGB color
    '''
    color = '#{:02x}{:02x}{:02x}'.format(*map(lambda x: random.randint(0, 255), range(3)))
    return color


class ProjectAccessInfo:
    '''
        Stores and print the project name and user who accessed it
    '''
    name = ""
    user = ""
    date_access = None

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "ProjectAccessInfo()"

    def __str__(self):
        return self.name + " " + self.date_access + " " + self.user


class JSONObject:
    '''
        Json object for the GitLab queries
    '''
    def __init__(self, dicto):
        vars(self).update(dicto)


def check_permissions_project(param_gitlab_url, dest_project_name):
    '''
        Checks if the current user has permisisons to *dest_project_name* in
        *param_gitlab_url*
    '''

    private_token = GITLAB_ADMIN_TOKEN
    headers_token = {'PRIVATE-TOKEN': private_token}

    gitl = gitlab.Gitlab('https://' + param_gitlab_url, private_token)
    project = gitl.projects.get(dest_project_name)

    # First of all we need to check whether if we allow the migration or not
    url = 'https://' + param_gitlab_url + '/api/v4/projects/' + str(project.id)
    resp = requests.get(url, headers=headers_token, verify=BUNDLE)
    jsonobject = json.loads(resp.content, object_hook=JSONObject)

    # We need to check if the project was found, otherwise migration user cannot see
    # the project and will not be able to migrate
    if hasattr(jsonobject, 'message'):
        return False

    url = 'https://' + param_gitlab_url + '/api/v4/projects/' + str(project.id) + "/members"
    resp = requests.get(url, headers=headers_token, verify=BUNDLE)
    jsonobject = json.loads(resp.content, object_hook=JSONObject)

    # We need to retrieve the username of the migration user just in case
    url = 'https://' + param_gitlab_url + '/api/v4/user'
    resp = requests.get(url, headers=headers_token, verify=BUNDLE)
    user_jsonobject = json.loads(resp.content, object_hook=JSONObject)
    username = user_jsonobject.username

    for user in jsonobject:
        if user.username == username:
            return True
    return False


def trac_to_gitlab(clas, db_host, db_port, db_user, db_passwd, db_origin_name,
                   param_gitlab_url, dest_project_name):
    '''
        See https://trac.edgewall.org/wiki/TracDev/DatabaseSchema/TicketSystem#Tableticket
        from DB schema
    '''

    private_token = GITLAB_ADMIN_TOKEN
    headers_token = {'PRIVATE-TOKEN': private_token}

    gitl = gitlab.Gitlab('https://' + param_gitlab_url, private_token)

    dbase = mysql.connector.connect(host=db_host,  # your host
                                    port=db_port,  # host port
                                    user=db_user,  # your username
                                    passwd=db_passwd,  # your password
                                    db=db_origin_name)  # name of the data base

    # you must create a Cursor object. It will let
    #  you execute all the queries you need
    cur_tickets = dbase.cursor()
    cur_milestones = dbase.cursor()
    cur_components = dbase.cursor()
    cur_ticket_changes = dbase.cursor()

    project = gitl.projects.get(dest_project_name)

    cur_components.execute("SELECT name, owner, description FROM component")
    #ADDING COMPONENTS AS LABELS
    for row in cur_components.fetchall():
        try:
            if row[0] != '':
                project.labels.create({'name': row[0], 'color': generate_color()})
        except gitlab.exceptions.GitlabCreateError:
            label_name = unicode(row[0], errors='ignore')
            clas.write_stdout("Component already migrated: " + str(label_name))
        except Exception as exc:
            forgelogger('error', str(exc), False)

    # ADDING MILESTONES (Milestones need to be closed AFTER adding issues)
    cur_milestones.execute("SELECT name, due, completed, description FROM milestone")
    for row in cur_milestones.fetchall():
        if row[0] != '':
            try:
                miles_name = unicode(row[0], errors='ignore')
                milestone = project.milestones.create({'title': miles_name})
                milestone.description = row[3]
                if row[2] != 0:
                    milestone.state_event = 'close'
                    milestone.state = 'closed'
                else:
                    milestone.state_event = 'activate'
                    milestone.state = 'active'
                due_date_time = datetime.datetime.fromtimestamp(row[1]/1e6)\
                                .strftime('%Y-%m-%d %H:%M:%S')
                milestone.due_date = due_date_time
                milestone.save()
            except gitlab.exceptions.GitlabCreateError:
                clas.write_stdout("Milestone already migrated: " + str(miles_name))
            except Exception as exc:
                forgelogger('error', str(exc), False)

    # ADDING ISSUES
    cur_tickets.execute("SELECT id, type, time, changetime, component, severity, \
priority, owner, reporter, cc, version, milestone, status, resolution, summary, \
description, keywords FROM ticket")
    for row in cur_tickets.fetchall():
        ticket_id = row[0]
        try:
            descrip = unicode(row[15], errors='ignore')
        except Exception:
            descrip = ""
        update_time = datetime.datetime.fromtimestamp(row[3]/1e6).strftime('%Y-%m-%d %H:%M:%S')
        create_time = datetime.datetime.fromtimestamp(row[2]/1e6).strftime('%Y-%m-%d %H:%M:%S')
        try:
            issue = project.issues.create({'title': unicode(row[14], errors='ignore'),
                                           'updated_at': update_time,  # row[3],
                                           'created_at': create_time,  # row[2],
                                           # 'author': "test", #row[8],
                                           # 'assignee_id': 2, #row[7],
                                           'description': descrip})

            # Adding component as label
            issue.labels = [row[4]]
            issue.save()

            # Need to define a new value for the weight:
            # ('priority', 'blocker', '1')
            # ('priority', 'critical', '2')
            # ('priority', 'major', '3')
            # ('priority', 'minor', '4')
            # ('priority', 'trivial', '5'
            choices = {'blocker': 5, 'critical': 4, 'major': 3, 'minor': 2, 'trivial': 1, }
            weight = choices.get(row[6], 'default')

            # Using Api directly
            # Adding weight
            url = 'https://{gitlab}/api/v4/projects/{pid}/issues/{iid}?weight={weight}'.format(
                gitlab=param_gitlab_url,
                pid=str(project.id),
                iid=str(issue.iid),
                weight=str(weight))
            requests.put(url, headers=headers_token)

            milestones = project.milestones.list()

            for miles in milestones:
                miles_row = unicode(row[11], errors='ignore')
                if miles.title == miles_row:
                    clas.write_stdout("FOUND Milestone: " + str(miles.id))
                    url = 'https://{gitlab}/api/v4/projects/{pid}/issues/{iid}?milestone_id={mid}'\
                          .format(gitlab=param_gitlab_url,
                                  pid=str(project.id),
                                  iid=str(issue.iid),
                                  mid=str(miles.id))
                    requests.put(url, headers=headers_token)
                    break
                else:
                    clas.write_stdout("Not the same milestone")

            cur_ticket_changes.execute("SELECT ticket, time, author, field, oldvalue, \
newvalue FROM ticket_change WHERE ticket=" + str(ticket_id) + ";")

            for ticket_change in cur_ticket_changes.fetchall():

                if(ticket_change[3] == 'comment') and (ticket_change[5] != ''):
                    body = "**User " + ticket_change[2] + " commented:** " + unicode(ticket_change[5], errors='ignore')
                    body = urllib.pathname2url(body)
                    url = 'https://' + param_gitlab_url + '/api/v4/projects/' + str(project.id) + '/issues/' + str(issue.iid) + '/notes?body=' + str(body) + '&created_at=' + str(ticket_change[1]/1e6)
                    requests.post(url, headers=headers_token)

                elif ticket_change[3] == 'status':
                    body = "**User " + ticket_change[2] + " changed status from " + ticket_change[4] + " to " + ticket_change[5] + "**"
                    url = 'https://' + param_gitlab_url + '/api/v4/projects/' + str(project.id) + '/issues/' + str(issue.iid) + '/notes?body=' + str(body) + '&created_at=' + str(ticket_change[1]/1e6)
                    requests.post(url, headers=headers_token)

                    if ticket_change[5] == 'closed':
                        # close an issue
                        issue.state_event = 'close'
                        issue.save()
                    else:
                        # reopen it
                        issue.state_event = 'reopen'
                        issue.save()

                elif ticket_change[3] == 'resolution':
                    if ticket_change[4] is not None:
                        body = "**User {user} changed resolution from {from_type} to {to_type}**"\
                               .format(user=ticket_change[2],
                                       from_type=ticket_change[4],
                                       to_type=ticket_change[5])
                    else:
                        body = "**User {user} changed resolution to {to_type}**"\
                               .format(user=ticket_change[2],
                                       to_type=ticket_change[5])
                        url = 'https://{url}/api/v4/projects/{pid}/issues/{iid}/notes?body={body}&created_at={created_at}'\
                              .format(url=param_gitlab_url,
                                      pid=str(project.id),
                                      iid=str(project.id),
                                      body=str(body),
                                      created_at=str(ticket_change[1]/1e6))
                    requests.post(url, headers=headers_token)

                elif ticket_change[3] == 'type':
                    body = "**User {user} changed type from {from_type} to {to_type}**"\
                           .format(user=ticket_change[2],
                                   from_type=ticket_change[4],
                                   to_type=ticket_change[5])
                    body = "**User " + ticket_change[2] + " changed type from " + ticket_change[4] + " to " + ticket_change[5] + "**"
                    url = 'https://' + param_gitlab_url + '/api/v4/projects/' + str(project.id) + '/issues/' + str(issue.iid) + '/notes?body=' + str(body) + '&created_at=' + str(ticket_change[1]/1e6)
                    requests.post(url, headers=headers_token)

        except Exception as exc:
            forgelogger('error', str(exc), False)

    dbase.close()


def get_trac_info(projkey):
    '''
        Retreives the mysql line from the trac.ini file of the repository
    '''
    output = runCommand(
        command='{connect_svn} "grep ^mysql /reps/{svnproject}/trac/conf/trac.ini"'.format(
            connect_svn=CONNECT_TO_SVN,
            svnproject=projkey,
        ),
        timeout=None,
        silentFail=False,
        shell=True,
    )
    return output


def has_trac(repository_name):
    '''
        Given a repository name, return True if it has trac, False otherwise
    '''
    try:
        output = runCommand(
            command='{connect_svn} "if test -f {svn_reps_path}/{shortname}/trac/conf/trac.ini; then echo 0; else echo -1; fi"'.format(
                connect_svn=CONNECT_TO_SVN,
                svn_reps_path=SVN_REPS_PATH,
                shortname=repository_name,
            ),
            timeout=None,
            silentFail=False,
            shell=True,
        )
        existence = output[:1]
        return bool(existence.strip() == str(0))
    except NonZeroReturnCode:
        return False
    return True


def get_svn_last_wrote(projkey):
    '''
        Checks the date of the file db/current to return the last modified date
    '''
    try:
        output = runCommand(
            command='{connect_svn} "{script}"'.format(
                connect_svn=CONNECT_TO_SVN,
                script="if test -f /reps/{svnproject}/db/current; then echo 0; else echo -1; fi".format(svnproject=projkey),
            ),
            timeout=None,
            silentFail=False,
            shell=True,
        )
        test = output[:1]
        if test.strip() == str(0):
            output = runCommand(
                command='{connect_svn} "{script} {repository}"'.format(
                    connect_svn=CONNECT_TO_SVN,
                    script='/bin/date +%s -r',
                    # script='date',
                    repository="/reps/" + projkey + '/db/current',
                ),
                timeout=None,
                silentFail=True,
                shell=True,
            )
            svnlogger(
                level='info',
                message=projkey + " " + output,
                email_admins=False,
            )
        else:
            return None
    except OSError as ose:
        forgelogger('error',
                    'Command failed (OSError {errno}): {strerror}\nWhen: {connect_svn} "{script} {repository}"'.format(
                        errno=ose.errno,
                        strerror=ose.strerror,
                        connect_svn=CONNECT_TO_SVN,
                        script='/bin/date +%s -r',
                        repository=projkey+'/db/current',
                    ),
                    False)
        return None
    except NonZeroReturnCode as nze:
        forgelogger('error',
                    'NonZeroReturnCode (%s): %s' % (nze.returncode, nze.stderr),
                    False)
        return None

    try:
        return date.fromtimestamp(float(output.split('\n')[0]))
    except ValueError as verr:
        forgelogger('error', str(verr), False)
        return None
    except UnboundLocalError as ule:
        forgelogger('error', str(ule), False)
        return None


def get_svn_last_wrote_user(projkey):
    '''
        Checks the username of the last svn operation
    '''
    try:
        output = runCommand(
            command='{connect_svn} "{script}"'.format(
                connect_svn=CONNECT_TO_SVN,
                script="if test -f /reps/{svnproject}/db/current; then echo 0; else echo -1; fi"
                .format(svnproject=projkey),
            ),
            timeout=None,
            silentFail=False,
            shell=True,
        )
        project_name = output[:1]
        if project_name.strip() == str(0):
            output = runCommand(
                command='{connect_svn} "{script}"'.format(
                    connect_svn=CONNECT_TO_SVN,
                    script='svn log file:///reps/{0} | head -6'.format(projkey)
                ),
                timeout=None,
                silentFail=True,
                shell=True,
            )
            # output = unicode(output, errors='ignore')
            try:
                user = output.split("\n")[1].split(" | ")[1]
                return user
            except Exception:
                return "-"
        else:
            return "-"
    except OSError as ose:
        forgelogger('error', 'Command failed (OSError {errno}): {strerror}\nWhen: svn log file:///reps/{repo} | head -6"'.format(
            errno=ose.errno,
            strerror=ose.strerror,
            repo=projkey,
        ), False)
        return "-"
    except NonZeroReturnCode as nze:
        forgelogger('error', 'NonZeroReturnCode (%s): %s' % (nze.returncode, nze.stderr), False)
        return "-"


def get_svn_httpdlog_list(lxsvnparam):
    '''
        Connects to the provided SVN host and retreives the content of
        HTTPD_LATEST_LOGFILE (/var/log/httpd/latest_logfile)
    '''
    output = runCommand(
        command='{connect_svn} "{script}" {lxsvn}'.format(
            lxsvn="root@" + lxsvnparam,
            connect_svn=CONNECT_TO_SVN,
            script='cat {location}'.format(location=HTTPD_LATEST_LOGFILE)
        ),
        timeout=None,
        silentFail=False,
        shell=True,
    )
    return output


def get_svn_sshlog_list(lxsvnparam):
    '''
        Connects to the provided SVN host and retreives the content of
        SSH_LATEST_LOGFILE (/var/log/svnserve/repository_logs/latest_logfile)
    '''
    output = runCommand(
        command='{connect_svn} "{script}" {lxsvn}'.format(
            lxsvn="root@" + lxsvnparam,
            connect_svn=CONNECT_TO_SVN,
            script='cat {location}'.format(location=SSH_LATEST_LOGFILE)
        ),
        timeout=None,
        silentFail=False,
        shell=True,
    )
    return output


def get_last_access():
    '''
        For every project, consults the log files in the SVN hosts, to
        determinate the last access date for each of them
    '''
    lxsvn_list = get_cluster_list()

    lxsvn_list_results = []
    for lxsvn in lxsvn_list:
        output = get_svn_httpdlog_list(lxsvn)
        output_split = output.decode().split('\n')
        output_split.pop()  # Need to remove console kill line
        output_split.pop()  # Need to remove new line
        for rep_last_access in output_split:
            info = rep_last_access.split(" ")
            proj_info = ProjectAccessInfo(info[0])
            # time.strptime(info[1], "%d/%b/%Y:%H:%M:%S")
            proj_info.date_access = info[1]
            # This step is just for old machines that still don't have last_user
            # on logfiles
            try:
                proj_info.user = info[2]
            except Exception as exc:
                forgelogger('error',
                            'While doing "proj_info.user = info[2]" %s' % str(exc),
                            False)
            if not any(p.name == proj_info.name for p in lxsvn_list_results):
                lxsvn_list_results.append(proj_info)
            else:
                prev_proj_info = next((x for x in lxsvn_list_results if x.name == proj_info.name),
                                      None)
                if time.strptime(prev_proj_info.date_access,
                                 "%d/%b/%Y:%H:%M:%S") < time.strptime(proj_info.date_access,
                                                                      "%d/%b/%Y:%H:%M:%S"):
                    index = lxsvn_list_results.index(prev_proj_info)
                    if proj_info.user == "-":
                        proj_info.user = prev_proj_info.user
                    lxsvn_list_results[index] = proj_info

        output = get_svn_sshlog_list(lxsvn)
        output_split = output.decode().split('\n')
        output_split.pop()  # Need to remove console kill line
        output_split.pop()  # Need to remove new line
        for rep_last_access in output_split:
            svnlogger(
                level='info',
                message=lxsvn + " " + rep_last_access,
                email_admins=False,
            )
            info = rep_last_access.split(" ")
            proj_info = ProjectAccessInfo(info[0])
            # time.strptime(info[1], "%d/%b/%Y:%H:%M:%S")
            proj_info.date_access = info[1]
            # This step is just for old machines that still don't have last_user
            # on logfiles
            try:
                proj_info.user = info[2]
            except Exception as exc:
                forgelogger('error', str(exc), False)
            if not any(p.name == proj_info.name for p in lxsvn_list_results):
                lxsvn_list_results.append(proj_info)
            else:
                prev_proj_info = next((x for x in lxsvn_list_results if x.name == proj_info.name),
                                      None)
                if time.strptime(prev_proj_info.date_access,
                                 "%d/%b/%Y:%H:%M:%S") < time.strptime(proj_info.date_access,
                                                                      "%d/%b/%Y:%H:%M:%S"):
                    index = lxsvn_list_results.index(prev_proj_info)
                    if proj_info.user == "-":
                        proj_info.user = prev_proj_info.user
                    lxsvn_list_results[index] = proj_info

    for rep in lxsvn_list_results:
        projects = SvnProject.objects.filter(shortname=rep.name)

        for pro in projects:
            svnlogger(
                level='info',
                message=rep.name + " " + rep.date_access,  # 23/Sep/2017:13:12:44

                email_admins=False,
            )
            # fulldate = rep.date.split(":")
            # date = fulldate[0].split("/")
            # pro.last_access = datetime.date(int(date[0]), int(date[1]), int(date[2]))
            pro.last_access = datetime.datetime.strptime(rep.date_access, "%d/%b/%Y:%H:%M:%S")
            pro.last_user_access = str(rep.user)
            pro.save()


def get_cluster_list():
    '''
        Get a lis of all hosts in SVN_CLI_PROD "svn/cli/prod"
    '''
    output = runCommand(
        command='{wassh} {hostgroup}'.format(
            wassh=WASSH_SVNADMIN,
            hostgroup=SVN_CLI_PROD,
        ),
        timeout=None,
        silentFail=False,
        shell=True,
    )
    lxsvn_list = output.decode().split("\n")
    lxsvn_list.pop()
    lxsvn_list.pop()
    return lxsvn_list
