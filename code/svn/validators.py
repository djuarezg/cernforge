'''
    SVN froms validators
'''
import re

from django.core.exceptions import ValidationError

import svn

from cernforge.globals import is_afs_account
from cernforge.validators import validate_cern_account, raise_validationerror
from cernforge import cernldap


def add_field_validation(instance, required_fields):
    '''
        Takes an instance of a ModelForm or a ModelSerializer, and applies the
        validators and required rules defined here
    '''
    validators = {
        'shortname': validate_shortname,
        'longname': validate_longname,
        'requestor': validate_cern_account,
        'respuser': validate_cern_account,
    }

    for field_name in instance.fields.keys():
        if field_name in validators:
            instance.fields[field_name].validators.append(validators[field_name])

        instance.fields[field_name].required = field_name in required_fields


def validate_shortname(value, attribute=None):
    '''
        Validates the shortname of the project
    '''
    if not re.compile(r'^[a-zA-Z]+[a-zA-Z0-9\-_]*$').match(value):
        raise_validationerror("Only alphanumeric (and '-', '_') characters are \
allowed, first character must be a letter.", attribute)


def validate_longname(value, attribute=None):
    '''
        Makes sure that the longname (or description) is valid
    '''
    errors = []
    if not re.compile(r'^[a-zA-Z0-9\'\.\,\-/_\s]*$').match(value):
        errors.append("Only alphanumeric (and '/', '-', '_') characters are allowed.")
    if not re.compile(r'^[a-zA-Z]$').match(value[0]):
        errors.append('First character must be a letter.')
    if re.compile('/{2,}').search(value) is not None:
        errors.append("You cannot have more than one consecutive '/' in full repository name.")
    if value[-1] == '/':
        errors.append("The last character cannot be '/'.")
    if errors:
        raise_validationerror(errors, attribute)


def validate_afs_account(value, attribute=None):
    '''
        Checks if the AFS account exists
    '''
    if not is_afs_account(value):
        raise_validationerror(
            "CERN account {account} doesn't exist in AFS.".format(account=value),
            attribute,
        )


def validate_libuser(shortname, libuser, attribute=None):
    '''
        Checks that the account exists and has an AFS valid account
    '''
    validate_cern_account(libuser, attribute)
    validate_afs_account(libuser, attribute)

    if shortname.startswith('atlas-') and libuser != 'alibrari':
        raise_validationerror(
            "The repository has prefix 'atlas-', which is intended for personal \
ATLAS repositories repository. The librarian for these repositories has to be \
'alibrari', not '{0}'.".format(libuser),
            attribute,
        )

    elif 'cernAccountOwner' not in cernldap.get_user_info(libuser):
        raise_validationerror(
            "The librarian ({}) has to be a service account!".format(libuser),
            attribute,
        )


def is_unique_caseinsensitive(shortname, attribute=None):
    '''
        We only want to run this check if the SVN repository with this exact shortname
        does not exist
    '''
    try:
        svn.models.SvnProject.objects.get(pk=shortname)
    except svn.models.SvnProject.DoesNotExist:
        if svn.models.SvnProject.objects.filter(shortname__iexact=shortname).count() > 0:
            raise_validationerror(
                'SVN repositories are case-insensitive. By choosing this name, \
you collide with an already existing repository name.',
                attribute,
            )
