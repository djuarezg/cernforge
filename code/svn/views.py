'''
    SVN views
'''
import copy
import re

from datetime import date

from django.db.models import Q
from django.core.mail import EmailMultiAlternatives
from django.core.cache import cache
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.utils.safestring import mark_safe
from django.template import loader

from cernforge.log import forgelogger

from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.parsers import JSONParser
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from rest_framework.response import Response
from rest_framework.exceptions import ParseError

from cernforge.exceptions import NonZeroReturnCode, Timeout
from cernforge.decorators import runAsynchronously
from cernforge.globals import (
    runCommand,
    getAfsInfo,
    ldap_authentication,
    JSONResponse,
    REMOVE_SVN_PROJECT_SCRIPT,
    CHANGE_SVN_LIBRARIAN_SCRIPT,
    ENABLE_ANONYMOUS_SCRIPT,
    DISABLE_ANONYMOUS_SCRIPT,
    CONNECT_TO_SVN,
    SVN_WIPEOUT_REPOSITORY_SCRIPT,
    SVN_CREATE_REPOSITORY_SCRIPT,
)
from cernforge import cernldap, forgeutils

from cernforge.settings import (
    TRAC_LOCATION,
    TRAC_INIT_LOCKFILE,
    SVN_MIN_QUOTA,
    SVN_MAX_QUOTA,
)

from projecttable.utils import createFieldData, limitQueryset

from svn.forms import SvnForm, SvnFormAtlasPersonal, SvnDetailsStatusForm
from svn.serializers import NewSvnProjectSerializer, ExistingSvnProjectSerializer

from svn.models import (
    SvnProject,
    DEFAULT_SVN_PERMISSION,
    CHOICES_SVN_PERMISSION
)

from svn.log import svnlogger
from svn.mail import sendRequestedRepository
from svn.utils import (
    get_trac_info,
    has_trac,
    # get_cluster_list,
    get_svn_last_wrote_user,
    check_permissions_project,
)


@ldap_authentication
@api_view(["GET", "POST"])
@renderer_classes((TemplateHTMLRenderer, TemplateHTMLRenderer))
def svn_form(request, user):
    '''
        This view displays form to request a new SVN repository.
    '''

    # Only admins can see this
    if not user['admin']:
        raise PermissionDenied
        #return HttpResponse(status=status.HTTP_403_FORBIDDEN)

    # Values to be used if the user chooses 'Advanced', then 'Basic'
    initial_data = {
        'requestor': user['login'],
        'restrictionlevel': DEFAULT_SVN_PERMISSION,
    }

    if request.method == 'GET':
        form = SvnForm(initial=initial_data)
        return Response(
            {
                'form': form,
                'displayName': user['fullname'],
                'username': user['login'],
                'admin': user['admin'],
                'impersonate': user['impersonate'],
            },
            template_name='svn/svn_form.html',
        )

    elif request.method == 'POST':
        return _do_svn_post(request, user, 'SvnForm', initial_data)

@ldap_authentication
@api_view(["GET", "POST"])
@renderer_classes((TemplateHTMLRenderer, TemplateHTMLRenderer))
def svn_form_atlaspersonal(request, user):
    '''
        This view displays form to request a new personal SVN repository for Atlas users
    '''

    return Response(
        {
            'displayName': user['fullname'],
            'username': user['login'],
        },
        template_name='svn/svn_form_atlaspersonal-blocked.html',
    )

def _do_svn_post(request, user, form_name, initial_data):
    post = request.POST.copy()

    if form_name == 'SvnForm':
        form = SvnForm(post, initial=initial_data)
    elif form_name == 'SvnFormAtlasPersonal':
        form = SvnFormAtlasPersonal(post, initial=initial_data)

    if form.is_valid():

        repo = form.save()

        sendRequestedRepository(
            repo=repo,
            sender=user['email'],
            justification=form.data['justification'],
        )

        return Response({
            'success_message': mark_safe("""
                <p>Your SVN repository has been requested.</p>
                    <p>An SVN Administrator <strong>will now review</strong> the request.</p>
                    <p>When accepted, the repository will typically be created within a few hours.</p>
                """.strip()),
            'displayName': user['fullname'],
            'username': user['login'],
            'admin': user['admin'],
            'impersonate': user['impersonate'],
            },
                        template_name="cernforge/success_page.html"
                       )
    else:
        if form_name == 'SvnForm':
            template_name = 'svn/svn_form.html'
        elif form_name == 'SvnFormAtlasPersonal':
            template_name = 'svn/svn_form_atlaspersonal.html'

        return Response({
            'form': form,
            'displayName': user['fullname'],
            'username': user['login'],
            'admin': user['admin'],
            'impersonate': user['impersonate'],
        },
                        template_name=template_name,
                        status=status.HTTP_400_BAD_REQUEST
                       )

@ldap_authentication
@api_view(['GET'])
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def svn_myprojects(request, user):
    '''
        This view displays all SVN repositories where the user is the librarian or the owner of it.
    '''

    field_names = (
        'shortname',
        'longname',
        #'requestor',
        'libuser',
        'restrictionlevel',
        'status',
        #'location',
        'create_date',
        'last_wrote',
        'last_access',
        #'hash',
    )

    # https://docs.djangoproject.com/en/1.7/ref/models/querysets/#field-lookups
    field_lookups = {
        'shortname': 'icontains',
        'longname': 'icontains',
        #'requestor': 'iexact',
        'libuser': 'iexact',
        #'restrictionlevel': 'iexact',
        'status': 'exact',
        #'location': 'icontains',
        'create_date': 'contains',
        'last_wrote': 'contains',
        'last_access': 'contains'
                       #'hash': 'iexact',
    }



    field_data = createFieldData(
        field_names=field_names,
        admin_fields=[],
        field_lookups=field_lookups,
        model=SvnProject,
    )

    repositories = limitQueryset(
        queryset=SvnProject.objects.filter(Q(libuser=user['login'])|Q(respuser=user['login'])),
        request=request,
        field_data=field_data,
    )

    for repo in repositories:
        repo.shortname = mark_safe('<a href="/svn/details/{0}/">{0}</a>'.format(repo.shortname))
        if repo.restrictionlevel in dict(CHOICES_SVN_PERMISSION):
            repo.restrictionlevel = '{0} ({1})'.format(
                repo.restrictionlevel,
                dict(CHOICES_SVN_PERMISSION)[repo.restrictionlevel],
            )

    data = {
        'projecttable': {
            'projects': repositories,
            'field_data': field_data,
            'field_names': field_names,
        },
        'admin': user['admin'],
    }

    if not request.is_ajax():
        data.update({
            'displayName': user['fullname'],
            'username': user['login'],
            'impersonate': user['impersonate'],
        })
        data['projecttable'].update({
            'showheader': True,
            'search_placeholder': 'Search in my SVN projects ...',
            'sortable': True,
        })

        return Response(
            data,
            template_name='svn/svn_myprojects.html',
        )
    return JSONResponse(
        {
            'projecttable_tbody': loader.render_to_string(
                'projecttable/projecttable_tbody.html',
                data,
            ),
        },
        status=status.HTTP_200_OK,
    )


@ldap_authentication
@api_view(['GET'])
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def svn_allprojects(request, user):
    '''
        This view displays all SVN repositories.
    '''

    field_names = (
        'shortname',
        'longname',
        'requestor',
        'libuser',
        'restrictionlevel',
        'status',
        #'location',
        'create_date',
        'last_wrote',
        'last_access',
        'hash',
    )

    admin_fields = (
        'requestor',
        'location',
        'hash',
    )

    # https://docs.djangoproject.com/en/1.7/ref/models/querysets/#field-lookups
    field_lookups = {
        'shortname': 'icontains',
        'longname': 'icontains',
        'requestor': 'iexact',
        'libuser': 'iexact',
        #'restrictionlevel': 'iexact',
        'status': 'exact',
        'create_date': 'contains',
        'last_wrote': 'contains',
        'last_access': 'contains',
        'hash': 'exact',
    }

    field_data = createFieldData(
        field_names=field_names,
        admin_fields=admin_fields,
        field_lookups=field_lookups,
        model=SvnProject,
    )

    repositories = limitQueryset(
        queryset=SvnProject.objects.all(),
        request=request,
        field_data=field_data,
        is_admin=user['admin'],
    )

    for repo in repositories:
        if not user['admin']:
            if repo.restrictionlevel in dict(CHOICES_SVN_PERMISSION):
                repo.restrictionlevel = '{0} ({1})'.format(
                    repo.restrictionlevel,
                    dict(CHOICES_SVN_PERMISSION)[repo.restrictionlevel],
                )
        if user['admin'] or repo.respuser == user['login'] or repo.libuser == user['login']:
            repo.shortname = mark_safe('<a href="/svn/details/{0}/">{0}</a>'.format(repo.shortname))

    data = {
        'projecttable': {
            'projects': repositories,
            'field_data': field_data,
            'field_names': field_names,
        },
        'admin': user['admin'],
    }

    if not request.is_ajax():
        data.update({
            'displayName': user['fullname'],
            'username': user['login'],
            'impersonate': user['impersonate'],
        })
        data['projecttable'].update({
            'showheader': True,
            'search_placeholder': 'Search in all SVN projects ...',
            'sortable': True,
        })

        return Response(
            data,
            template_name='svn/svn_allprojects.html',
        )
    return JSONResponse(
        {
            'projecttable_tbody': loader.render_to_string(
                'projecttable/projecttable_tbody.html',
                data,
            ),
        },
        status=status.HTTP_200_OK,
    )

@ldap_authentication
@api_view(["GET"])
@renderer_classes((JSONRenderer, JSONRenderer))
def svn_last_modified(request, user, api_version=2):
#def svn_last_modified(request, user):
    '''
    This view displays all active SVN repositories where the librarian (libuser)
    or its owner (respuser) does not exist in LDAP.
    '''

    # Only admins can see this
    if not user['admin']:
        return HttpResponse(status=status.HTTP_403_FORBIDDEN)


    repositories = cache.get('svnprojects_modified')
    if not repositories:
        projects = SvnProject.objects.filter(status__exact='active')

        repositories = []
        for project in projects:
            repositories.append({
                'shortname': project.shortname,
                'last_modified': svn_lastModifiedDate(project),
            })

        cache.set('svnprojects_modified', repositories, 5 * 60)

    return JSONResponse(
        #ExistingSvnProjectSerializer(repositories, many=True).data,
        repositories,
        status=status.HTTP_200_OK
    )

# @cache_page(9000) # 15min
@ldap_authentication
@api_view(['GET'])
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def svn_allprojects_obsolete(request, user):
    '''
    This view displays all obsolete and archived SVN repositories
    '''

    # Only admins can see this
    #if not user['admin']:
    #    return HttpResponse(status=status.HTTP_403_FORBIDDEN)

    field_names = (
        'shortname',
        'requestor',
        'libuser',
        'respuser',
        'create_date',
        'last_wrote',
        'last_access',
        'hash',
        'status',
    )

    # https://docs.djangoproject.com/en/1.7/ref/models/querysets/#field-lookups
    field_lookups = {
        'shortname': 'icontains',
        'requestor': 'iexact',
        'libuser': 'iexact',
        'respuser': 'iexact',
        'create_date': 'contains',
        'last_wrote': 'contains',
        'last_access': 'contains',
        'hash': 'exact',
        'status': 'exact',
    }

    field_data = createFieldData(
        field_names=field_names,
        admin_fields=[],
        field_lookups=field_lookups,
        model=SvnProject,
    )

    obsolete = (request.GET.get('type') == 'obsolete')
    if obsolete:
        invalid_projects = cache.get('svnprojects_obsolete')
    else:
        invalid_projects = cache.get('svnprojects_obsolete_archived')

    if not invalid_projects:
        if obsolete:
            invalid_projects_queryset = SvnProject.objects.filter(status__in=['obsolete'])
        else:
            invalid_projects_queryset = SvnProject.objects.filter(status__in=['obsolete',
                                                                              'archived'])

        invalid_projects = {
            'queryset': invalid_projects_queryset,
        }

    if obsolete:
        cache.set('svnprojects_obsolete', invalid_projects, 5 * 60)
    else:
        cache.set('svnprojects_obsolete_archived', invalid_projects, 5 * 60)

    repositories = limitQueryset(
        queryset=invalid_projects['queryset'],
        request=request,
        field_data=field_data,
        is_admin=user['admin'],
    )

    data = {
        'projecttable': {
            'projects': repositories,
            'field_data': field_data,
            'field_names': field_names,
            'sortable': True,
        },
        'admin': user['admin'],
    }

    if not request.is_ajax():
        data.update({
            'displayName': user['fullname'],
            'username': user['login'],
            'impersonate': user['impersonate'],
        })

        return Response(
            data,
            template_name='svn/svn_allprojects_obsolete.html',
        )
    return JSONResponse(
        {
            'projecttable_tbody': loader.render_to_string(
                'projecttable/projecttable_tbody.html',
                data,
            ),
        },
        status=status.HTTP_200_OK,
    )


# @cache_page(9000) # 15min
@ldap_authentication
@api_view(['GET'])
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def svn_allprojects_unmanaged(request, user):
    '''
    This view displays all active SVN repositories where the librarian (libuser)
    or its owner (respuser) does not exist in LDAP.
    '''

    # Only admins can see this
    if not user['admin']:
        return HttpResponse(status=status.HTTP_403_FORBIDDEN)

    field_names = (
        'shortname',
        'requestor',
        'libuser',
        'respuser',
        'create_date',
        'last_wrote',
        'last_access',
        'hash',
    )

    # https://docs.djangoproject.com/en/1.7/ref/models/querysets/#field-lookups
    field_lookups = {
        'shortname': 'icontains',
        'requestor': 'iexact',
        'libuser': 'iexact',
        'respuser': 'iexact',
        'create_date': 'contains',
        'last_wrote': 'contains',
        'last_access': 'contains',
        'hash': 'exact',
    }

    field_data = createFieldData(
        field_names=field_names,
        admin_fields=[],
        field_lookups=field_lookups,
        model=SvnProject,
    )

    all_users = set(cernldap.all_users().keys())

    orphans = (request.GET.get('type') == 'orphans')
    if orphans:
        invalid_projects = cache.get('svnprojects_invalid_orphans')
    else:
        invalid_projects = cache.get('svnprojects_invalid')

    if not invalid_projects:
        allreps = SvnProject.objects.filter(status__exact='active').exclude(shortname__startswith='atlas-')

        invalid_shortnames_requestor = set([x.shortname for x in allreps if x.requestor not in all_users])
        invalid_shortnames_libuser = set([x.shortname for x in allreps if x.libuser not in all_users])
        invalid_shortnames_respuser = set([x.shortname for x in allreps if x.respuser not in all_users])
        if orphans:
            invalid_projects_queryset = SvnProject.objects.filter(shortname__in=list(invalid_shortnames_libuser & invalid_shortnames_respuser))
        else:
            invalid_projects_queryset = SvnProject.objects.filter(shortname__in=list(invalid_shortnames_libuser | invalid_shortnames_respuser))
        invalid_projects = {
            'requestor': invalid_shortnames_requestor,
            'libuser': invalid_shortnames_libuser,
            'respuser': invalid_shortnames_respuser,
            'queryset': invalid_projects_queryset,
        }
        if orphans:
            cache.set('svnprojects_invalid_orphans', invalid_projects, 5 * 60)
        else:
            cache.set('svnprojects_invalid', invalid_projects, 5 * 60)

    repositories = limitQueryset(
        queryset=invalid_projects['queryset'],
        request=request,
        field_data=field_data,
        is_admin=user['admin'],
    )
    for rep in repositories:
        if rep.shortname in invalid_projects['requestor']:
            rep.requestor = mark_safe('<span class="red">{0}</span>'.format(rep.requestor))
        if rep.shortname in invalid_projects['libuser']:
            rep.libuser = mark_safe('<span class="red">{0}</span>'.format(rep.libuser))
        if rep.shortname in invalid_projects['respuser']:
            rep.respuser = mark_safe('<span class="red">{0}</span>'.format(rep.respuser))
        rep.shortname = mark_safe('<a href =\'/svn/details/{0}/\'>{0}</a>'.format(rep.shortname))

    data = {
        'projecttable': {
            'projects': repositories,
            'field_data': field_data,
            'field_names': field_names,
        },
        'admin': user['admin'],
    }

    if not request.is_ajax():
        data.update({
            'displayName': user['fullname'],
            'username': user['login'],
            'impersonate': user['impersonate'],
        })
        data['projecttable'].update({
            'showheader': True,
            'search_placeholder': 'Search in all invalid SVN projects ...',
            'sortable': True,
        })

        return Response(
            data,
            template_name='svn/svn_allprojects_unmanaged.html',
        )
    return JSONResponse(
        {
            'projecttable_tbody': loader.render_to_string(
                'projecttable/projecttable_tbody.html',
                data,
            ),
        },
        status=status.HTTP_200_OK,
    )


@ldap_authentication
@api_view(["GET"])
@renderer_classes((JSONRenderer, JSONRenderer))
def svn_unmanaged(request, user, api_version=2):
    '''
    This view displays all active SVN repositories where the librarian (libuser)
    or its owner (respuser) does not exist in LDAP.
    '''

    # Only admins can see this
    if not user['admin']:
        return HttpResponse(status=status.HTTP_403_FORBIDDEN)

    all_users = set(cernldap.all_users().keys())

    invalid_projects = cache.get('svnprojects_invalid')
    if not invalid_projects:
        allreps = SvnProject.objects.filter(status__exact='active').exclude(shortname__startswith='atlas-')
        invalid_shortnames_requestor = set([x.shortname for x in allreps if x.requestor not in all_users])
        invalid_shortnames_libuser = set([x.shortname for x in allreps if x.libuser not in all_users])
        invalid_shortnames_respuser = set([x.shortname for x in allreps if x.respuser not in all_users])
        invalid_projects_queryset = SvnProject.objects.filter(shortname__in=list(invalid_shortnames_libuser | invalid_shortnames_respuser))
        invalid_projects = {
            'requestor': invalid_shortnames_requestor,
            'libuser': invalid_shortnames_libuser,
            'respuser': invalid_shortnames_respuser,
            'queryset': invalid_projects_queryset,
        }
        cache.set('svnprojects_invalid', invalid_projects, 5 * 60)


    repositories = invalid_projects['queryset']

    for rep in repositories:
        if rep.shortname in invalid_projects['requestor'] and rep.requestor:
            rep.requestor = mark_safe('[{0}]'.format(rep.requestor))
        if rep.shortname in invalid_projects['libuser']:
            rep.libuser = mark_safe('[{0}]'.format(rep.libuser))
        if rep.shortname in invalid_projects['respuser']:
            rep.respuser = mark_safe('[{0}]'.format(rep.respuser))

    return JSONResponse(
        #ExistingSvnProjectSerializer(repositories, many=True).data,
        ExistingSvnProjectSerializer(repositories, many=True).data,
        status=status.HTTP_200_OK
    )

@ldap_authentication
@api_view(["GET", "POST", "DELETE", "PUT"])
@renderer_classes((JSONRenderer, JSONRenderer))
def svn(request, user, api_version, pk=None):
    '''
        This view is for CRUD operations for SVN repositories.
        GET - get list of all repositories
        GET with given pk - get details of repository with given primary key
        POST - request a new repository
        PUT - update repository
        DELETE - delete repository
    '''
    if request.method == 'GET':
        if pk == None:
            serializer = ExistingSvnProjectSerializer(SvnProject.objects.all(), many=True)
        else:
            repository = get_object_or_404(SvnProject, pk=pk)
            serializer = ExistingSvnProjectSerializer(instance=repository)

        return JSONResponse(serializer.data)

    # request the new SVN repository
    elif request.method == 'POST':
        if pk != None:
            return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)

        json = JSONParser().parse(request)
        json['libuser'] = json['requestor'] if 'requestor' in json else None
        serializer = NewSvnProjectSerializer(data=json)
        if serializer.is_valid():

            repo = serializer.save()

            sendRequestedRepository(
                repo=repo,
                sender=user['email'],
            )

            return JSONResponse({
                'message': 'Your SVN repository has been requested.\nAn SVN Administrator will now review the request.\nWhen accepted, the repository will typically be created within a few hours.',
                'repository_data': serializer.data,
            })

        return JSONResponse(
            {'errors': serializer.errors},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # update existing SVN repository
    # you don't have to specify value for every field, you can omit fields you don't wanna change
    elif request.method == 'PUT':
        if pk == None:
            return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)

        repository = get_object_or_404(SvnProject, pk=pk)

        if not (user['admin'] or user['login'] in [repository.libuser, repository.respuser]):
            return HttpResponse(status=status.HTTP_403_FORBIDDEN)

        if repository.status != 'active':
            return JSONResponse(
                {'errors': {'general': ['This repository is not active.']}},
                status=status.HTTP_400_BAD_REQUEST,
            )

        try:
            json = JSONParser().parse(request)
        except ParseError:
            return JSONResponse(
                {'errors': {'general': ["Malformed PUT request received for repository '{shortname}, did not succeed in parsing the request as JSON.'".format(shortname=pk)]}},
                status=status.HTTP_400_BAD_REQUEST,
            )

        repository_before = copy.copy(repository)

        # The serializer will not throw an error if the user tries to change ai
        # read-only field, it will simply not change it and return 200 OK
        # returning a dataset with the old value.
        #
        # For legacy, (and good practice?) reasons, we throw a 400 BAD REQUEST
        # error manually if the user attempts this
        errors = {}
        for exe in ExistingSvnProjectSerializer.Meta.read_only_fields:
            if exe in json and json[exe] != getattr(repository, exe):
                errors[exe] = ['Read-only attribute.']
        if errors:
            return JSONResponse(
                {'errors': errors},
                status=status.HTTP_400_BAD_REQUEST,
            )

        if errors:
            return JSONResponse(errors, status=status.HTTP_400_BAD_REQUEST)

        serializer = ExistingSvnProjectSerializer(instance=repository,
                                                  data=json, partial=True)
        if serializer.is_valid():
            serializer.save()
            if repository_before.libuser != serializer.data['libuser']:
                changeLibrarian(repository_before, repository)
            if repository_before.restrictionlevel != serializer.data['restrictionlevel']:
                changeRestrictionLevel(repository_before, repository)

            return JSONResponse(serializer.data)
        else:
            return JSONResponse(
                {'errors': serializer.errors},
                status=status.HTTP_400_BAD_REQUEST,
            )

    elif request.method == 'DELETE':
        if pk is None:
            return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)

        repository = get_object_or_404(SvnProject, pk=pk)

        if not (user['admin'] or user['login'] in [repository.libuser, repository.respuser]):
            return HttpResponse(status=status.HTTP_403_FORBIDDEN)

        if repository.status != 'active':
            return JSONResponse(
                {'errors': {'general': ['This repository is not active.']}},
                status=status.HTTP_400_BAD_REQUEST,
            )

        deleteRepository(repository)
        return HttpResponse(status=status.HTTP_200_OK)

@ldap_authentication
@api_view(["GET", "POST"])
@renderer_classes((JSONRenderer, JSONRenderer))
def svn_quota(request, api_version, user, pk=None):
    '''
        This view is for displaying AFS volume's info [GET] and changing its quota [POST].
        Available only for existing, active repositories.
    '''
    repository = get_object_or_404(SvnProject, pk=pk)

    if not (user['admin'] or user['login'] in [repository.libuser, repository.respuser]):
        return HttpResponse(status=status.HTTP_403_FORBIDDEN)

    if repository.status != 'active':
        return JSONResponse(
            {'errors': {'general': ['This repository is not active.']}},
            status=status.HTTP_400_BAD_REQUEST,
        )

    afs_info = getAfsInfo(repository.location)

    if afs_info is None:
        return JSONResponse(
            {'errors': {'general': ["Cannot get repository's info from AFS."]}},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )

    if request.method == 'GET':
        return JSONResponse(afs_info, status=status.HTTP_200_OK)

    elif request.method == 'POST':
        json_data = JSONParser().parse(request)
        #oldQuota = afs_info['quota']

        if "quota" in json_data:


            try:
                newQuota = int(json_data['quota'])
            except ValueError:
                return JSONResponse(
                    {'quota': ['A number is required.']},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            else:
                if newQuota <= SVN_MIN_QUOTA or newQuota >= SVN_MAX_QUOTA:
                    return JSONResponse(
                        {'quota': ['Quota must be a number between %d and %d.' % (SVN_MIN_QUOTA, SVN_MAX_QUOTA)]},
                        status=status.HTTP_400_BAD_REQUEST,
                    )
        else:
            return JSONResponse({"quota":["This field is required"]}, status=status.HTTP_400_BAD_REQUEST)

        try:
            runCommand(
                command='{connect_svn} "{afs_command} {repository_path} {new_quota}"'.format(
                    connect_svn=CONNECT_TO_SVN,
                    #k5reauth=K5REAUTH_AS_FORADMIN,
                    afs_command='afs_admin sq',
                    repository_path=repository.location,
                    new_quota=str(newQuota * 1000),
                ),
                timeout=15.0,
                silentFail=False,
                shell=True,
            )
        except (NonZeroReturnCode, Timeout):
            # TO-DO: set silentFail = True on the runCommand and instead send a more specific email here
            # to the admins.
            return HttpResponse(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return HttpResponse(status=status.HTTP_200_OK)

def svn_lastModifiedDate(repository):
    '''
        Checks the date of the file db/current to return the last modified date
    '''
    try:
        output = runCommand(
            command='{connect_svn} "{script} {repository}"'.format(
                connect_svn=CONNECT_TO_SVN,
                script='/bin/date +%s -r',
                #script='date',
                repository=repository.location+'/db/current',
            ),
            timeout=None,
            silentFail=True,
            shell=True,
        )
    except OSError as ose:
        forgelogger('error', 'Command failed (OSError {errno}): {strerror}\nWhen: {connect_svn} "{script} {repository}"'.format(
            errno=ose.errno,
            strerror=ose.strerror,
            connect_svn=CONNECT_TO_SVN,
            script='/bin/date +%s -r',
            repository=repository.location+'db/current',
        ), False)
        return 0
    except NonZeroReturnCode as nze:
        forgelogger('error', 'NonZeroReturnCode (%s): %s' % (nze.returncode, nze.stderr), False)
        return 0

    try:
        return date.fromtimestamp(float(output.decode().split('\n')[0]))
    except ValueError as ver:
        forgelogger('error', str(ver), False)
        return 0
    except UnboundLocalError as ule:
        forgelogger('error', str(ule), False)
        return 0

def svn_lastAccessDate(repository):
    '''
        Checks the date of the file db/current to return the last access date
    '''
    #projects = SvnProject.objects.filter(shortname=repository)
    return repository.last_access

def svn_lastAccessUser(repository):
    '''
        Checks the date of the file db/current to return the last access date
    '''
    #projects = SvnProject.objects.filter(shortname=repository)
    return repository.last_user_access

#def svn_lastModifiedDate(repository):
#    try:
#        mtime = time.gmtime(os.path.getmtime(repository.location+'/db/current'))
#    except OSError as e:
#        print(e)
#        return 0
#    else:
#        return datetime.date(year=mtime.tm_year, month=mtime.tm_mon, day=mtime.tm_mday)


@ldap_authentication
@api_view(["GET"])
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def svn_details(request, pk, user):
    '''
        This view displays given repositories details.
    '''
    repository = get_object_or_404(SvnProject, pk=pk)


    if not (user['admin'] or user['login'] in [repository.libuser, repository.respuser]):
        return HttpResponse(status=status.HTTP_403_FORBIDDEN)

    afs_info = getAfsInfo(repository.location)

    # check if there is a Trac for this repo
    trac = has_trac(repository.shortname)
    last_changed_date = svn_lastModifiedDate(repository)
    last_access_date = svn_lastAccessDate(repository)
    last_write_user = get_svn_last_wrote_user(repository)
    last_access_user = svn_lastAccessUser(repository)

    if repository.restrictionlevel in dict(CHOICES_SVN_PERMISSION):
        repository.restrictionlevelHR = dict(CHOICES_SVN_PERMISSION)[repository.restrictionlevel]

    return Response({
        'displayName': user['fullname'],
        'username': user['login'],
        'admin': user['admin'],
        'impersonate': user['impersonate'],
        'data': repository,
        'pk': pk,
        'afs_info': afs_info,
        'has_trac': trac,
        'last_changed_date': last_changed_date,
        'last_access_date': last_access_date,
        'last_write_user': last_write_user,
        'last_access_user': last_access_user,
    }, template_name="svn/svn_details.html")

@ldap_authentication
@api_view(["GET", "POST"])
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def svn_status_form(request, pk, user):
    '''
        This view displays given repositories details and a form to give info about its migration status.
    '''
    repository = get_object_or_404(SvnProject, pk=pk)

    if not (user['admin'] or user['login'] in [repository.libuser, repository.respuser]):
        return HttpResponse(status=status.HTTP_403_FORBIDDEN)

    # check if there is a Trac for this repo
    last_changed_date = svn_lastModifiedDate(repository)
    last_access_date = svn_lastAccessDate(repository)
    last_write_user = get_svn_last_wrote_user(repository)
    last_access_user = svn_lastAccessUser(repository)

    if repository.restrictionlevel in dict(CHOICES_SVN_PERMISSION):
        repository.restrictionlevelHR = dict(CHOICES_SVN_PERMISSION)[repository.restrictionlevel]

    if request.method == 'GET':

        form = SvnDetailsStatusForm(initial={'migration_status': str(repository.migration_status)})
        prevents_list = repository.migration_prevents.split(",")
        prevents_list = filter(None, prevents_list)
        prevents_list_int = [int(numeric_string) for numeric_string in prevents_list]
        form.fields['migration_prevents'].initial = prevents_list_int

        return Response(
            {
                'form': form,
                'displayName': user['fullname'],
                'username': user['login'],
                'admin': user['admin'],
                'impersonate': user['impersonate'],
                'data': repository,
                'pk': pk,
                'last_changed_date': last_changed_date,
                'last_access_date': last_access_date,
                'last_write_user': last_write_user,
                'last_access_user': last_access_user,
            },
            template_name='svn/svn_status_form.html',
        )

    elif request.method == 'POST':
        return _do_svn_details_form_post(request, user, 'SvnDetailsStatusForm', repository)

def _do_svn_details_form_post(request, user, form_name, repository):
    post = request.POST.copy()

    form = SvnDetailsStatusForm(post)
    if form.is_valid():
        if repository.status == 'active':
            options_prevents = ''
            for option in request.POST.getlist("migration_prevents"):
                options_prevents = options_prevents + ',' + option

            repository.migration_status = form.cleaned_data.get("migration_status")
            repository.migration_prevents = options_prevents

            repository.save()
            return Response({
                'success_message': mark_safe("""
                    <p>Thank you for collaboration.</p>
                    """.strip()),
                'displayName': user['fullname'],
                'username': user['login'],
                'admin': user['admin'],
                'impersonate': user['impersonate'],
                },
                            template_name="cernforge/success_page.html"
                           )
        return HttpResponse(status=status.HTTP_403_FORBIDDEN)
    return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

@ldap_authentication
@api_view(["DELETE"])
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def svn_archive_repo(request, user, api_version, pk):
    svnlogger(
        level='info',
        message='STARTING ARCHIVING',
        email_admins=False,
        )
    repository = get_object_or_404(SvnProject, pk=pk)
    svnlogger(
        level='info',
        message='ARCHIVING1: ' + str(repository.shortname),
        email_admins=False,
        )

    if not (user['admin'] or user['login'] in [repository.libuser, repository.respuser]):
        return HttpResponse(status=status.HTTP_403_FORBIDDEN)

    if repository.status != 'active':
        return JSONResponse(
            {'errors': {'general': ['This repository is not active or marked for archival.']}},
            status=status.HTTP_400_BAD_REQUEST,
        )
    else:
        try:
            archive_project(pk)
            svnlogger(
                level='info',
                message='ARCHIVING FINISHED: ',
                email_admins=False,
            )
            return HttpResponse(status=status.HTTP_200_OK)
        except Exception as e:
            svnlogger(
                level='error',
                message='Error info: {0}'.format(e),
                email_admins=False,
            )
            return JSONResponse(
                {'errors': {'general': ['An error occurred. Make sure that SVN project has not been already archived or is in the process.']}},
                status=status.HTTP_400_BAD_REQUEST,
            )


@ldap_authentication
@api_view(["POST"])
@renderer_classes((JSONRenderer, JSONRenderer))
def svn_reset_repository(request, user, api_version, pk):
    '''
        This method wipes out given SVN repository, except config files and hook scripts.
        Available only for existing, active repositories.
    '''
    repository = get_object_or_404(SvnProject, pk=pk)

    if not (user['admin'] or user['login'] in [repository.libuser, repository.respuser]):
        return HttpResponse(status=status.HTTP_403_FORBIDDEN)

    if repository.status != 'active':
        return JSONResponse(
            {'errors': {'general': ['This repository is not active.']}},
            status=status.HTTP_400_BAD_REQUEST,
        )

    resetRepository(repository)

    return HttpResponse(status=status.HTTP_200_OK)

@ldap_authentication
@api_view(['POST'])
@renderer_classes((JSONRenderer, JSONRenderer))
def svn_approve_reject(request, user, api_version, pk, choice):
    '''
        End point for REST to approve or reject a svn project
    '''
    return forgeutils.approve_reject(user, pk, choice, SvnProject)

@ldap_authentication
@api_view(["POST"])
@renderer_classes((JSONRenderer, JSONRenderer))
def trac(request, user, api_version, pk, action):
    '''
        This method allows to initialize Trac for SVN repository, resync or upgrade its database.
        Available only for existing, active repositories.
        action = init | resync | upgrade
    '''
    repository = get_object_or_404(SvnProject, pk=pk)

    if not (user['admin'] or user['login'] in [repository.libuser, repository.respuser]):
        return HttpResponse(status=status.HTTP_403_FORBIDDEN)

    if repository.status != 'active':
        return JSONResponse(
            {'errors': {'general': ['This repository is not active.']}},
            status=status.HTTP_400_BAD_REQUEST,
        )

    if action == 'init':
        if has_trac(repository.shortname):
            return JSONResponse(
                {'errors': {'general': ['Trac for this repository already initialized']}},
                status=status.HTTP_400_BAD_REQUEST,
            )

        try:
            runCommand(
                command='{connect_svn} "{test} {trac_init_lockfile}"'.format(
                    connect_svn=CONNECT_TO_SVN,
                    #k5reauth=K5REAUTH_AS_SVNADMIN,
                    test='/usr/bin/test -f',
                    trac_init_lockfile=TRAC_INIT_LOCKFILE,
                ),
                timeout=10.0,
                silentFail=True,
                shell=False,
            )
        except Timeout:
            # The command timeout out
            return JSONResponse(
                {'errors': {'general': ['The SVN administration system is currently busy - please try again later.']}},
                status=status.HTTP_503_SERVICE_UNAVAILABLE
            )
        except NonZeroReturnCode:
            # The lockfile does not exist - initialize trac.
            initializeTrac(repository)
        else:
            # The lockfile exists.
            return JSONResponse(
                {'errors': {'general': ['The SVN administration system is currently busy, please try again later.']}},
                status=status.HTTP_503_SERVICE_UNAVAILABLE
            )

    else:
        if not has_trac(repository.shortname):
            return JSONResponse(
                {'errors': {'general': ["Trac for this repository wasn't initialized"]}},
                status=status.HTTP_400_BAD_REQUEST,
            )
        elif action == 'resync':
            resyncTrac(repository)
        elif action == 'upgrade':
            upgrade_trac(repository)

    return HttpResponse(status=status.HTTP_200_OK)


@runAsynchronously
def changeLibrarian(repo_before, repo):
    '''
        This method will asynchronously run a script which changes the librarian service account for a SVN repository.
    '''
    try:
        svnlogger(
            level='info',
            message='Running command to change the librarian from {0} to {1} for repository {2}.'.format(repo_before.libuser, repo.libuser, repo.shortname),
            email_admins=False,
        )

        libuser_info = cernldap.get_user_info(repo.libuser)
        if 'cernAccountOwner' in libuser_info:
            match = re.match(r'CN=(?P<owner_cn>[^,]+),', libuser_info['cernAccountOwner'][0])
            if match:
                respuser = match.group('owner_cn')
                respuser_info = cernldap.get_user_info(respuser)

                runCommand(
                    command='{connect} "{script} \\"{shortname}\\" \\"{libuser_new}\\" \\"{libuser_old}\\" \\"{respuser}\\" \\"{respname}\\" \\"{respemail}\\""'.format(
                        connect=CONNECT_TO_SVN,
                        script=CHANGE_SVN_LIBRARIAN_SCRIPT,
                        shortname=repo_before.shortname,
                        libuser_new=repo.libuser,
                        libuser_old=repo_before.libuser,
                        respuser=respuser,
                        respname=respuser_info['displayName'][0],
                        respemail=respuser_info['mail'][0],
                    ),
                    timeout=None,
                    silentFail=False,
                    shell=True,
                )

                if respuser != repo.respuser:
                    repo.respuser = respuser
                    repo.save()
    except NonZeroReturnCode:
        # TO-DO: set silentFail = True on the runCommand and instead send a more specific email here to
        # the admins.
        repo.libuser = repo_before.libuser
        repo.save()

@runAsynchronously
def resetRepository(repository):
    '''
        This method will asynchronously run a script which wipes out a SVN repository.
    '''
    try:
        svnlogger(
            level='info',
            message='Running command to reset / wipe out repository {0}.'.format(repository.shortname),
            email_admins=False,
        )
        runCommand(
            command='{connect_svnweb} "{script} \\"{shortname}\\""'.format(
                connect_svnweb=CONNECT_TO_SVN,
                script=SVN_WIPEOUT_REPOSITORY_SCRIPT,
                shortname=repository.shortname,
            ),
            timeout=None,
            silentFail=False,
            shell=True,
        )
    except NonZeroReturnCode:
        # TO-DO: set silentFail = True on the runCommand and instead send a more specific email here to
        # the admins.
        pass

@runAsynchronously
def changeRestrictionLevel(repo_before, repo):
    '''
        This method will asynchronously run a script which enables or disables public access for a SVN repository.
    '''
    svnlogger(
        level='info',
        message='Running command to change the access level from {0} to {1} for repository {2}.'.format(repo_before.restrictionlevel, repo.restrictionlevel, repo.shortname),
        email_admins=False,
    )
    if repo_before.restrictionlevel == 'W':

        # weird thing, but script wants to change it on its own :(
        repo.restrictionlevel = 'W'
        repo.save()
        command = '{connect_svnweb} "{script} \\"{shortname}\\""'.format(
            connect_svnweb=CONNECT_TO_SVN,
            script=DISABLE_ANONYMOUS_SCRIPT,
            shortname=repo_before.shortname,
        )
    else:
        repo.save()
        command = '{connect_svnweb} "{script} \\"{shortname}\\""'.format(
            connect_svnweb=CONNECT_TO_SVN,
            script=ENABLE_ANONYMOUS_SCRIPT,
            shortname=repo_before.shortname,
        )
    try:
        runCommand(
            command=command,
            timeout=None,
            silentFail=False,
            shell=True,
        )
    except NonZeroReturnCode:
        # TO-DO: set silentFail = True on the runCommand and instead send a more specific email here to
        # the admins.
        pass

@runAsynchronously
def deleteRepository(repository):
    '''
        This method will asynchronously run a script which deletes a SVN repository.
    '''
    try:
        svnlogger(
            level='info',
            message='Running command to delete repository {0}.'.format(repository.shortname),
            email_admins=False,
        )

        runCommand(
            command='{connect_svnweb} "{script} {shortname}"'.format(
                connect_svnweb=CONNECT_TO_SVN,
                script=REMOVE_SVN_PROJECT_SCRIPT,
                shortname=repository.shortname,
            ),
            timeout=None,
            silentFail=False,
            shell=True,
        )
    except NonZeroReturnCode:
        # TO-DO: set silentFail = True on the runCommand and instead send a more specific email here to
        # the admins.
        pass
    else:
        repository.delete()

@runAsynchronously
def initializeTrac(repository):
    '''
        This method will asynchronously run a script which initializes Trac for a given SVN repository.
    '''
    try:
        svnlogger(
            level='info',
            message='Running command to initialize trac for repository {0}.'.format(repository.shortname),
            email_admins=False,
        )

        runCommand(
            command='{connect_svnweb} "{script} {shortname} {service_email} -o 0"'.format(
                connect_svnweb=CONNECT_TO_SVN,
                script=SVN_CREATE_REPOSITORY_SCRIPT,
                shortname=repository.shortname,
                service_email='it-service-vcs-forward@cern.ch',
            ),
            timeout=None,
            silentFail=False,
            shell=True,
        )
    except NonZeroReturnCode:
        # TO-DO: set silentFail = True on the runCommand and instead send a more specific email here to
        # the admins.
        pass

@runAsynchronously
def resyncTrac(repository):
    '''
        This method will asynchronously run a script which resyncs the Trac database for a given SVN repository.
    '''
    try:
        svnlogger(
            level='info',
            message='Running command to resync trac for repository {0}.'
            .format(repository.shortname),
            email_admins=False,
        )

        runCommand(
            command='{connect_svnweb} "{script} {trac_location}{shortname}/ resync"'.format(
                connect_svnweb=CONNECT_TO_SVN,
                script='trac-admin',
                trac_location=TRAC_LOCATION,
                shortname=repository.shortname,
            ),
            timeout=None,
            silentFail=False,
            shell=True,
        )
    except NonZeroReturnCode:
        # TO-DO: set silentFail = True on the runCommand and instead send a more specific email here to
        # the admins.
        pass

@runAsynchronously
def upgrade_trac(repository):
    '''
        This method will asynchronously run a script which upgrades the Trac database for a given SVN repository.
    '''
    try:
        svnlogger(
            level='info',
            message='Running command to upgrade trac for repository {0}.'.format(repository.shortname),
            email_admins=False,
        )

        runCommand(
            command='{connect_svnweb} "{script} {trac_location}{shortname}/ upgrade"'.format(
                connect_svnweb=CONNECT_TO_SVN,
                script='trac-admin',
                trac_location=TRAC_LOCATION,
                shortname=repository.shortname,
            ),
            timeout=None,
            silentFail=False,
            shell=True,
        )
    except NonZeroReturnCode:
        # TO-DO: set silentFail = True on the runCommand and instead send a more
        # specific email here to the admins.
        pass


@ldap_authentication
@api_view(["GET", "POST", "PUT"])
@renderer_classes((JSONRenderer, JSONRenderer))
def trac_migrate(request, user, api_version, pk=None):
    '''
        This method will run a script which migrates the Trac database for a given SVN repository to a specific Gitlab project.
    '''
    try:
        svnlogger(
            level='info',
            message='Running command to migrate trac for repository {0}.'.format(pk),
            email_admins=False,
        )

        repository = get_object_or_404(SvnProject, pk=pk)

        if not (user['admin'] or user['login'] in [repository.libuser, repository.respuser]):
            return HttpResponse(status=status.HTTP_403_FORBIDDEN)

        if repository.status != 'active':
            return JSONResponse(
                {'errors': {'general': ['This repository is not active.']}},
                status=status.HTTP_400_BAD_REQUEST,
            )

        try:
            output = runCommand(
                command='{connect_svn} "{script}"'.format(
                    connect_svn=CONNECT_TO_SVN,
                    script="if test -d /reps/{svnproject}; then echo 0; else echo -1; fi".format(svnproject=pk),
                ),
                timeout=None,
                silentFail=False,
                shell=True,
            )
            project_name = output[:1]
            if project_name.strip() == str(0):

                output = get_trac_info(projkey=pk)

                #Extracting info from trac.ini
                split1 = output.split(" ")
                split2 = split1[2].split(":")

                user_param = split2[1][2:]
                passwd_param = split2[2].split("@")[0]
                db_host = split2[2].split("@")[1]
                db_port = split2[3].split("/")[0]
                db_origin_name = split2[3].split("/")[1]
                db_origin_name = db_origin_name.split("\n")[0]
                json_data = JSONParser().parse(request)

                svnlogger(
                    level='info',
                    message='Database info: {0}'.format(output),
                    email_admins=False,
                )

                svnlogger(
                    level='info',
                    message='dbOriginName info: {0}'.format(db_origin_name),
                    email_admins=False,
                )

                svnlogger(
                    level='info',
                    message='If you want to run this manually: /var/django/manage.py migrateTrac {0} {1} {2} {3} {4} {5}'.format(
                        str(db_host),
                        str(db_port),
                        str(user_param),
                        str(passwd_param),
                        str(db_origin_name),
                        str(json_data['destProjectName'])),
                    email_admins=False,
                )

                if not check_permissions_project('gitlab.cern.ch', json_data['destProjectName']):
                    return JSONResponse(
                        {'errors': {'general': ['The migration account has not been added to the project. You do not have permission to do this action, see the KB article.']}},
                        status=status.HTTP_401_UNAUTHORIZED,
                    )

                #migration async task
                migrate_trac_task(json_data, db_host, db_port, user_param, passwd_param, db_origin_name)
                return HttpResponse(status=status.HTTP_200_OK)
            else:
                return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        except OSError as ose:
            forgelogger('error', 'Command failed (OSError {errno}): {strerror}\nWhen: {connect_svn} "{script} {repository}"'.format(
                errno=ose.errno,
                strerror=ose.strerror,
                connect_svn=CONNECT_TO_SVN,
                script='grep mysql /reps/ctpcmsos/trac/conf/trac.ini',
                repository=repository.location+'db/current',
            ), False)
            return 0
        except NonZeroReturnCode as nze:
            forgelogger('error', 'NonZeroReturnCode (%s): %s' % (nze.returncode, nze.stderr), False)
            return 0
        except Exception as exp:
            svnlogger(
                level='error',
                message='Error info: {0}'.format(exp),
                email_admins=False,
            )
            return JSONResponse(
                {'errors': {'general': ['An error occurred. Make sure that Gitlab project exists and that migration user has been added to the destination project.']}},
                status=status.HTTP_400_BAD_REQUEST,
            )
    except NonZeroReturnCode as nze:
        # TO-DO: set silentFail==True on the runCommand and instead send a more
        # specific email here to the admins.
        forgelogger('error', 'NonZeroReturnCode (%s): %s' % (nze.returncode, nze.stderr), False)
        return HttpResponse(status=status.HTTP_200_OK)
    return 0

@runAsynchronously
def migrate_trac_task(json_data, db_host, db_port, user_param, passwd_param, db_origin_name):
    '''
        This method will asynchronously run a script which resyncs the Trac database for a given SVN repository.
    '''
    try:
        runCommand(
            command="/var/django/manage.py migrateTrac {0} {1} {2} {3} {4} {5} {6}"
            .format(
                str(db_host),
                str(db_port),
                str(user_param),
                str(passwd_param),
                str(db_origin_name),
                str('gitlab.cern.ch'),
                str(json_data['destProjectName'])),
            timeout=None,
            silentFail=False,
            shell=True,
        )
    except NonZeroReturnCode:
        return HttpResponse(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@runAsynchronously
def archive_project(pk):
    '''
        This method will asynchronously run a script which archives the given SVN repository.
    '''
    try:
        output = runCommand(
            command='{connect_svn} "{script}" {host}'.format(
                host="svnadmin@lxplus",
                connect_svn=CONNECT_TO_SVN,
                script='~svnadmin/bin/svncastor.sh {0}'.format(pk)
            ),
            timeout=None,
            silentFail=False,
            shell=True,
        )

        svnlogger(
            level='error',
            message='output: ' + str(output),
            email_admins=False,
        )
        return output
    except NonZeroReturnCode:
        return HttpResponse(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    except Exception as exp:
        svnlogger(
            level='error',
            message='Error info: {0}'.format(exp),
            email_admins=False,
        )
        return JSONResponse(
            {'errors': {'general': ['An error occurred. Make sure that the status of the repository is consistent.']}},
            status=status.HTTP_400_BAD_REQUEST,
        )
