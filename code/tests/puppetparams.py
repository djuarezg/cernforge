'''
    Dummy puppetparams.py for tests
'''
import os

try:
    DEBUG = (os.environ['DEBUG'] == 'True')
except KeyError:
    DEBUG = False

# Usernames and passwords
DB_NAME = 'devdb11'
DB_USER = 'forge_admin'
DB_PASS = ''
DB_ENGINE = 'django.db.backends.postgresql'
FORGE_USER = 'foradmin'
FORGE_PASS = ''
GIT2JIRA_PASS = ''
GITLAB_ADMIN_TOKEN = ''

ADMIN_EMAIL = 'agonzale@cern.ch'
DEV_EMAIL = 'agonzale@cern.ch'

# Misc parameters
SUPERUSER = 'foradmin'
ADMIN_EGROUP = 'VC-HD-Access'

TRAC_LOCATION = '/afs/cern.ch/project/svn/tracSLC5/'
TRAC_INIT_LOCKFILE = '/afs/cern.ch/project/svn/scripts/repo/create-project.lock'

JIRA_SERVICE_EMAIL = 'Snow-RQF-JIRA-Project@cern.ch'

SVN_REPS_PATH = '/afs/cern.ch/project/svn/reps'
SVN_MAX_QUOTA = 5200000 # in kiloBytes
SVN_MIN_QUOTA = 100 # in kiloBytes

JIRA_HOST = 'preprod.its.cern.ch'
