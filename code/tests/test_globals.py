'''
    Test class for our Global methods
'''
from django.test import TestCase

from cernforge.globals import (
    useRestApiWithBasicAuth,
    findOwnerOfServiceAccount,
    JIRA_REST_API,
    JIRA_MAIN
)


class Globals(TestCase):
    '''
        Test class for our Global methods
    '''
    def test_use_rest_api_with_basic_auth(self):
        '''
            Test useRestApiWithBasicAuth, query anonymously JIRA's server info page
        '''
        url = '%s/serverInfo' % JIRA_REST_API

        result, json = useRestApiWithBasicAuth(url)

        self.assertTrue(result)

        self.assertEqual(json['baseUrl'], JIRA_MAIN)

    def test_find_owner_of_service_account(self):
        '''
            Test findOwnerOfServiceAccount, agonzale must be the owner of jiradmin
        '''
        user = 'jiradmin'

        owner = findOwnerOfServiceAccount(user)

        self.assertEqual(owner['login'], 'agonzale')
