'''
    Test class for our LDAP methods
'''
from django.test import TestCase

from cernforge.cernldap import (
    egroup_exist,
    get_egroup_members,
    GroupNotFoundInLdap,
    egroup_exist_with_user_members,
    AccountNotFoundInLdap,
    get_user_info,
    user_exist,
    get_user_memberships,
    is_user_egroup_member,
    all_users,
    all_serviceaccounts,
    all_groups,
)

from cernforge.cernldap_pagedsearch import cernldap_pagedsearch


class LDAP(TestCase):
    '''
        Test class for our LDAP methods
    '''
    def test_search(self):
        '''
            Test the paged search for a user
        '''
        user = 'agonzale'
        basedn = 'OU=Users,OU=Organic Units,DC=cern,DC=ch'
        searchfilter = '(sAMAccountName=%s)' % user
        attrlist = ['sAMAccountName']

        dn_value = 'CN=%s,%s' % (user, basedn)

        result = cernldap_pagedsearch(basedn, searchfilter, attrlist, 3600)

        self.assertEqual([user], result[dn_value]['sAMAccountName'])

    def test_egroup_exist(self):
        '''
            Test the egroup_exist method, it exists
        '''
        self.assertTrue(egroup_exist('forge-unittest'))

    def test_egroup_not_exist(self):
        '''
            Test the egroup_exist method, it doesn't
        '''
        self.assertFalse(egroup_exist('fuck-uniitest'))

    def test_egroup_not_empty(self):
        '''
            Test the egroup has members, it should have
        '''
        self.assertTrue(egroup_exist_with_user_members('forge-unittest'))

    def test_egroup_empty(self):
        '''
            Test the egroup has members, it is empty
        '''
        self.assertFalse(egroup_exist_with_user_members('forge-empty'))

    def test_egroups_members_exception(self):
        '''
            Test the egroup doesn't exists
            I hope nobody creates this egroup (even though it will be ironicaly funny)
        '''
        self.assertRaises(GroupNotFoundInLdap, get_egroup_members, 'fuck-unittests')

    def test_egroups_members(self):
        '''
            Test nested egroups, forge-unittest has agonzale and forge-unittest2,
            which has djuarez.
        '''
        self.assertCountEqual(['agonzale', 'djuarezg'], get_egroup_members('forge-unittest'))

    def test_get_user_info__exception(self):
        '''
            Test idonteexists raises an Exception
        '''
        user = 'idonteexists'
        self.assertRaises(AccountNotFoundInLdap, get_user_info, user)

    def test_get_user_info(self):
        '''
            Test few data about jiradmin
        '''

        user = 'jiradmin'
        res = get_user_info(user)

        self.assertEqual(['JIRA Administrator'], res['displayName'])
        self.assertEqual(['Service'], res['employeeType'])
        self.assertEqual(['CN=jiradmin,OU=Users,OU=Organic Units,DC=cern,DC=ch'],
                         res['distinguishedName'])

    def test_user_exists(self):
        '''
            Test jiradmin exist
        '''
        self.assertTrue(user_exist('jiradmin'))

    def test_user_dont_exists(self):
        '''
            Test idonteexists doesn't exist
        '''
        self.assertFalse(user_exist('idonteexists'))

    def test_get_user_memberships_acc(self):
        '''
            Test the account idonteexists doesn't exist
        '''
        self.assertRaises(AccountNotFoundInLdap, get_user_memberships, 'idonteexists')

    def test_get_user_memberships(self):
        '''
            Test that jiramdin is member of at least one egroup. Must probe for a couple of
            groups jiradmin must be member
        '''
        self.assertTrue(len(get_user_memberships('jiradmin')) > 0)

    def test_is_user_egroup_member_nog(self):
        '''
            Test that fuck-unittests doesn't esist
        '''
        self.assertRaises(GroupNotFoundInLdap, is_user_egroup_member, 'jiradmin', 'fuck-unittests')

    def test_is_user_egroup_member_yes(self):
        '''
            Test jiradmin is member of it-service-its
        '''
        self.assertTrue(is_user_egroup_member('jiradmin', 'it-service-its'))

    def test_is_user_egroup_member_no(self):
        '''
            Test jiradmin is not member of it-service-vcs.
        '''
        self.assertFalse(is_user_egroup_member('jiradmin', 'it-service-vcs'))

    def test_all_users(self):
        '''
            Test all_users method, now it only check if return any. Should just
            probe for some accounts we know that must exist.
        '''

        self.assertTrue(len(all_users()) > 0)

    def test_all_serviceaccounts(self):
        '''
            Test all_serviceaccounts method, now it only check if return any. Should just
            probe for some service accounts we know that must exist.
        '''

        self.assertTrue(len(all_serviceaccounts()) > 0)

    def test_all_groups(self):
        '''
            Test all_groups method, now it only check if return any. Should just
            probe for some groups we know that must exist.
        '''
        self.assertTrue(len(all_groups()) > 0)
