'''
    Settings file for tests
'''
import os

SECRET_KEY = 'fake-key'
INSTALLED_APPS = [
    "tests",
    "svn",
]
DEBUG = True
DEV = DEBUG
DEV_EMAIL = 'agonzale@cern.ch'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ['POSTGRES_DB'],
        'USER': os.environ['POSTGRES_USER'],
        'PASSWORD': os.environ['POSTGRES_PASSWORD'],
        'HOST': os.environ['POSTGRES_HOST'],
        # 'PORT': os.environ['DB_PORT'],
    }
}
