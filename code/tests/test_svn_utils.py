'''
    Test SVN utils
'''
import re

from django.test import TestCase

from svn.utils import (
    generate_color,
)


class SvnUtils(TestCase):
    '''
        Test svn utils functions
    '''
    def test_generate_color(self):
        '''
            Test the generate color function
        '''
        color = generate_color()
        self.assertIs(len(re.findall(r'#[0-9a-f]{6}', color, re.I)), 1)
